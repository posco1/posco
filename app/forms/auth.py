from flask.ext.wtf import Form
from wtforms import StringField, BooleanField, PasswordField
from wtforms.validators import DataRequired, EqualTo, Email, length


class LoginForm(Form):
    email = StringField('email', validators=[DataRequired(), Email()])
    password = PasswordField('password', validators=[DataRequired() ])
    remember_me = BooleanField('remember_me', default=False)
    # for custom validation see this example http://flask.pocoo.org/snippets/64/


class RegistrationForm(Form):
    email = StringField('email', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired(), EqualTo(
        'confirm_password', message='Passwords must match'), length(min=5)])
    confirm_password = PasswordField('confirm_password', validators=[DataRequired()])
    accept_tos = BooleanField('accept_tos', validators=[DataRequired()])
