from flask.ext.wtf import Form
from app.models.db_queries import active_category_exists_by_name
from wtforms import StringField
from wtforms.validators import DataRequired


class CustomersForm(Form):
    customer_name = StringField(validators=[DataRequired()])
    contact_number = StringField(validators=[DataRequired()])
    email_address = StringField()
    customer_address = StringField()

    def __init__(self):
        Form.__init__(self)

    def validate(self):
        if not Form.validate(self):
            return False
        return True
