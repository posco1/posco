from flask.ext.wtf import Form
import re
from app.models.db_queries import active_server_name_exists
from wtforms import SubmitField, StringField
from wtforms.validators import DataRequired, Length, Regexp


class ServerNameForm(Form):
    server_name = StringField(validators=[DataRequired(),
                                          Regexp(r'^[a-zA-Z ]+$', re.IGNORECASE,
                                                 'only alphabets and spaces allowed'),
                                          Length(min=3, max=25)])


    def __init__(self):
        Form.__init__(self)

    def validate(self):
        if not Form.validate(self):
            return False
        if active_server_name_exists(self.server_name.data):
            self.server_name.errors.append('Server already exists')
            return False
        return True
