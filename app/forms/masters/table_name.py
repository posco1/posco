from flask.ext.wtf import Form
from app.models.db_queries import active_table_name_exists
from wtforms import SubmitField, StringField
from wtforms.validators import DataRequired


class TableNameForm(Form):
    table_name = StringField(validators=[DataRequired()])
    add_table_name = SubmitField()

    def __init__(self):
        Form.__init__(self)


    def validate(self):
        if not Form.validate(self):
            return False
        if active_table_name_exists(self.table_name.data):
            self.table_name.errors.append('Table already exists')
            return False
        return True

