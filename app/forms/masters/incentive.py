from flask.ext.wtf import Form
from app.models.db_queries import active_incentive_exists
from wtforms import SubmitField, StringField
from wtforms.validators import DataRequired


class IncentiveForm(Form):
    incentive = StringField(
        validators=[DataRequired(message='Incentive value is required')])
    add_incentive = SubmitField()

    def __init__(self):
        Form.__init__(self)
        # upsert_0_as_default_if_not_exists()

    def validate(self):
        if not Form.validate(self):
            return False
        try:
            float(self.incentive.data)
        except ValueError:
            self.incentive.errors.append('must be a valid number')
            return False
        if float(self.incentive.data) < 0:
            self.incentive.errors.append('must be a positive number')
            return False
        if active_incentive_exists(self.incentive.data):
            self.incentive.errors.append('Incentive already exists')
            return False
        return True
