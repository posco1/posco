from flask.ext.wtf import Form
from app.models.db_queries import printer_exists
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class PrinterForm(Form):
    printer_name = StringField(validators=[DataRequired()])
    add_printer = SubmitField()

    def __init__(self):
        Form.__init__(self)

    def validate(self):
        if not Form.validate(self):
            return False
        if printer_exists(self.printer_name.data):
            self.printer_name.errors.append('printer by that name already exists')
            return False
        return True
