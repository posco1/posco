from flask.ext.wtf import Form
from app.models.db_queries import menu_item_name_exists
from app.views.utils import allowed_file
from config import MENU_IMAGE_MAX_SIZE
from wtforms import StringField, SubmitField, SelectField, FileField
from wtforms.validators import DataRequired, NoneOf


class MenuForm(Form):
    item_name = StringField(validators=[DataRequired('Name is required')])
    item_price = StringField(validators=[DataRequired('required')])
    subcategory_id = SelectField(coerce=str, validators=[DataRequired(), NoneOf(['0'],
                                                                                message=u"select sub category")])
    incentive_id = SelectField(coerce=str, validators=[DataRequired(),
                                                       NoneOf(['0'],
                                                              message=u"select incentive")])
    item_image = FileField()

    def __init__(self):
        Form.__init__(self)

    def validate(self):
        if not Form.validate(self):
            return False
        try:
            float(self.item_price.data)
        except ValueError:
            self.item_price.errors.append(
                'must be a valid number')
            return False
        if float(self.item_price.data) < 0:
            self.item_price.errors.append(
                'price cannot be negative')
            return False
        if self.item_image.data.filename is not '' and not allowed_file(
                self.item_image.data.filename):
            self.item_image.errors.append('allowed extensions - png, jpeg, jpg, gif')
            return False
        if menu_item_name_exists(self.item_name.data):
            self.item_name.errors.append('Item already exists')
            return False
        return True
