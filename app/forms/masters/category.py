from flask.ext.wtf import Form
from app.models.db_queries import active_category_exists_by_name
from wtforms import StringField
from wtforms.validators import DataRequired


class CategoryForm(Form):
    category = StringField(validators=[DataRequired(message='category name is required')])
    applicable_vat = StringField(validators=[DataRequired(message='vat is required')])

    def __init__(self):
        Form.__init__(self)

    def validate(self):
        if not Form.validate(self):
            return False
        try:
            float(self.applicable_vat.data)
        except ValueError:
            self.applicable_vat.errors.append('must be a valid number')
            return False
        if float(self.applicable_vat.data) < 0:
            self.applicable_vat.errors.append('must be a positive number')
            return False
        if float(self.applicable_vat.data) > 100:
            self.applicable_vat.errors.append('cannot exceed 100% !')
            return False
        if active_category_exists_by_name(self.category.data):
            self.category.errors.append('Category already exists')
            return False
        return True
