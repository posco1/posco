from flask.ext.wtf import Form
import re
from app.models.db_queries import printer_exists, settlement_mode_exists
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Regexp, Length


class SettlementModeForm(Form):
    settlement_mode_name = StringField(validators=[DataRequired(),
                                          Regexp(r'^[0-9a-zA-Z ]+$', re.IGNORECASE,
                                                 'only alphabets, numbers and spaces allowed'),
                                          Length(min=3, max=25)])
    add_printer = SubmitField()

    def __init__(self):
        Form.__init__(self)

    def validate(self):
        if not Form.validate(self):
            return False
        if settlement_mode_exists(self.settlement_mode_name.data):
            self.settlement_mode_name.errors.append('settlement mode already exists')
            return False
        return True
