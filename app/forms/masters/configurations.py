from flask.ext.wtf import Form
from app.models.db_inserts import add_empty_configurations_if_does_not_exist
from wtforms import StringField, SubmitField, SelectField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, NoneOf, Email
from wtforms.widgets import TextArea


class ConfigurationsForm(Form):
    add_empty_configurations_if_does_not_exist()
    outlet_name = StringField(validators=[DataRequired(message='Outlet Name required')])
    email = EmailField(validators=[DataRequired(message='Email required'), Email()])
    phone = StringField(validators=[DataRequired(message='Phone required')])
    address = StringField(validators=[DataRequired(message='Address required')], widget=TextArea())
    city = StringField(validators=[DataRequired(message='City required')])
    state = StringField(validators=[DataRequired(message='State required')])
    country = SelectField(coerce=str, validators=[DataRequired(),
                                                  NoneOf(['0'],
                                                         message=u"select country")])
    postal_code = StringField(validators=[DataRequired(message='Postal code required')])
    service_tax = StringField(validators=[DataRequired(message='Service Tax rate required')])
    service_charge = StringField(validators=[DataRequired(message='Service charge required. Enter 0 for all inclusive service charge')])
    home_delivery_charge = StringField(validators=[DataRequired(message='Home Delivery Charge required')])
    vat_number = StringField()
    service_tax_registration_number = StringField()
    bill_footer_message = StringField()
    save = SubmitField()

    def __init__(self):
        Form.__init__(self)

    def validate(self):
        if not Form.validate(self):
            return False
        try:
            float(self.service_tax.data)
        except ValueError:
            self.service_tax.errors.append('must be a valid number')
            return False
        if 0 > float(self.service_tax.data) > 100:
            self.service_tax.errors.append('must be a valid tax rate')
            return False
        try:
            float(self.home_delivery_charge.data)
        except ValueError:
            self.home_delivery_charge.errors.append('must be a valid number')
            return False
        if float(self.home_delivery_charge.data) < 0:
            self.home_delivery_charge.errors.append('must be a positive value')
            return False
        try:
            float(self.service_charge.data)
        except ValueError:
            self.service_charge.errors.append('must be a valid number')
            return False
        if 0 > float(self.service_charge.data) > 100:
            self.service_charge.errors.append('must be a valid rate')
            return False
        return True
