from flask.ext.wtf import Form
from app.models.db_queries import active_subcategory_exists_by_name
from wtforms import StringField, SelectField
from wtforms.validators import DataRequired, NoneOf


class SubCategoryForm(Form):
    subcategory_name = StringField('subcategory_name', validators=[
        DataRequired(message='subcategory name is required')])
    category_id = SelectField(coerce=str, validators=[DataRequired(),
                                                      NoneOf(['0'],
                                                             message=u"select category")])
    printer_id = SelectField(coerce=str, validators=[DataRequired(),
                                                     NoneOf(['0'],
                                                            message=u"select printer")])

    def __init__(self):
        Form.__init__(self)

    def validate(self):
        if not Form.validate(self):
            return False
        if active_subcategory_exists_by_name(self.subcategory_name.data):
            self.subcategory_name.errors.append('Sub Category already exists')
            return False
        return True
