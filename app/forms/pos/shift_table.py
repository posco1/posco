from flask.ext.wtf import Form
from app.models.db_queries import active_subcategory_exists_by_name
from wtforms import StringField, SelectField
from wtforms.validators import DataRequired, NoneOf


class ShiftTableForm(Form):
    running_table_session_id = SelectField(coerce=str, validators=[DataRequired(),
                                                     NoneOf(['0'],
                                                            message=u"select from table")])
    vacant_table_id = SelectField(coerce=str, validators=[DataRequired(),
                                                     NoneOf(['0'],
                                                            message=u"select destination table")])
    split_table_name = StringField()

    def __init__(self):
        Form.__init__(self)

    def validate(self):
        if not Form.validate(self):
            return False
        return True
