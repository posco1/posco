from flask.ext.wtf import Form
from app.models.db_queries import active_category_exists_by_name
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class ModifyOrderTokenForm(Form):

    def __init__(self):
        Form.__init__(self)

    def validate(self):
        if not Form.validate(self):
            return False
        return True
