from flask.ext.wtf import Form
from app.models.db_queries import active_subcategory_exists_by_name
from wtforms import StringField, SelectField
from wtforms.validators import DataRequired, NoneOf


class MergeTablesForm(Form):
    absorbed_table_session_id = SelectField(coerce=str, validators=[NoneOf(['0'],
                                                            message=u"select table")])
    absorber_table_session_id = SelectField(coerce=str, validators=[DataRequired(),
                                                     NoneOf(['0'],
                                                            message=u"select table")])

    def __init__(self):
        Form.__init__(self)

    def validate(self):
        if not Form.validate(self):
            return False
        return True
