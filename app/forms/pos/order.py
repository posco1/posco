from flask.ext.wtf import Form
from wtforms import StringField, SubmitField, SelectField
from wtforms.validators import DataRequired, NoneOf


class OrderForm(Form):
    server_name = SelectField(coerce=str, validators=[DataRequired(),
                                                       NoneOf(['0'],
                                                              message=u"select server")])
    table_name = SelectField(coerce=str, validators=[DataRequired(),
                                                       NoneOf(['0'],
                                                              message=u"select table")])
    split_table_name = StringField()
    number_of_pax = StringField(validators=[DataRequired()])
    email_address = StringField()
    contact_number = StringField()
    customer_name = StringField()
    customer_address = StringField()

    def __init__(self):
        Form.__init__(self)

    def validate(self):
        if not Form.validate(self):
            return False
        return True
