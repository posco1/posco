import datetime
from pprint import pprint
import subprocess
from app.models.db_queries import get_configurations_from_db, \
    get_subcategory_id_for_menu_item_id, \
    get_printer_id_for_subcategory_id, get_printer_name_from_printer_id, \
    get_server_name_from_server_id, \
    get_active_printer_id_from_printer_name, table_name_from_table_id_and_extension, \
    get_table_session_info_from_table_session_id, get_bill_details_for_table_session
from bson import ObjectId
import os
from config import TEMP_FILES_FOLDER, PRINT_TEMPLATE_FILES_FOLDER


def send_bill_to_bill_printer(bill_id, bill, table_session_id, printer_name):
    configuration = get_configurations_from_db()
    bill_body = make_static_bill_body_for_table_session_id(bill_id, bill,
                                                           table_session_id,
                                                           configuration)
    bill_all_items_content = generate_bill_content_from_bill_template(bill,
                                                                      configuration)
    bill_body = bill_body.replace('<bill_placeholder>', bill_all_items_content)
    print_to_file(bill_body, printer_name)


def make_static_bill_body_for_table_session_id(bill_id, bill, table_session_id,
                                               configuration):
    table_session_info = get_table_session_info_from_table_session_id(
        table_session_id)
    table_id = table_session_info['table_id']
    split_table_name = table_session_info['split_table_name']
    table_name = table_name_from_table_id_and_extension(table_id, split_table_name)
    pax = table_session_info['number_of_pax']
    server_name = get_server_name_from_server_id(table_session_info['server_id'])
    return generate_static_content_from_bill_template(configuration, bill_id,
                                                      bill, table_name, pax,
                                                      server_name)


def length_to_be_left_as_whitespace(menu_item_name):
    return 32 - len(menu_item_name)


def print_to_file(content, filename):
    with open(TEMP_FILES_FOLDER + '/' + filename+'.txt', "w") as text_file:
        text_file.write(content)


def do_the_actual_printing(filename_to_print, printer_name):
    # http://www.compuphase.com/software_spool.htm
    try:
        subprocess.Popen(["spool.exe", filename_to_print, printer_name])
    except FileNotFoundError:
        return 'printer error - invalid printer configuration'


def delete_all_files_from_temp_folder():
    for the_file in os.listdir(TEMP_FILES_FOLDER):
        file_path = os.path.join(TEMP_FILES_FOLDER, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)


def generate_static_content_from_bill_template(configuration, bill_id, bill,
                                               table_name, pax,
                                               server_name):
    with open(PRINT_TEMPLATE_FILES_FOLDER + '/bill_template.txt') as f:
        content = f.readlines()
        c = ''.join(content)
    return c.format(if_bill_duplicate_message='',
                    company_name=configuration['outlet_name'],
                    company_address=configuration['address'],
                    company_city=configuration['city'],
                    company_phone=configuration['phone'],
                    bill_id=str(bill_id)[-5:],
                    date_time=datetime.datetime.now().strftime('%d-%m-%Y %H:%M'),
                    table_name=table_name,
                    pax=pax,
                    server_name=server_name,
                    grand_total='{0: <20}'.format(bill['total_bill_amount']),
                    vat_number=configuration['vat_number'],
                    service_tax_number=configuration[
                        'service_tax_registration_number'],
                    bill_footer_message=configuration['bill_footer_message'])


def generate_bill_content_from_bill_template(bill, configuration):
    grand_total = 0
    order_type = bill['order_type']
    home_delivery_charge = 0
    service_tax_rate = float(configuration['service_tax'])
    service_charge_rate = float(configuration['service_charge'])
    if order_type == 'take_away':
        service_charge_rate = 0
    if order_type == 'home_delivery':
        service_charge_rate = 0
        home_delivery_charge = float(configuration['home_delivery_charge'])
    bill_content = ''
    for category_name in bill['category_wise'].keys():
        if category_has_all_zero_items(bill, category_name):
            continue
        total_for_category = 0
        vat_rate = float(bill['category_wise'][category_name]['vat_rate'])
        for item in bill['category_wise'][category_name]['all_items']:
            item_name = item[0]
            qty = item[1]
            if qty == 0:
                continue
            discount_rate_on_item = item[5]
            number_of_complementary_items = item[6]
            if discount_rate_on_item == 0 and number_of_complementary_items==0:
                total = item[3]
                total_for_category += total
                bill_content = bill_content + '{0: <16}'.format(
                    item_name[0:16]) + ' ' * 5 + str(qty) + ' ' * 10 + '{0: >10}'.format(
                    '{0:.2f}'.format(total)) + '\n'
            elif discount_rate_on_item != 0 and number_of_complementary_items==0:
                discount_amount = discount_rate_on_item*item[3]/100
                total = item[3] - discount_amount
                total_for_category += total
                bill_content = bill_content + '{0: <16}'.format(
                    item_name[0:16]) + ' ' * 5 + str(qty)  + '{0: >10}'.format(
                    '{0:.2f}'.format(item[3])) + '\n'
                bill_content = bill_content + '{0: <16}'.format(
                    '- less discount @'+ str(discount_rate_on_item)+'%') + ' ' * 7 + '{0:.2f}'.format(discount_amount) + '{0: >10}'.format(
                    '{0:.2f}'.format(total)) + '\n'
            elif discount_rate_on_item == 0 and number_of_complementary_items != 0:
                complementary_amount = item[6]*item[2]
                total = item[3] - complementary_amount
                total_for_category += total
                bill_content = bill_content + '{0: <16}'.format(
                    item_name[0:16]) + ' ' * 5 + str(qty)  + '{0: >10}'.format(
                    '{0:.2f}'.format(item[3])) + '\n'
                bill_content = bill_content + '{0: <16}'.format(
                    '- less complementary '+ str(item[6])) + ' ' * 4 + '{0:.2f}'.format(complementary_amount) + '{0: >10}'.format(
                    '{0:.2f}'.format(total)) + '\n'
        bill_content = bill_content + ' ' * 28 + '-' * 15 + '\n'
        bill_content = bill_content + ' ' * 3 + '{0: <25}'.format(
            'Total for ' + category_name[0:16]) + '{0: >14}'.format(
            '{0:.2f}'.format(total_for_category)) + '\n'
        service_charge = total_for_category * service_charge_rate / 100
        vat_charge = total_for_category * vat_rate / 100
        service_tax = total_for_category * service_tax_rate / 100
        grand_total = grand_total + total_for_category +service_charge + vat_charge +service_tax
        if order_type == 'dine-in':
            bill_content = bill_content + ' ' * 3 + 'Service Charge @ ' + str(
                service_charge_rate) + '% ' + '{0: >17}'.format(
                '{0:.2f}'.format(service_charge)) + '\n'
        bill_content = bill_content + ' ' * 3 + 'Vat @ ' + str(
            vat_rate) + '% ' + '{0: >28}'.format('{0:.2f}'.format(vat_charge)) + '\n'
        bill_content = bill_content + ' ' * 3 + 'Govt Service Tax @ ' + str(
            service_tax_rate) + '% ' + '{0: >15}'.format(
            '{0:.2f}'.format(service_tax)) + '\n'
    if order_type == 'home_delivery':
        grand_total = grand_total + home_delivery_charge
        bill_content = bill_content + ' ' * 3 + 'Home Delivery Charge' + '{0: >19}'.format(
            '{0:.2f}'.format(home_delivery_charge)) + '\n'
    discount_percent_on_total = float(bill['over_all_bill_discount_percentage'])
    if discount_percent_on_total != 0:
        overall_discount = grand_total * discount_percent_on_total/100
        bill_content = bill_content + 'Discount on total @'+ str(discount_percent_on_total) +'%' + '{0: >21}'.format(
            '{0:.2f}'.format(overall_discount)) + '\n'
    return bill_content


def category_has_all_zero_items(bill, category_name):
    for item in bill['category_wise'][category_name]['all_items']:
            qty = item[1]
            if qty == 0:
                continue
            else:
                return False
    return True
