import datetime
import subprocess

from app.models.db_queries import get_configurations_from_db, \
    get_printer_name_from_printer_id, \
    get_server_name_from_server_id, \
    table_name_from_table_id_and_extension, \
    get_order_details_from_order_id, get_order_token_details_from_order_token_id
import os
from config import TEMP_FILES_FOLDER, PRINT_TEMPLATE_FILES_FOLDER, TABLE_ID_TAKE_AWAY, \
    TABLE_ID_HOME_DELIVERY


def send_order_token_to_printers(order_id, table_id, split_table_name):
    delete_all_files_from_temp_folder()
    order_details = get_order_details_from_order_id(order_id)
    outlet_information = get_configurations_from_db()
    if order_details['table_id'] == TABLE_ID_TAKE_AWAY:
        server_name = 'take away'
    elif order_details['table_id'] == TABLE_ID_HOME_DELIVERY:
        server_name = 'home delivery'
    else:
        server_name = get_server_name_from_server_id(order_details['server_id'])
    table_name_with_extension = table_name_from_table_id_and_extension(
        order_details['table_id'], order_details['split_table_name'])
    number_of_pax = order_details['number_of_pax']
    order_token_ids_list = order_details['order_token_ids_list']
    for order_token_id in order_token_ids_list:
        order_token_details = get_order_token_details_from_order_token_id(
            order_token_id)
        printer_name = get_printer_name_from_printer_id(
            order_token_details['printer_id'])
        printer_message = order_token_details['printer_message']
        items_in_this_token = order_token_details['items_in_this_token']
        menu_string = ''
        for item in items_in_this_token:
            menu_string = menu_string + item[1] + (
                ' ' * length_to_be_left_as_whitespace(item[1])) + item[2] + '\n-' + \
                          item[4] + '\n\n'
        ot_body = generate_content_from_ot_template(outlet_information,
                                                    order_token_id,
                                                    table_name_with_extension,
                                                    server_name, order_details)
        ot_body = ot_body.replace('<order_details_placeholder>', menu_string)
        ot_body = ot_body.replace('<printer_remark_placeholder>', printer_message)
        print_to_file(ot_body, printer_name)


def length_to_be_left_as_whitespace(menu_item_name):
    return 32 - len(menu_item_name)


def print_to_file(content, filename):
    with open(TEMP_FILES_FOLDER + '/' + filename+'.txt', "w") as text_file:
        text_file.write(content)


def do_the_actual_printing(filename_to_print, printer_name):
    # http://www.compuphase.com/software_spool.htm
    try:
        subprocess.Popen(["spool.exe", filename_to_print, printer_name])
    except FileNotFoundError:
        return 'printer error - invalid printer configuration'


def delete_all_files_from_temp_folder():
    for the_file in os.listdir(TEMP_FILES_FOLDER):
        file_path = os.path.join(TEMP_FILES_FOLDER, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)


def generate_content_from_ot_template(outlet_information, ot_id,
                                      table_name_with_extension,
                                      server_name, order_details):
    with open(PRINT_TEMPLATE_FILES_FOLDER + '/ot_template.txt') as f:
        content = f.readlines()
        c = ''.join(content)
    return c.format(company_name=outlet_information['outlet_name'],
                    company_address=outlet_information['city'],
                    token_id=str(ot_id)[-5:],
                    date_time=datetime.datetime.now().strftime('%d-%m-%Y %H:%M'),
                    table_name=table_name_with_extension, server_name=server_name)
