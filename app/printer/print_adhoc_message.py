from app.printer.print_order_token import delete_all_files_from_temp_folder
from config import TEMP_FILES_FOLDER


def print_adhoc_message(content, printer_name):
    delete_all_files_from_temp_folder()
    print_to_file(content, printer_name)


def print_to_file(content, filename):
    with open(TEMP_FILES_FOLDER + '/' + filename +'.txt', "w") as text_file:
        text_file.write(content)
