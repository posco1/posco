from pprint import pprint
from app.models.utils import calculate_bill_total
from app.views.utils import pretty_date
from collections import defaultdict
import time
import datetime
from bson import ObjectId
from config import CONFIGURATIONS_OBJECT_ID, TABLE_ID_HOME_DELIVERY, \
    TABLE_ID_TAKE_AWAY, SERVER_ID_HOME_DELIVERY, SERVER_ID_TAKE_AWAY
import copy
import re
from app import app


def active_category_exists_by_name(category_name):
    query_result = app.config['CATEGORIES_COLLECTION'].find(
        {'data.category_name': re.compile('^' + re.escape(category_name) + '$',
                                          re.IGNORECASE)},
        {'data': {'$slice': -1}, '_id': 0, 'vid': 0, 'is_active': 0,
         'applicable_vat': 0})
    query_result = [f for f in query_result if f['data'][0]['is_active']]
    return bool(query_result)


def get_category_name_from_category_id(cat_id):
    return app.config['CATEGORIES_COLLECTION'].find_one(
        {'_id': ObjectId(cat_id)},
        {'data': {'$slice': -1}, '_id': 0, 'data.vid': 0, 'data.applicable_vat': 0})[
        'data'][0][
        'category_name']


def get_all_categories_with_applicable_vat():
    return app.config['CATEGORIES_COLLECTION'].find(
        {}, {'data': {'$slice': -1}, '_id': 0, 'data.vid': 0})


def get_all_categories_with_id():
    return app.config['CATEGORIES_COLLECTION'].find({}, {'data': {'$slice': -1},
                                                         'data.applicable_vat': 0})


def get_all_subcategories_with_categories_and_printer_id():
    return app.config['SUBCATEGORIES_COLLECTION'].find(
        {}, {'data': {'$slice': -1}, '_id': 0, 'data.vid': 0})


def active_subcategory_exists_by_name(subcategory_name):
    query_result = app.config['SUBCATEGORIES_COLLECTION'].find(
        {'data.subcategory_name': re.compile('^' + re.escape(subcategory_name) + '$',
                                             re.IGNORECASE)},
        {'data': {'$slice': -1}})
    query_result = [f for f in query_result if f['data'][0]['is_active']]
    return bool(query_result)


def get_all_active_subcategory_category_printer_maps():
    all_subcat_and_cat_ids_printer_ids = [
        (i['data'][0]['subcategory_name'],
         get_category_name_from_category_id(i['data'][0]['category_id']),
         get_printer_name_from_printer_id(i['data'][0]['printer_id'])) for i in
        get_all_subcategories_with_categories_and_printer_id() if
        i['data'][0]['is_active']]
    return all_subcat_and_cat_ids_printer_ids


def get_active_subcategory_id_from_name(subcategory_name):
    query_result = app.config['SUBCATEGORIES_COLLECTION'].find(
        {'data.subcategory_name': subcategory_name}, {'data': {'$slice': -1}})
    active_result = [f for f in query_result if f['data'][0]['is_active']]
    return active_result[0]['_id']


def get_category_id_for_subcategory_id(subcategory_id):
    query_result = app.config['SUBCATEGORIES_COLLECTION'].find_one(
        {'_id': subcategory_id}, {'data': {'$slice': -1}})
    return query_result['data'][0]['category_id']


def get_printer_id_for_subcategory_id(subcategory_id):
    query_result = app.config['SUBCATEGORIES_COLLECTION'].find_one(
        {'_id': subcategory_id}, {'data': {'$slice': -1}})
    return query_result['data'][0]['printer_id']


def category_has_subcategories(category_name):
    category_id = str(app.config['CATEGORIES_COLLECTION'].find_one(
        {'data.category_name': category_name},
        {"data": {'$elemMatch': {'category_name': category_name}}})['_id'])
    return bool(
        app.config['SUBCATEGORIES_COLLECTION'].find_one(
            {'data.category_id': category_id},
            {'data': {'$slice': -1}, '_id': 0, 'vid': 0}))


def get_all_active_incentives():
    return [i['data'][-1]['incentive'] for i in
            app.config['INCENTIVES_COLLECTION'].find({}, {'_id': 0}) if
            i['data'][-1]['is_active']]


def get_active_incentive_id_from_incentive(incentive):
    query_result = app.config['INCENTIVES_COLLECTION'].find(
        {'data.incentive': incentive}, {'data': {'$slice': -1}})
    active_result = [f for f in query_result if f['data'][-1]['is_active']]
    return active_result[-1]['_id']


def active_incentive_exists(incentive):
    query_result = app.config['INCENTIVES_COLLECTION'].find(
        {'data.incentive': incentive},
        {'data': {'$slice': -1}})
    query_result = [f for f in query_result if f['data'][0]['is_active']]
    return bool(query_result)


def get_all_active_table_names():
    return [i['data'][-1]['table_name'] for i in
            app.config['TABLE_NAMES_COLLECTION'].find({}, {'_id': 0}) if
            i['data'][-1]['is_active']]


def active_table_name_exists(table_name):
    query_result = app.config['TABLE_NAMES_COLLECTION'].find(
        {'data.table_name': re.compile('^' + re.escape(table_name) + '$',
                                       re.IGNORECASE)},
        {'data': {'$slice': -1}})
    query_result = [f for f in query_result if f['data'][0]['is_active']]
    return bool(query_result)


def get_active_table_id_from_table_name(table_name):
    query_result = app.config['TABLE_NAMES_COLLECTION'].find(
        {'data.table_name': table_name}, {'data': {'$slice': -1}})
    active_result = [f for f in query_result if f['data'][-1]['is_active']]
    return active_result[-1]['_id']


def get_all_active_server_names():
    return [i['data'][-1]['server_name'] for i in
            app.config['SERVER_NAMES_COLLECTION'].find({}, {'_id': 0}) if
            i['data'][-1]['is_active']]


def active_server_name_exists(server_name):
    query_result = app.config['SERVER_NAMES_COLLECTION'].find(
        {'data.server_name': re.compile('^' + re.escape(server_name) + '$',
                                        re.IGNORECASE)},
        {'data': {'$slice': -1}})
    query_result = [f for f in query_result if f['data'][0]['is_active']]
    return bool(query_result)


def get_active_server_id_from_server_name(server_name):
    query_result = app.config['SERVER_NAMES_COLLECTION'].find(
        {'data.server_name': server_name}, {'data': {'$slice': -1}})
    active_result = [f for f in query_result if f['data'][-1]['is_active']]
    return active_result[-1]['_id']


def get_all_active_subcategories_with_id():
    query_result = app.config['SUBCATEGORIES_COLLECTION'].find(
        {}, {'data': {'$slice': -1}, 'data.vid': 0, 'data.category_id': 0})
    all_active_subcategories_with_id = [
        (str(f['_id']), f['data'][0]['subcategory_name']) for f in query_result if
        f['data'][0]['is_active']]
    return all_active_subcategories_with_id


def get_all_active_incentives_with_id():
    query_result = app.config['INCENTIVES_COLLECTION'].find(
        {}, {'data': {'$slice': -1}, 'data.vid': 0})
    all_active_incentives_with_id = [(str(f['_id']), f['data'][0]['incentive']) for f
                                     in query_result if f['data'][0]['is_active']]
    return all_active_incentives_with_id


def menu_item_name_exists(item_name):
    query_result = app.config['MENU_ITEMS_COLLECTION'].find(
        {'data.item_name': re.compile('^' + re.escape(item_name) + '$',
                                      re.IGNORECASE)},
        {'data': {'$slice': -1}})
    query_result = [f for f in query_result if f['data'][0]['is_active']]
    return bool(query_result)


def get_all_menu_items():
    return [
        (i['data'][0]['item_name'], i['data'][0]['item_price'],
         get_subcategory_name_from_subcategory_id(i['data'][0]['subcategory_id']),
         get_incentive_name_from_incentive_id(i['data'][0]['incentive_id']),
         i['data'][0]['image']) for i in
        app.config['MENU_ITEMS_COLLECTION'].find({}, {'data': {'$slice': -1},
                                                      'data.vid': 0}) if
        i['data'][0]['is_active']]


def get_subcategory_name_from_subcategory_id(subcategory_id):
    return \
        app.config['SUBCATEGORIES_COLLECTION'].find_one(
            {'_id': ObjectId(subcategory_id)},
            {'data': {'$slice': -1}, 'data.category_id': 0})['data'][0][
            'subcategory_name']


def get_incentive_name_from_incentive_id(incentive_id):
    return \
        app.config['INCENTIVES_COLLECTION'].find_one(
            {'_id': ObjectId(incentive_id)},
            {'data': {'$slice': -1}})['data'][0]['incentive']


def get_active_item_details_from_item_name(item_name):
    query_result = app.config['MENU_ITEMS_COLLECTION'].find(
        {'data.item_name': item_name}, {'data': {'$slice': -1}})
    active_result = [f for f in query_result if f['data'][-1]['is_active']]
    return active_result[-1]


def there_are_menu_items_in_subcategory(subcategory_name_to_be_deleted):
    subcategory_id = str(app.config['SUBCATEGORIES_COLLECTION'].find_one(
        {'subcategory_name': subcategory_name_to_be_deleted},
        {'subcategory_name': 0, 'time': 0})['_id'])
    return bool(
        app.config['MENU_ITEMS_COLLECTION'].find_one(
            {'subcategory_id': subcategory_id},
            {'_id': 0, 'time': 0}))


def there_are_menu_items_in_incentive(incentive_to_be_deleted):
    incentive_id = str(app.config['INCENTIVES_COLLECTION'].find_one(
        {'incentive': incentive_to_be_deleted},
        {'incentive': 0, 'time': 0})['_id'])
    return bool(
        app.config['MENU_ITEMS_COLLECTION'].find_one(
            {'incentive_id': incentive_id},
            {'_id': 0, 'time': 0}))


def get_image_file_name_from_menu_item_name(menu_item_name):
    return \
        app.config['MENU_ITEMS_COLLECTION'].find_one(
            {'item_name': menu_item_name},
            {'_id': 0, 'time': 0, 'subcategory_id': 0, 'incentive_id': 0})['image']


def get_configurations_from_db():
    return app.config['CONFIGURATIONS_COLLECTION'].find_one(
        {'_id': ObjectId(CONFIGURATIONS_OBJECT_ID)})


def get_all_active_servers_with_id():
    query_result = app.config['SERVER_NAMES_COLLECTION']. \
        find({}, {'data': {'$slice': -1}, 'data.vid': 0})
    all_active_servers = [(str(f['_id']), f['data'][0]['server_name']) for f in
                          query_result
                          if f['data'][0]['is_active']]
    return all_active_servers


def get_all_active_table_names_with_id():
    query_result = app.config['TABLE_NAMES_COLLECTION']. \
        find({}, {'data': {'$slice': -1}, 'data.vid': 0})
    all_active_tables = [(str(f['_id']), f['data'][0]['table_name']) for f in
                         query_result if
                         f['data'][0]['is_active']]
    return all_active_tables


def get_all_menu_items_with_subcategory_id_and_price():
    query_result = app.config[
        'MENU_ITEMS_COLLECTION'].find({}, {'data': {'$slice': -1},
                                           'data.vid': 0, 'data.image': 0,
                                           'data.incentive_id': 0})
    return [{'item_id': str(i['_id']), 'item_name': i['data'][0]['item_name'],
             'subcategory_id': i['data'][0]['subcategory_id'],
             'item_price': i['data'][0]['item_price']} for i in query_result if
            i['data'][0]['is_active']]


def printer_exists(printer_name):
    result = [i for i in app.config['PRINTERS_COLLECTION'].find(
        {'data.printer_name': re.compile('^' + re.escape(printer_name) + '$',
                                         re.IGNORECASE)},
        {'data': {'$slice': -1}, '_id': 0, 'data.vid': 0}) if
              i['data'][0]['is_active']]
    return bool(result)


def get_all_active_printers():
    return app.config['PRINTERS_COLLECTION'].find(
        {'data.is_active': True}, {'data': {'$slice': -1}, '_id': 0, 'vid': 0})


def get_all_printer_names_with_id():
    return app.config['PRINTERS_COLLECTION'].find({},
                                                  {'data': {'$slice': -1},
                                                   'data.vid': 0})


def get_printer_name_from_printer_id(printer_id):
    return app.config['PRINTERS_COLLECTION'].find_one(
        {'_id': ObjectId(printer_id)},
        {'data': {'$slice': -1}, '_id': 0, 'data.vid': 0, 'data.user_id': 0})['data'][
        0]['printer_name']


def get_all_printers_with_id():
    return app.config['PRINTERS_COLLECTION'].find({}, {'data': {'$slice': -1}})


def get_active_printer_id_from_printer_name(printer_name):
    query_result = app.config['PRINTERS_COLLECTION'].find(
        {'data.printer_name': printer_name},
        {'data': {'$slice': -1}, 'data.vid': 0, 'data.user_id': 0})
    return [f for f in query_result if f['data'][0]['is_active']][0]['_id']


def is_printer_id_linked_to_active_subcategory(printer_id):
    query = app.config['SUBCATEGORIES_COLLECTION'].find(
        {'data.printer_id': str(printer_id)},
        {'data': {'$slice': -1}, 'data.vid': 0, 'data.user_id': 0})
    return len([f for f in query if f['data'][0]['is_active']]) > 0


def get_subcategory_id_for_menu_item_id(item_id):
    query_result = app.config[
        'MENU_ITEMS_COLLECTION'].find({'_id': ObjectId(item_id)},
                                      {'data': {'$slice': -1},
                                       'data.vid': 0, 'data.image': 0,
                                       'data.incentive_id': 0})
    return list(query_result)[0]['data'][0]['subcategory_id']


def get_table_name_from_table_id(table_id):
    if table_id == TABLE_ID_HOME_DELIVERY:
        return 'home delivery'
    elif table_id == TABLE_ID_TAKE_AWAY:
        return 'take away'
    else:
        return app.config['TABLE_NAMES_COLLECTION'].find_one(
            {'_id': ObjectId(table_id)},
            {'data': {'$slice': -1}, 'data.table_name': 1})[
            'data'][0]['table_name']


def get_server_name_from_server_id(server_id):
    if server_id == SERVER_ID_HOME_DELIVERY:
        return 'home delivery'
    elif server_id == SERVER_ID_TAKE_AWAY:
        return 'take away'
    else:
        return app.config['SERVER_NAMES_COLLECTION'].find_one(
            {'_id': ObjectId(server_id)},
            {'data': {'$slice': -1}, 'data.server_name': 1})[
            'data'][0]['server_name']


def table_name_from_table_id_and_extension(table_id, split_table_name):
    table_name = get_table_name_from_table_id(table_id)
    if split_table_name == '':
        return table_name
    else:
        return table_name + '-' + split_table_name


def get_bill_printer_name_for_user_id(user_id):
    return app.config['BILL_PRINTER_USER_MAP_COLLECTION'].find_one(
        {'user_id': user_id},
        {'printer_name': 1})


def get_category_id_for_item_id(item_id):
    subcategory_id = get_subcategory_id_for_menu_item_id(item_id)
    category_id = get_category_id_for_subcategory_id(ObjectId(subcategory_id))
    return category_id


def get_menu_item_name_from_menu_item_id(item_id):
    query_result = app.config['MENU_ITEMS_COLLECTION'].find_one(
        {'_id': ObjectId(item_id)},
        {'data': {'$slice': -1}})
    return query_result['data'][-1]['item_name'], query_result['data'][-1][
        'item_price']


def get_vat_rate_for_category_id(category_id):
    return app.config['CATEGORIES_COLLECTION'].find_one(
        {'_id': ObjectId(category_id)},
        {'data': {'$slice': -1}, '_id': 0, 'data.vid': 0, 'data.category_name': 0})[
        'data'][0]['applicable_vat']


def there_exists_a_running_session_on_table(table_id, split_table_name):
    query = app.config['TABLE_SESSION_COLLECTION'].find(
        {"$and": [{"initial_order_time": {'$gte': time.time() - 24 * 60 * 60}},
                  {"table_id": table_id},
                  {"split_table_name": split_table_name},
                  {"is_settled": False}]},
        {'data.user_id': 0})
    return query.count() > 0


def there_exists_a_billed_but_unsettled_session_on_table(table_id, split_table_name):
    print(table_id, split_table_name)
    query = app.config['TABLE_SESSION_COLLECTION'].find(
        {"$and": [{"table_id": table_id},
                  {"split_table_name": split_table_name},
                  {"is_settled": False, "is_billed": True}]},
        {'data.user_id': 0})
    print(list(query))
    return query.count() > 0


def get_order_details_from_order_id(order_id):
    return app.config['ORDERS_COLLECTION'].find_one(
        {'_id': ObjectId(order_id)}, {'_id': 0})


def get_order_token_details_from_order_token_id(order_token_id):
    return app.config['ORDER_TOKENS_COLLECTION'].find_one(
        {'_id': ObjectId(order_token_id)}, {'_id': 0})


def get_all_un_billed_running_tables_with_id():
    query_result = app.config['TABLE_SESSION_COLLECTION'].find(
        {'is_billed': False}, {'table_id': 1, 'split_table_name': 1})
    all_running_tables_with_id = [(str(f['_id']),
                                   table_name_from_table_id_and_extension(
                                       f['table_id'], f['split_table_name'])) for f
                                  in query_result]
    return all_running_tables_with_id


def get_all_items_in_order_tokens_with_table_session_id(table_session_id):
    q = app.config['ORDER_TOKENS_COLLECTION'].find(
        {'table_session_id': table_session_id, 'is_deleted': False},
        {'items_in_this_token': 1, '_id': 0})
    all_items = list(q)

    temp = copy.deepcopy(all_items)
    for index, item in enumerate(temp):
        for i, t in enumerate(item['items_in_this_token']):
            quantity = t[2]
            if quantity == '0':
                del all_items[index]['items_in_this_token'][i]
    all_items = [v for v in all_items if v['items_in_this_token']]
    return all_items


def get_order_type_for_table_session_id(table_session_id):
    return app.config['TABLE_SESSION_COLLECTION'].find_one(
        {'_id': ObjectId(table_session_id)}, {'order_type': 1, '_id': 0})[
        'order_type']


def get_bill_details_for_table_session(table_session_id):
    '''
        bill = {'category_wise': {
        'Food & Beverages': {'all_items': [['Chicken Fried', 2, random_price, random_price * 2],
                               ['Alloo Bhaja', 1, 40, 40]], 'vat_rate': 5},
        'Bar': {'all_items': [['Royal Challenge 30 ml', 3, 100, 300]], 'vat_rate': 15}
    }, -'service_tax_rate': 10, 'service_charge_rate': 7, 'vat_number': 'abcd', 'service_tax_registration_number': 'OKNAHI'}

    '''
    configurations = get_configurations_from_db()
    order_type = get_order_type_for_table_session_id(table_session_id)
    if order_type == 'home_delivery':
        home_delivery_charge = configurations['home_delivery_charge']
    else:
        home_delivery_charge = 0
    bill = {'order_type': order_type, 'category_wise': {},
            'home_delivery_charge': home_delivery_charge,
            'service_tax_rate': configurations['service_tax'],
            'service_charge_rate': configurations['service_charge'],
            'vat_number': configurations['vat_number'],
            'over_all_bill_discount_percentage': 0,
            'service_tax_registration_number': configurations[
                'service_tax_registration_number'], 'is_settled': False,
            'billed_by_user_id': '', 'settlement_details': '', 'settlement_time': '',
            'billing_time': '',
            'is_bill_modified': False, 'settled_by_user_id': ''}
    all_ordered_items = []

    all_items_in_this_order = get_all_items_in_order_tokens_with_table_session_id(
        table_session_id)
    for items in all_items_in_this_order:
        x = items['items_in_this_token']
        for i in x:
            all_ordered_items.append(i)
    all_ordered_items = normalize_duplicate_items(all_ordered_items)
    all_category_ids = []
    for item in all_ordered_items:
        category_id = get_category_id_for_item_id(item[0])
        category_name = get_category_name_from_category_id(category_id)
        item_name, item_price = get_menu_item_name_from_menu_item_id(item[0])
        try:
            bill['category_wise'][category_name]['all_items'].append(
                [item_name, item[1], int(item_price), item[1] * int(item_price),
                 item[0], 0,
                 0])  # last two numbers are discount percentage and number of items given as complementary
        except KeyError:
            bill['category_wise'][category_name] = {'all_items': [
                [item_name, item[1], int(item_price), item[1] * int(item_price),
                 item[0], 0, 0]]}
            all_category_ids.append(category_id)
    for category_id in all_category_ids:
        vat_rate = get_vat_rate_for_category_id(category_id)
        category_name = get_category_name_from_category_id(category_id)
        bill['category_wise'][category_name]['vat_rate'] = vat_rate
    return bill


def get_items_in_order_token_id(order_token_id):
    return app.config['ORDERS_COLLECTION'].find_one(
        {'_id': ObjectId(order_token_id)},
        {'data': {'$slice': -1}, 'data.items_ordered': 1})[
        'data'][0]['items_ordered']


def normalize_duplicate_items(all_ordered_items):
    trimmed_data = [[x[0], int(x[2])] for x in all_ordered_items]
    d = defaultdict(int)
    for key, value in trimmed_data:
        d[key] += value
    return d.items()


def get_table_session_info_from_table_session_id(table_session_id):
    return app.config['TABLE_SESSION_COLLECTION'].find_one(
        {'_id': ObjectId(table_session_id)},
        {'_id': 0})


def get_all_un_settled_running_tables_with_id():
    query_result = app.config['TABLE_SESSION_COLLECTION'].find(
        {'is_settled': False}, {'table_id': 1, 'split_table_name': 1})
    all_unsettled_tables_with_id = [(str(f['_id']),
                                     table_name_from_table_id_and_extension(
                                         f['table_id'], f['split_table_name'])) for f
                                    in query_result]
    return all_unsettled_tables_with_id


def get_customer_info_from_contact_number(contact_number):
    return app.config['CUSTOMERS_COLLECTION'].find_one(
        {'contact_number': contact_number}, {'_id': 0})


def get_customer_id_for_contact_number(contact_number):
    return str(app.config['CUSTOMERS_COLLECTION'].find_one(
        {'contact_number': contact_number}, {'_id': 1})['_id'])


def get_all_customers():
    return list(app.config['CUSTOMERS_COLLECTION'].find({}, {'_id': 0}))


def get_all_billed_but_unsettled_table_with_id():
    query_result = app.config['TABLE_SESSION_COLLECTION'].find(
        {'is_settled': False, 'is_billed': True},
        {'table_id': 1, 'split_table_name': 1})
    all_billed_but_unsettled_tables_with_id = [(str(f['_id']),
                                                table_name_from_table_id_and_extension(
                                                    f['table_id'],
                                                    f['split_table_name'])) for f
                                               in query_result]
    return all_billed_but_unsettled_tables_with_id


def get_unsettled_bill_ids_for_table_session(table_session_id):
    all_unsettled_bill_ids = []
    all_bill_ids = app.config['TABLE_SESSION_COLLECTION'].find_one(
        {'_id': ObjectId(table_session_id)},
        {'bill_ids': 1})['bill_ids']
    for bill_id in all_bill_ids:
        unsettled_bill = app.config['BILLS_COLLECTION'].find_one(
            {'_id': ObjectId(bill_id), 'is_settled': False}, {})
        if unsettled_bill is not None:
            id = str(unsettled_bill['_id'])
            all_unsettled_bill_ids.append(id)
    return all_unsettled_bill_ids


def get_bill_amount_for_bill_id(bill_id):
    return app.config['BILLS_COLLECTION'].find_one({'_id': ObjectId(bill_id)},
                                                   {'total_bill_amount': 1})[
        'total_bill_amount']


def get_all_active_settlement_mode():
    return app.config['SETTLEMENT_MODES_COLLECTION'].find(
        {'data.is_active': True}, {'data': {'$slice': -1}, '_id': 0, 'vid': 0})


def settlement_mode_exists(settlement_mode):
    result = [i for i in app.config['SETTLEMENT_MODES_COLLECTION'].find(
        {'data.settlement_mode_name': re.compile(
            '^' + re.escape(settlement_mode) + '$',
            re.IGNORECASE)},
        {'data': {'$slice': -1}, '_id': 0, 'data.vid': 0}) if
              i['data'][0]['is_active']]
    return bool(result)


def get_active_settlement_mode_id_from_settlement_mode_name(settlement_mode):
    query_result = app.config['SETTLEMENT_MODES_COLLECTION'].find(
        {'data.settlement_mode_name': settlement_mode},
        {'data': {'$slice': -1}, 'data.vid': 0, 'data.user_id': 0})
    return [f for f in query_result if f['data'][0]['is_active']][0]['_id']


def get_all_active_settlement_modes_with_id():
    return app.config['SETTLEMENT_MODES_COLLECTION'].find({},
                                                          {'data': {'$slice': -1}})


def get_all_printer_wise_order_token_ids_for_table_session(table_session_id):
    all_printer_wise_order_token_ids = {}
    all_order_ids = app.config['TABLE_SESSION_COLLECTION'].find_one(
        {'_id': ObjectId(table_session_id)}, {'order_ids': 1})['order_ids']
    for order_id in all_order_ids:
        query_result = app.config['ORDERS_COLLECTION'].find_one(
            {'_id': ObjectId(order_id)}, {'order_token_ids_list': 1})
        order_token_ids = query_result['order_token_ids_list']
        for order_token_id in order_token_ids:
            query_result = app.config['ORDER_TOKENS_COLLECTION'].find_one(
                {'_id': ObjectId(order_token_id), 'is_deleted': False},
                {'printer_id': 1})
            printer_id = query_result['printer_id']
            printer_name = get_printer_name_from_printer_id(printer_id)
            all_printer_wise_order_token_ids.setdefault(
                printer_name + '~' + printer_id, []).append(order_token_id)
    return all_printer_wise_order_token_ids


def get_order_token_details(order_token_id):
    query_result = app.config['ORDER_TOKENS_COLLECTION'].find_one(
        {'_id': ObjectId(order_token_id), 'is_deleted': False},
        {'items_in_this_token': 1})
    return query_result['items_in_this_token']


def get_all_unbilled_running_table_sessions_with_id():
    query_result = app.config['TABLE_SESSION_COLLECTION'].find(
        {'is_billed': False},
        {'table_id': 1, 'split_table_name': 1})
    all_running_tables_with_id = [(str(f['_id']),
                                   table_name_from_table_id_and_extension(
                                       f['table_id'], f['split_table_name'])) for f
                                  in query_result]
    return all_running_tables_with_id


def get_all_vacant_tables_with_id():
    query_result = app.config['TABLE_NAMES_COLLECTION'].find({},
                                                             {'data': {'$slice': -1},
                                                              'data.table_name': 1,
                                                              'data.is_active': 1})
    all_table_ids = [str(f['_id']) for f in query_result if f['data'][0]['is_active']]
    query_result = app.config['TABLE_SESSION_COLLECTION'].find(
        {'is_settled': False}, {'table_id': 1})
    all_running_table_ids = [f['table_id'] for f in query_result]
    all_vacant_table_ids = list(set(all_table_ids) - set(all_running_table_ids))
    all_vacant_table_names_with_id = [
        (f, table_name_from_table_id_and_extension(f, '')) for f in
        all_vacant_table_ids]
    return all_vacant_table_names_with_id


def get_table_details_for_sqaure_buttons():
    all_un_billed_table_sessions_with_id = get_all_unbilled_running_table_sessions_with_id()
    all_billed_but_unsettled_table_sessions_with_id = get_all_billed_but_unsettled_table_with_id()
    all_vacant_tables_with_id = get_all_vacant_tables_with_id()
    table_details = []
    for session in all_vacant_tables_with_id:
        table_details.append((session[1], 'vacant'))
    for session in all_un_billed_table_sessions_with_id:
        table_details.append((session[1], 'running'))
    for session in all_billed_but_unsettled_table_sessions_with_id:
        table_details.append((session[1], 'billed'))
    return sorted(table_details)


def get_order_timestamp_from_order_id(order_id):
    query_result = app.config['ORDERS_COLLECTION'].find_one(
        {'_id': ObjectId(order_id)}, {'time': 1})
    return int(query_result['time'])


def get_all_un_settled_running_table_sessions(table_session_id):
    q = app.config['TABLE_SESSION_COLLECTION'].find_one(
        {'_id': ObjectId(table_session_id), 'is_settled': False},
        {'table_id': 1, 'split_table_name': 1, 'server_id': 1,
         'initial_order_time': 1,
         'number_of_pax': 1, 'order_ids': 1})
    table_name = table_name_from_table_id_and_extension(q['table_id'],
                                                        q['split_table_name'])
    server_name = get_server_name_from_server_id(q['server_id'])
    initial_order_time = datetime.datetime.fromtimestamp(
        q['initial_order_time']).strftime('%H:%M:%S')
    number_of_pax = q['number_of_pax']
    last_order_id = q['order_ids'][-1]
    last_order_pretty_time = pretty_date(
        get_order_timestamp_from_order_id(last_order_id))

    configuration = get_configurations_from_db()
    service_tax_rate = float(configuration['service_tax'])
    service_charge_rate = float(configuration['service_charge'])
    bill = get_bill_details_for_table_session(table_session_id)
    bill_so_far_total = '{0:.2f}'.format(calculate_bill_total(bill, service_tax_rate,
                                             service_charge_rate, configuration))

    return (table_name, server_name, initial_order_time, number_of_pax,
            last_order_pretty_time, bill_so_far_total)


def get_unbilled_running_table_details_for_dashboard_table():
    all_running_table_details = []
    all_un_billed_table_sessions_with_id = get_all_unbilled_running_table_sessions_with_id()
    for session in all_un_billed_table_sessions_with_id:
        table_session_id = session[0]
        all_running_table_details.append(
            get_all_un_settled_running_table_sessions(table_session_id))

    return all_running_table_details


def get_unbilled_running_tables_with_session_id():
    all_un_billed_table_sessions_with_id = get_all_unbilled_running_table_sessions_with_id()
    return all_un_billed_table_sessions_with_id


def get_unsettled_bill_details_for_dashboard():
    unsettled_bill_details = []
    all_billed_but_unsettled_table_with_id = get_all_billed_but_unsettled_table_with_id()
    for session in all_billed_but_unsettled_table_with_id:
        table_session_id = session[0]
        unsettled_bill_details.append(get_table_details_for_unsettled_tables_for_dashboard(table_session_id))
    return unsettled_bill_details


def get_table_details_for_unsettled_tables_for_dashboard(table_session_id):
    q = app.config['TABLE_SESSION_COLLECTION'].find_one(
        {'_id': ObjectId(table_session_id), 'is_settled': False},
        {'table_id': 1, 'split_table_name': 1, 'server_id': 1,
         'billing_time': 1})
    table_name = table_name_from_table_id_and_extension(q['table_id'],
                                                        q['split_table_name'])
    server_name = get_server_name_from_server_id(q['server_id'])
    billing_time = pretty_date(int(q['billing_time']))

    configuration = get_configurations_from_db()
    service_tax_rate = float(configuration['service_tax'])
    service_charge_rate = float(configuration['service_charge'])
    bill = get_bill_details_for_table_session(table_session_id)
    bill_amount_final = '{0:.2f}'.format(calculate_bill_total(bill, service_tax_rate,
                                             service_charge_rate, configuration))
    return (table_name, server_name, billing_time, bill_amount_final )
