import datetime
import time
from app.models.db_queries import get_image_file_name_from_menu_item_name, \
    get_active_subcategory_id_from_name, get_active_incentive_id_from_incentive, \
    get_active_table_id_from_table_name, get_active_server_id_from_server_name, \
    get_active_item_details_from_item_name
from app.views.utils import random_filename
from bson import ObjectId
from config import CONFIGURATIONS_OBJECT_ID
from flask import g
import os
from app import app


def edit_category_from_db(original_name, new_name, new_vat):
    app.config['CATEGORIES_COLLECTION']. \
        update_one({'data.category_name': original_name},
                   {"$push": {"data": {'vid': time.time(), 'category_name': new_name,
                                       'applicable_vat': new_vat, 'is_active': True,
                                       'user_id': g.user_id}}})


def edit_subcategory_from_db(subcategory0, subcategory1, category1,
                             printer1):
    sub_category_id = get_active_subcategory_id_from_name(subcategory0)
    app.config['SUBCATEGORIES_COLLECTION']. \
        update_one({'_id': sub_category_id},
                   {"$push": {
                       "data": {'vid': time.time(), 'subcategory_name': subcategory1,
                                'category_id': category1, 'printer_id': printer1,
                                'is_active': True, 'user_id': g.user_id}}})


def edit_incentive_from_db(incentive_0, incentive_1):
    incentive_id = get_active_incentive_id_from_incentive(incentive_0)
    app.config['INCENTIVES_COLLECTION']. \
        update_one({'_id': incentive_id},
                   {"$push": {
                       "data": {'vid': time.time(), 'incentive': incentive_1,
                                'is_active': True, 'user_id': g.user_id}}})


def edit_table_name_from_db(table_name_0, table_name_1):
    table_id_to_be_edited = get_active_table_id_from_table_name(table_name_0)
    app.config['TABLE_NAMES_COLLECTION']. \
        update_one({'_id': table_id_to_be_edited},
                   {"$push": {
                       "data": {'vid': time.time(), 'table_name': table_name_1,
                                'is_active': True, 'user_id': g.user_id}}})


def edit_server_name_from_db(server_name_0, server_name_1):
    server_id_to_be_edited = get_active_server_id_from_server_name(server_name_0)
    app.config['SERVER_NAMES_COLLECTION']. \
        update_one({'_id': server_id_to_be_edited},
                   {"$push": {
                       "data": {'vid': time.time(), 'server_name': server_name_1,
                                'is_active': True, 'user_id': g.user_id}}})


def edit_menu_item_from_db(menu_item_0, menu_item_1, price_1, subcategory_1,
                           incentives_1, new_image_file, image0):
    if new_image_file != 'undefined' and new_image_file is not None:
        if image0 == 'default.png':
            filename = random_filename('default.png')
        else:
            filename = get_image_file_name_from_menu_item_name(menu_item_0)
        new_image_file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    else:
        filename = image0
    item_id_to_be_edited = get_active_item_details_from_item_name(menu_item_0)['_id']
    app.config['MENU_ITEMS_COLLECTION']. \
        update_one({'_id': item_id_to_be_edited},
                   {"$push": {
                       "data": {'vid': time.time(), 'item_name': menu_item_1,
                                'item_price': price_1,
                                "subcategory_id": subcategory_1,
                                "incentive_id": incentives_1,
                                "image": filename, 'is_active': True,
                                'user_id': g.user_id}}})


def update_configurations(form):
    app.config['CONFIGURATIONS_COLLECTION'].update_one(
        {'_id': ObjectId(CONFIGURATIONS_OBJECT_ID)},
        {"$set": {'outlet_name': form.outlet_name.data, 'email': form.email.data,
                  "phone": form.phone.data, "address": form.address.data,
                  "city": form.city.data, "state": form.state.data,
                  "country": form.country.data, "postal_code": form.postal_code.data,
                  'service_tax': form.service_tax.data,
                  'service_charge': form.service_charge.data,
                  'vat_number': form.vat_number.data,
                  'home_delivery_charge': form.home_delivery_charge.data,
                  'service_tax_registration_number': form.service_tax_registration_number.data,
                  'bill_footer_message': form.bill_footer_message.data,
                  "time": time.time()}})


def edit_printer_from_db(original_name, new_name):
    app.config['PRINTERS_COLLECTION']. \
        update_one({'data.printer_name': original_name},
                   {"$push": {"data": {'vid': time.time(), 'printer_name': new_name,
                                       'is_active': True, 'user_id': g.user_id}}})


def update_bill_printer_user_id_mapping(user_id, printer_name):
    app.config['BILL_PRINTER_USER_MAP_COLLECTION'].update_one({'user_id': user_id},
                                                              {'$set': {
                                                                  'user_id': user_id,
                                                                  'printer_name': printer_name}})


def add_order_id_to_existing_table_session(table_id, split_table_name, order_id):
    return app.config['TABLE_SESSION_COLLECTION']. \
        find_and_modify(
        {"$and": [{"initial_order_time": {'$gte': time.time() - 24 * 60 * 60}},
                  {"table_id": table_id},
                  {"split_table_name": split_table_name},
                  {"is_settled": False}]},
        {'$push': {'order_ids': str(order_id)}}, True)


def update_order_id_and_table_session_id_in_order_token(order_token_id, order_id,
                                                        table_session_id):
    app.config['ORDER_TOKENS_COLLECTION'].update_one(
        {'_id': ObjectId(order_token_id)},
        {"$set": {'order_id': order_id, 'table_session_id': table_session_id}})


def update_billing_details_in_table_session_db(running_table_session_id, bill_id):
    app.config['TABLE_SESSION_COLLECTION'].update_one(
        {'_id': ObjectId(running_table_session_id)},
        {'$push': {'bill_ids': str(bill_id)}, "$set": {'billing_time': time.time(),
                                                       'is_billed': True}})
    # todo False for now change this to true after testing - now changed to True but comment left for referanc


def insert_or_update_customer_info(contact_number, email_address, customer_name,
                                   customer_address, dob='na'):
    key = {'contact_number': contact_number}
    data = {'contact_number': contact_number, 'email_address': email_address,
            'customer_name': customer_name, 'is_active': True,
            'customer_address': customer_address, 'dob': dob, 'user_id': g.user_id,
            'time': time.time()}
    return app.config['CUSTOMERS_COLLECTION'].update(key, data, upsert=True)


def edit_customer_from_db(contact_number_0, customer_name_1, email_address_1,
                          contact_number_1, customer_address_1):
    key = {'contact_number': contact_number_0}
    data = {'contact_number': contact_number_1, 'email_address': email_address_1,
            'customer_name': customer_name_1, 'is_active': True,
            'customer_address': customer_address_1, 'dob': 'na', 'user_id': g.user_id,
            'time': time.time()}
    return app.config['CUSTOMERS_COLLECTION'].update(key, data)


def edit_settlement_mode_from_db(original_name, new_name):
    app.config['SETTLEMENT_MODES_COLLECTION']. \
        update_one({'data.settlement_mode_name': original_name},
                   {"$push": {
                       "data": {'vid': time.time(), 'settlement_mode_name': new_name,
                                'is_active': True, 'user_id': g.user_id}}})


def mark_bill_as_settled_with_settlement_details(bill_id, settlement_details):
    key = {'_id': ObjectId(bill_id)}
    data = {"$set": {'is_settled': True,
                     'settlement_details': settlement_details,
                     'settled_by_user_id': g.user_id,
                     'settlement_time': time.time()}}
    return app.config['BILLS_COLLECTION'].update(key, data, True)


def mark_table_session_as_settled(table_session_id):
    key = {'_id': ObjectId(table_session_id)}
    data = {"$set": {'is_settled': True, 'settlement_time': time.time()}}
    return app.config['TABLE_SESSION_COLLECTION'].update(key, data)


def modify_order_token_in_db(order_token_id, modified_items_in_this_order_token,
                             ot_modification_reason, ot_modification_printer_message):
    key = {'_id': ObjectId(order_token_id)}
    data = {"$set": {'items_in_this_token': modified_items_in_this_order_token,
                     'modification_justification': ot_modification_reason,
                     'modification_message_to_printer': ot_modification_printer_message,
                     'is_modified': True, 'modified_by': g.user_id,
                     'modification_time': time.time()}}
    return app.config['ORDER_TOKENS_COLLECTION'].update(key, data, True)


def shift_table_in_db(form):
    key = {'_id': ObjectId(form.data['running_table_session_id'])}
    data = {"$set": {'table_id': form.data['vacant_table_id'],
                     'split_table_name': form.data['split_table_name']}}
    app.config['TABLE_SESSION_COLLECTION'].update(key, data)
    all_order_ids = \
        app.config['TABLE_SESSION_COLLECTION'].find_one(key, {'order_ids': 1})[
            'order_ids']
    for order_id in all_order_ids:
        key = {'_id': ObjectId(order_id)}
        app.config['ORDERS_COLLECTION'].update(key, data)


def merge_table_in_db(form):
    absorbed_table_session_id = form.data['absorbed_table_session_id']
    absorber_table_session_id = form.data['absorber_table_session_id']
    absorber = app.config['TABLE_SESSION_COLLECTION'].find_one(
        {'_id': ObjectId(absorber_table_session_id)},
        {'table_id': 1, 'split_table_name': 1})
    absorber_table_id = absorber['table_id']
    absorber_split_table_name = absorber['split_table_name']
    all_order_ids_of_absorbed_table_session = \
        app.config['TABLE_SESSION_COLLECTION'].find_one(
            {'_id': ObjectId(absorbed_table_session_id)}, {'order_ids': 1})[
            'order_ids']
    for order_id in all_order_ids_of_absorbed_table_session:
        app.config['TABLE_SESSION_COLLECTION'].update_one(
            {'_id': ObjectId(absorber_table_session_id)},
            {'$push': {'order_ids': order_id}})
    for order_id in all_order_ids_of_absorbed_table_session:
        key = {'_id': ObjectId(order_id)}
        data = {'table_id':absorber_table_id,
                'split_table_name': absorber_split_table_name}
        app.config['ORDERS_COLLECTION'].update_one(key, {'$set': data})
    for order_id in all_order_ids_of_absorbed_table_session:
        key = {'_id': ObjectId(order_id)}
        data = app.config['ORDERS_COLLECTION'].find_one(key, {'order_token_ids_list':1})
        order_token_ids = data['order_token_ids_list']
        for ot_id in order_token_ids:
            key = { '_id': ObjectId(ot_id)}
            data = {'table_session_id': absorber_table_session_id}
            app.config['ORDER_TOKENS_COLLECTION'].update_one(key, {'$set': data})
    app.config['TABLE_SESSION_COLLECTION'].remove(
        {'_id': ObjectId(absorbed_table_session_id)})
