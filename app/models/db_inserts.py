from pprint import pprint
import time
from app.models.db_queries import get_configurations_from_db
from app.models.utils import calculate_bill_total
from bson import ObjectId
from config import CONFIGURATIONS_OBJECT_ID
from app import app
from flask import g
from pymongo.errors import DuplicateKeyError


def add_new_client(form, hashed_password):
    app.config['CLIENTS_COLLECTION'].insert({'email': form.email.data,
                                             'password': hashed_password,
                                             'time': time.time()})


def add_new_category(form):
    app.config['CATEGORIES_COLLECTION']. \
        insert({'data': [{'vid': time.time(), 'category_name': form.category.data,
                          'applicable_vat': form.applicable_vat.data,
                          'is_active': True, 'user_id': g.user_id}]})


def add_new_subcategory(form):
    app.config['SUBCATEGORIES_COLLECTION']. \
        insert(
        {'data': [{'vid': time.time(), 'subcategory_name': form.subcategory_name.data,
                   'category_id': form.category_id.data,
                   'printer_id': form.printer_id.data,
                   'is_active': True, 'user_id': g.user_id}]})


def add_new_incentive(form):
    app.config['INCENTIVES_COLLECTION']. \
        insert({'data': [{'vid': time.time(), 'incentive': form.incentive.data,
                          'is_active': True, 'user_id': g.user_id}]})


def add_new_table_name(form):
    app.config['TABLE_NAMES_COLLECTION']. \
        insert({'data': [{'vid': time.time(), 'table_name': form.table_name.data,
                          'is_active': True, 'user_id': g.user_id}]})


def add_new_server_name(form):
    app.config['SERVER_NAMES_COLLECTION']. \
        insert({'data': [{'vid': time.time(), 'server_name': form.server_name.data,
                          'is_active': True, 'user_id': g.user_id}]})


def add_new_item_to_menu(form, image_filename):
    app.config['MENU_ITEMS_COLLECTION']. \
        insert({'data': [{'vid': time.time(), 'item_name': form.item_name.data,
                          'item_price': form.item_price.data,
                          'subcategory_id': form.subcategory_id.data,
                          'incentive_id': form.incentive_id.data,
                          'image': image_filename,
                          'is_active': True, 'user_id': g.user_id}]})


def add_empty_configurations_if_does_not_exist():
    try:
        app.config['CONFIGURATIONS_COLLECTION']. \
            insert({'_id': ObjectId(CONFIGURATIONS_OBJECT_ID), 'outlet_name': 'na',
                    'email': 'na', 'phone': '91-xxxxx xxxxx', 'address': 'na',
                    'city': 'na', 'state': 'na', 'country': 'na',
                    'postal_code': '000000', 'service_tax': '0',
                    'service_charge': '0',
                    'home_delivery_charge': '0',
                    'vat_number': 'na', 'service_tax_registration_number': 'na',
                    'bill_footer_message': 'Thank You Visit Again !',
                    'time': time.time()})
    except DuplicateKeyError:
        pass


def add_new_printer(form):
    app.config['PRINTERS_COLLECTION']. \
        insert({'data': [{'vid': time.time(), 'printer_name': form.printer_name.data,
                          'is_active': True, 'user_id': g.user_id}]})


def add_bill_printer_user_id_mapping(user_id, printer_name):
    app.config['BILL_PRINTER_USER_MAP_COLLECTION'].insert(
        {'user_id': user_id, 'printer_name': printer_name})


def add_new_order_token(order_token):
    return app.config['ORDER_TOKENS_COLLECTION'].insert(order_token)


def add_new_order(table_id, split_table_name, server_id, number_of_pax,
                  order_token_ids_list, order_type):
    return app.config['ORDERS_COLLECTION'].insert(
        {'table_id': table_id, 'split_table_name': split_table_name,
         'server_id': server_id, 'order_token_ids_list': order_token_ids_list,
         'number_of_pax': number_of_pax, 'order_type': order_type,
         'user_id': g.user_id, 'time': time.time()})


def add_new_table_session(table_id, split_table_name, order_id, number_of_pax,
                          server_id, order_type, customer_id):
    return app.config['TABLE_SESSION_COLLECTION']. \
        insert({'initial_order_time': time.time(),
                'table_id': table_id, 'split_table_name': split_table_name,
                'number_of_pax': number_of_pax, 'server_id': server_id,
                'order_type': order_type, 'customer_id': customer_id,
                'order_ids': [order_id], 'is_billed': False, 'is_settled': False,
                'is_price_modified': False, 'bill_ids': [], 'billing_time': '',
                'settlement_time': '', 'user_id': g.user_id})


def add_bill_to_database(bill, table_session_id, bill_total=None):
    configuration = get_configurations_from_db()
    service_tax_rate = float(configuration['service_tax'])
    service_charge_rate = float(configuration['service_charge'])
    if bill_total is None:
        bill_total = calculate_bill_total(bill, service_tax_rate,
                                                     service_charge_rate, configuration)
    bill.pop('service_tax_registration_number', None)
    bill.pop('vat_number', None)
    bill['table_session_id'] = table_session_id
    bill['billed_by_user_id'] = g.user_id
    bill['settled_by_user_id'] = ''
    bill['billing_time'] = time.time()
    bill['settlement_time'] = ''
    bill['settlement_details'] = {}
    bill['bill_modification_type'] = bill.get('bill_modification_type', 'na')
    bill['bill_modification_reason'] = bill.get('bill_modification_reason', 'na')
    bill['customer_id'] = ''
    bill['service_tax_rate'] = service_tax_rate
    bill['service_charge_rate'] = service_charge_rate
    bill['total_bill_amount'] = bill_total
    bill['is_settled'] = False
    return app.config['BILLS_COLLECTION'].insert(bill)


def add_new_customer(form):
    print(form.data)
    app.config['CUSTOMERS_COLLECTION']. \
        insert({'contact_number': form.data['contact_number'],
                'email_address': form.data['email_address'],
                'customer_address': form.data['customer_address'],
                'customer_name': form.data['customer_name'],
                'dob': 'na', 'time': time.time(),
                'is_active': True, 'user_id': g.user_id})


def add_new_settlement_mode(form):
    app.config['SETTLEMENT_MODES_COLLECTION']. \
        insert({'data': [{'vid': time.time(), 'settlement_mode_name': form.settlement_mode_name.data,
                          'is_active': True, 'user_id': g.user_id}]})
