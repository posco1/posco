from flask_login import UserMixin, AnonymousUserMixin
from werkzeug.security import check_password_hash


class User(UserMixin):

    def __init__(self, id):
        self.id = id

    @staticmethod
    def validate_login(password_hash, password):
        return check_password_hash(password_hash, password)


class Anonymous(AnonymousUserMixin):

    name = u"Anonymous"
