import time
from app import app
from app.models.db_queries import get_active_subcategory_id_from_name, \
    get_category_id_for_subcategory_id, get_printer_id_for_subcategory_id, \
    get_active_incentive_id_from_incentive, get_active_table_id_from_table_name, \
    get_active_server_id_from_server_name, get_active_item_details_from_item_name, \
    get_active_printer_id_from_printer_name, \
    get_active_settlement_mode_id_from_settlement_mode_name
from bson import ObjectId
from flask import g


def delete_category_from_db(category_name, vat):
    app.config['CATEGORIES_COLLECTION']. \
        update_one({'data.category_name': category_name},
                   {"$push": {
                       "data": {'vid': time.time(), 'category_name': category_name,
                                'applicable_vat': vat, 'is_active': False,
                                'user_id': g.user_id}}})


def delete_subcategory_from_db(subcategory_name):
    subcategory_id_to_be_deleted = get_active_subcategory_id_from_name(
        subcategory_name)
    category_id = get_category_id_for_subcategory_id(subcategory_id_to_be_deleted)
    printer_id = get_printer_id_for_subcategory_id(subcategory_id_to_be_deleted)
    app.config['SUBCATEGORIES_COLLECTION']. \
        update_one({'_id': subcategory_id_to_be_deleted},
                   {"$push": {
                       "data": {'vid': time.time(),
                                'subcategory_name': subcategory_name,
                                'category_id': category_id, 'printer_id': printer_id,
                                'is_active': False,
                                'user_id': g.user_id}}})


def delete_incentive_from_db(incentive):
    incentive_id_to_delete = get_active_incentive_id_from_incentive(incentive)
    app.config['INCENTIVES_COLLECTION']. \
        update_one({'_id': incentive_id_to_delete},
                   {"$push": {
                       "data": {'vid': time.time(), 'incentive': incentive,
                                'is_active': False, 'user_id': g.user_id}}})


def delete_table_name_from_db(table_name):
    table_id_to_delete = get_active_table_id_from_table_name(table_name)
    app.config['TABLE_NAMES_COLLECTION']. \
        update_one({'_id': table_id_to_delete},
                   {"$push": {
                       "data": {'vid': time.time(), 'table_name': table_name,
                                'is_active': False, 'user_id': g.user_id}}})


def delete_server_name_from_db(server_name):
    server_id_to_delete = get_active_server_id_from_server_name(server_name)
    app.config['SERVER_NAMES_COLLECTION']. \
        update_one({'_id': server_id_to_delete},
                   {"$push": {
                       "data": {'vid': time.time(), 'server_name': server_name,
                                'is_active': False, 'user_id': g.user_id}}})


def delete_menu_item_from_db(item_name):
    item = get_active_item_details_from_item_name(item_name)
    app.config['MENU_ITEMS_COLLECTION']. \
        update_one({'_id': item['_id']},
                   {"$push": {"data": {'vid': time.time(),
                                       'item_name': item['data'][0]['item_name'],
                                       'item_price': item['data'][0]['item_price'],
                                       'subcategory_id': item['data'][0][
                                           'subcategory_id'],
                                       'incentive_id': item['data'][0][
                                           'incentive_id'],
                                       'image': item['data'][0]['image'],
                                       'is_active': False, 'user_id': g.user_id}}})


def delete_printer_from_db(printer_name):
    printer_id_to_delete = get_active_printer_id_from_printer_name(printer_name)
    app.config['PRINTERS_COLLECTION']. \
        update_one({'_id': printer_id_to_delete},
                   {"$push": {
                       "data": {'vid': time.time(), 'printer_name': printer_name,
                                'is_active': False, 'user_id': g.user_id}}})


def delete_if_bill_already_exists_for_table_session_id(running_table_session_id):
    app.config['BILLS_COLLECTION'].remove(
        {'table_session_id': running_table_session_id})
    app.config['TABLE_SESSION_COLLECTION'].update_one(
        {'_id': ObjectId(running_table_session_id)}, {'$set': {'bill_ids': []}})


def delete_settlement_mode_from_db(settlement_mode_name):
    settlement_mode_id_to_delete = get_active_settlement_mode_id_from_settlement_mode_name(
        settlement_mode_name)
    app.config['SETTLEMENT_MODES_COLLECTION']. \
        update_one({'_id': settlement_mode_id_to_delete},
                   {"$push": {
                       "data": {'vid': time.time(),
                                'settlement_mode_name': settlement_mode_name,
                                'is_active': False, 'user_id': g.user_id}}})


def mark_order_token_as_deleted(order_token_id, ot_modification_reason,
                                ot_modification_printer_message, table_session_id):
    app.config['ORDER_TOKENS_COLLECTION'].update_one(
        {'_id': ObjectId(order_token_id)},
        {'$set': {'modification_justification': ot_modification_reason,
                  'modification_message_to_printer': ot_modification_printer_message,
                  'is_modified': True, 'is_deleted': True, 'modified_by': g.user_id,
                  'modification_time': time.time()}})
    all_order_ids = app.config['TABLE_SESSION_COLLECTION'].find_one(
        {'_id': ObjectId(table_session_id)}, {'order_ids': 1})['order_ids']
    for order_id in all_order_ids:
        order_token_ids_list = app.config['ORDERS_COLLECTION'].find_one(
            {'_id': ObjectId(order_id)}, {'order_token_ids_list': 1})[
            'order_token_ids_list']
        for o_t_id in order_token_ids_list:
            if o_t_id == order_token_id:
                app.config['ORDERS_COLLECTION'].update_one(
                    {'_id': ObjectId(order_id)},
                    {'$pull': {'order_token_ids_list': order_token_id}})
