def calculate_bill_total(bill, service_tax_rate, service_charge_rate, configurations):
    order_type = bill['order_type']
    home_delivery_charge = 0
    if order_type == 'home_delivery':
        service_charge_rate = 0
        home_delivery_charge = float(configurations['home_delivery_charge'])
    if order_type == 'take_away':
        service_charge_rate = 0
    grand_total = 0
    for category_name in bill['category_wise'].keys():
        total_for_category = 0
        vat_rate = float(bill['category_wise'][category_name]['vat_rate'])
        for item in bill['category_wise'][category_name]['all_items']:
            total = item[3] - (item[5]*item[3]/100) - (item[6]*item[2])
            total_for_category += total
        service_charge = total_for_category * service_charge_rate / 100
        vat_charge = total_for_category * vat_rate / 100
        service_tax = total_for_category * service_tax_rate / 100
        total_for_category_with_all_taxes_and_charges = \
            total_for_category + service_charge + service_tax + vat_charge
        grand_total += total_for_category_with_all_taxes_and_charges
    grand_total += home_delivery_charge
    grand_total = round(grand_total, 0)
    grand_total -= float(bill['over_all_bill_discount_percentage']) * grand_total/100
    return round(grand_total, 0)
