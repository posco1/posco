from app import app
from flask import render_template
from flask_login import login_required


@app.route("/receive-goods")
@login_required
def receive_goods():
    return render_template("mms/receive_goods.html")
