from app import app
from flask import render_template
from flask_login import login_required


@app.route("/recipe-manufacture")
@login_required
def recipe_manufacture():
    return render_template("mms/recipe_manufacture.html")
