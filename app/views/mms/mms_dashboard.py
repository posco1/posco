from app import app
from flask import render_template
from flask_login import login_required


@app.route("/mms-dashboard")
@login_required
def mms_dashboard():
    return render_template("mms/mms_dashboard.html")
