from app import app
from flask import render_template
from flask_login import login_required


@app.route("/mms-reports")
@login_required
def mms_reports():
    return render_template("mms/mms_reports.html")
