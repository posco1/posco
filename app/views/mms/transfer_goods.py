from app import app
from flask import render_template
from flask_login import login_required


@app.route("/transfer-goods")
@login_required
def transfer_goods():
    return render_template("mms/transfer_goods.html")
