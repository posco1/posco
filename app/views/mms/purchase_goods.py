from app import app
from flask import render_template
from flask_login import login_required


@app.route("/purchase-goods")
@login_required
def purchase_goods():
    return render_template("mms/purchase_goods.html")
