import os
from config import ALLOWED_EXTENSIONS
from functools import wraps
from datetime import datetime
from random import choice
from string import ascii_uppercase
from flask import flash, redirect, url_for

from flask_login import current_user


def check_expired(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if datetime.utcnow() > current_user.account_expires:
            flash("Your account has expired. Update your billing info.")
            return redirect(url_for('account_billing'))
        return func(*args, **kwargs)

    return decorated_function


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def random_filename(filename_with_extension):
    extension = os.path.splitext(os.path.basename(filename_with_extension))[1]
    random_string = ''.join(
        choice(ascii_uppercase) for i in range(12))  # ~56 bits of entropy
    return random_string + extension


def pretty_date(time=False):
    """
    Get a datetime object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """
    from datetime import datetime
    now = datetime.now()
    if type(time) is int:
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time,datetime):
        diff = now - time
    elif not time:
        diff = now - now
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return str(second_diff) + " seconds ago"
        if second_diff < 120:
            return "a minute ago"
        if second_diff < 3600:
            return str(round(second_diff / 60, 2)) + " minutes ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str(round(second_diff / 3600,2)) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(day_diff) + " days ago"
    if day_diff < 31:
        return str(day_diff / 7) + " weeks ago"
    if day_diff < 365:
        return str(day_diff / 30) + " months ago"
    return str(day_diff / 365) + " years ago"


countries_list = [("0", "--select country--"), ("Afghanistan", "Afghanistan"),
                  ("Albania", "Albania"),
                  ("Algeria", "Algeria"),
                  ("American Samoa", "American Samoa"),
                  ("Andorra", "Andorra"),
                  ("Angola", "Angola"),
                  ("Anguilla", "Anguilla"),
                  ("Antigua &amp; Barbuda", "Antigua &amp; Barbuda"),
                  ("Argentina", "Argentina"),
                  ("Armenia", "Armenia"),
                  ("Aruba", "Aruba"),
                  ("Australia", "Australia"),
                  ("Austria", "Austria"),
                  ("Azerbaijan", "Azerbaijan"),
                  ("Bahamas", "Bahamas"),
                  ("Bahrain", "Bahrain"),
                  ("Bangladesh", "Bangladesh"),
                  ("Barbados", "Barbados"),
                  ("Belarus", "Belarus"),
                  ("Belgium", "Belgium"),
                  ("Belize", "Belize"),
                  ("Benin", "Benin"),
                  ("Bermuda", "Bermuda"),
                  ("Bhutan", "Bhutan"),
                  ("Bolivia", "Bolivia"),
                  ("Bonaire", "Bonaire"),
                  ("Bosnia &amp; Herzegovina", "Bosnia &amp; Herzegovina"),
                  ("Botswana", "Botswana"),
                  ("Brazil", "Brazil"),
                  ("British Indian Ocean Ter", "British Indian Ocean Ter"),
                  ("Brunei", "Brunei"),
                  ("Bulgaria", "Bulgaria"),
                  ("Burkina Faso", "Burkina Faso"),
                  ("Burundi", "Burundi"),
                  ("Cambodia", "Cambodia"),
                  ("Cameroon", "Cameroon"),
                  ("Canada", "Canada"),
                  ("Canary Islands", "Canary Islands"),
                  ("Cape Verde", "Cape Verde"),
                  ("Cayman Islands", "Cayman Islands"),
                  ("Central African Republic", "Central African Republic"),
                  ("Chad", "Chad"),
                  ("Channel Islands", "Channel Islands"),
                  ("Chile", "Chile"),
                  ("China", "China"),
                  ("Christmas Island", "Christmas Island"),
                  ("Cocos Island", "Cocos Island"),
                  ("Colombia", "Colombia"),
                  ("Comoros", "Comoros"),
                  ("Congo", "Congo"),
                  ("Cook Islands", "Cook Islands"),
                  ("Costa Rica", "Costa Rica"),
                  ("Cote DIvoire", "Cote D'Ivoire"),
                  ("Croatia", "Croatia"),
                  ("Cuba", "Cuba"),
                  ("Curaco", "Curacao"),
                  ("Cyprus", "Cyprus"),
                  ("Czech Republic", "Czech Republic"),
                  ("Denmark", "Denmark"),
                  ("Djibouti", "Djibouti"),
                  ("Dominica", "Dominica"),
                  ("Dominican Republic", "Dominican Republic"),
                  ("East Timor", "East Timor"),
                  ("Ecuador", "Ecuador"),
                  ("Egypt", "Egypt"),
                  ("El Salvador", "El Salvador"),
                  ("Equatorial Guinea", "Equatorial Guinea"),
                  ("Eritrea", "Eritrea"),
                  ("Estonia", "Estonia"),
                  ("Ethiopia", "Ethiopia"),
                  ("Falkland Islands", "Falkland Islands"),
                  ("Faroe Islands", "Faroe Islands"),
                  ("Fiji", "Fiji"),
                  ("Finland", "Finland"),
                  ("France", "France"),
                  ("French Guiana", "French Guiana"),
                  ("French Polynesia", "French Polynesia"),
                  ("French Southern Ter", "French Southern Ter"),
                  ("Gabon", "Gabon"),
                  ("Gambia", "Gambia"),
                  ("Georgia", "Georgia"),
                  ("Germany", "Germany"),
                  ("Ghana", "Ghana"),
                  ("Gibraltar", "Gibraltar"),
                  ("Great Britain", "Great Britain"),
                  ("Greece", "Greece"),
                  ("Greenland", "Greenland"),
                  ("Grenada", "Grenada"),
                  ("Guadeloupe", "Guadeloupe"),
                  ("Guam", "Guam"),
                  ("Guatemala", "Guatemala"),
                  ("Guinea", "Guinea"),
                  ("Guyana", "Guyana"),
                  ("Haiti", "Haiti"),
                  ("Hawaii", "Hawaii"),
                  ("Honduras", "Honduras"),
                  ("Hong Kong", "Hong Kong"),
                  ("Hungary", "Hungary"),
                  ("Iceland", "Iceland"),
                  ("India", "India"),
                  ("Indonesia", "Indonesia"),
                  ("Iran", "Iran"),
                  ("Iraq", "Iraq"),
                  ("Ireland", "Ireland"),
                  ("Isle of Man", "Isle of Man"),
                  ("Israel", "Israel"),
                  ("Italy", "Italy"),
                  ("Jamaica", "Jamaica"),
                  ("Japan", "Japan"),
                  ("Jordan", "Jordan"),
                  ("Kazakhstan", "Kazakhstan"),
                  ("Kenya", "Kenya"),
                  ("Kiribati", "Kiribati"),
                  ("Korea North", "Korea North"),
                  ("Korea Sout", "Korea South"),
                  ("Kuwait", "Kuwait"),
                  ("Kyrgyzstan", "Kyrgyzstan"),
                  ("Laos", "Laos"),
                  ("Latvia", "Latvia"),
                  ("Lebanon", "Lebanon"),
                  ("Lesotho", "Lesotho"),
                  ("Liberia", "Liberia"),
                  ("Libya", "Libya"),
                  ("Liechtenstein", "Liechtenstein"),
                  ("Lithuania", "Lithuania"),
                  ("Luxembourg", "Luxembourg"),
                  ("Macau", "Macau"),
                  ("Macedonia", "Macedonia"),
                  ("Madagascar", "Madagascar"),
                  ("Malaysia", "Malaysia"),
                  ("Malawi", "Malawi"),
                  ("Maldives", "Maldives"),
                  ("Mali", "Mali"),
                  ("Malta", "Malta"),
                  ("Marshall Islands", "Marshall Islands"),
                  ("Martinique", "Martinique"),
                  ("Mauritania", "Mauritania"),
                  ("Mauritius", "Mauritius"),
                  ("Mayotte", "Mayotte"),
                  ("Mexico", "Mexico"),
                  ("Midway Islands", "Midway Islands"),
                  ("Moldova", "Moldova"),
                  ("Monaco", "Monaco"),
                  ("Mongolia", "Mongolia"),
                  ("Montserrat", "Montserrat"),
                  ("Morocco", "Morocco"),
                  ("Mozambique", "Mozambique"),
                  ("Myanmar", "Myanmar"),
                  ("Nambia", "Nambia"),
                  ("Nauru", "Nauru"),
                  ("Nepal", "Nepal"),
                  ("Netherland Antilles", "Netherland Antilles"),
                  ("Netherlands", "Netherlands (Holland, Europe)"),
                  ("Nevis", "Nevis"),
                  ("New Caledonia", "New Caledonia"),
                  ("New Zealand", "New Zealand"),
                  ("Nicaragua", "Nicaragua"),
                  ("Niger", "Niger"),
                  ("Nigeria", "Nigeria"),
                  ("Niue", "Niue"),
                  ("Norfolk Island", "Norfolk Island"),
                  ("Norway", "Norway"),
                  ("Oman", "Oman"),
                  ("Pakistan", "Pakistan"),
                  ("Palau Island", "Palau Island"),
                  ("Palestine", "Palestine"),
                  ("Panama", "Panama"),
                  ("Papua New Guinea", "Papua New Guinea"),
                  ("Paraguay", "Paraguay"),
                  ("Peru", "Peru"),
                  ("Phillipines", "Philippines"),
                  ("Pitcairn Island", "Pitcairn Island"),
                  ("Poland", "Poland"),
                  ("Portugal", "Portugal"),
                  ("Puerto Rico", "Puerto Rico"),
                  ("Qatar", "Qatar"),
                  ("Republic of Montenegro", "Republic of Montenegro"),
                  ("Republic of Serbia", "Republic of Serbia"),
                  ("Reunion", "Reunion"),
                  ("Romania", "Romania"),
                  ("Russia", "Russia"),
                  ("Rwanda", "Rwanda"),
                  ("St Barthelemy", "St Barthelemy"),
                  ("St Eustatius", "St Eustatius"),
                  ("St Helena", "St Helena"),
                  ("St Kitts-Nevis", "St Kitts-Nevis"),
                  ("St Lucia", "St Lucia"),
                  ("St Maarten", "St Maarten"),
                  ("St Pierre &amp; Miquelon", "St Pierre &amp; Miquelon"),
                  ("St Vincent &amp; Grenadines", "St Vincent &amp; Grenadines"),
                  ("Saipan", "Saipan"),
                  ("Samoa", "Samoa"),
                  ("Samoa American", "Samoa American"),
                  ("San Marino", "San Marino"),
                  ("Sao Tome &amp; Principe", "Sao Tome &amp; Principe"),
                  ("Saudi Arabia", "Saudi Arabia"),
                  ("Senegal", "Senegal"),
                  ("Serbia", "Serbia"),
                  ("Seychelles", "Seychelles"),
                  ("Sierra Leone", "Sierra Leone"),
                  ("Singapore", "Singapore"),
                  ("Slovakia", "Slovakia"),
                  ("Slovenia", "Slovenia"),
                  ("Solomon Islands", "Solomon Islands"),
                  ("Somalia", "Somalia"),
                  ("South Africa", "South Africa"),
                  ("Spain", "Spain"),
                  ("Sri Lanka", "Sri Lanka"),
                  ("Sudan", "Sudan"),
                  ("Suriname", "Suriname"),
                  ("Swaziland", "Swaziland"),
                  ("Sweden", "Sweden"),
                  ("Switzerland", "Switzerland"),
                  ("Syria", "Syria"),
                  ("Tahiti", "Tahiti"),
                  ("Taiwan", "Taiwan"),
                  ("Tajikistan", "Tajikistan"),
                  ("Tanzania", "Tanzania"),
                  ("Thailand", "Thailand"),
                  ("Togo", "Togo"),
                  ("Tokelau", "Tokelau"),
                  ("Tonga", "Tonga"),
                  ("Trinidad &amp; Tobago", "Trinidad &amp; Tobago"),
                  ("Tunisia", "Tunisia"),
                  ("Turkey", "Turkey"),
                  ("Turkmenistan", "Turkmenistan"),
                  ("Turks &amp; Caicos Is", "Turks &amp; Caicos Is"),
                  ("Tuvalu", "Tuvalu"),
                  ("Uganda", "Uganda"),
                  ("Ukraine", "Ukraine"),
                  ("United Arab Erimates", "United Arab Emirates"),
                  ("United Kingdom", "United Kingdom"),
                  ("United States of America", "United States of America"),
                  ("Uraguay", "Uruguay"),
                  ("Uzbekistan", "Uzbekistan"),
                  ("Vanuatu", "Vanuatu"),
                  ("Vatican City State", "Vatican City State"),
                  ("Venezuela", "Venezuela"),
                  ("Vietnam", "Vietnam"),
                  ("Virgin Islands (Brit)", "Virgin Islands (Brit)"),
                  ("Virgin Islands (USA)", "Virgin Islands (USA)"),
                  ("Wake Island", "Wake Island"),
                  ("Wallis &amp; Futana Is", "Wallis &amp; Futana Is"),
                  ("Yemen", "Yemen"),
                  ("Zaire", "Zaire"),
                  ("Zambia", "Zambia"),
                  ("Zimbabwe", "Zimbabwe")]
