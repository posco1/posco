import time
from app.forms.auth import LoginForm, RegistrationForm
from app.models.user import User
from flask import request, render_template, flash, redirect, url_for, session, g
from app import app, login_manager
from flask_login import login_user, login_required, logout_user, current_user
from flask_pymongo import MongoClient
from werkzeug.security import generate_password_hash
from bson.objectid import ObjectId


@app.route("/secret")
@login_required
def secret():
    DB_NAME = 'chocobar' #to-do - this is a unique database name for every single restaurant
    DATABASE = MongoClient()[DB_NAME]
    DATABASE.create_collection('categories')
    return render_template("secret.html")


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if request.method == 'POST' and form.validate_on_submit():
        client = app.config['CLIENTS_COLLECTION'].find_one({"email": form.email.data})
        if client and User.validate_login(client['password'], form.password.data):
            session['client_id'] = str(client['_id'])
            user_obj = User(str(client['_id']))
            login_user(user_obj, remember=form.remember_me.data)
            #flash("Logged in successfully", category='success')
            return redirect(request.args.get("next") or url_for("dashboard"))
        flash("Wrong username or password", category='error')
    return render_template('login.html', title='login', form=form)


@login_manager.user_loader
def load_user(userid):
    client = app.config['CLIENTS_COLLECTION'].find_one({"_id": ObjectId(userid)})
    if not client:
        return None
    g.user_id = client['_id']
    return User(client['_id'])


@app.route("/logout")
@login_required
def logout():
    session.pop('logged_in', None)
    logout_user()
    flash("Logged out.", category='success')
    return redirect(url_for("index"))


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if request.method == 'POST' and form.validate_on_submit():
        hashed_password = generate_password_hash(form.password.data)
        # todo ensure that email is valid & unique by overriding validate
        # todo move to db_inserts /
        app.config['CLIENTS_COLLECTION'].insert({'email': form.email.data,
                                                 'password': hashed_password,
                                                 'email_confirmed': False,
                                                 'time': time.time()})
        # todo send registration mail
        # todo a wonderful example of sending this and other mails
        # in general at https://exploreflask.com/users.html
        flash("Signed up successfully ! Login to your account", category='success')
        return redirect(url_for('login'))
    return render_template('register.html', form=form)


def forgot_password():
    # https://github.com/imwilsonxu/fbone
    # see its readme for implementation using flask-mail
    # another great example of forgot password at: https://exploreflask.com/users.html
    # todo forgot password implementation
    pass
