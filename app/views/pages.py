from app import app
from flask import render_template, abort
from jinja2 import TemplateNotFound


@app.route("/")
def index():
    return render_template("pages/index.html")


@app.route('/<page>')
def show(page):
    try:
        return render_template('pages/%s.html' % page)
    except TemplateNotFound:
        abort(404)


