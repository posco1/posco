from app import app
from app.forms.pos.shift_table import ShiftTableForm
from app.models.db_queries import get_all_unbilled_running_table_sessions_with_id, \
    get_all_vacant_tables_with_id
from app.models.db_upserts import shift_table_in_db
from flask import render_template, request, flash, url_for
from flask_login import login_required
from werkzeug.utils import redirect


@app.route("/shift-table", methods=['GET', 'POST'])
@login_required
def shift_table():
    form = ShiftTableForm()
    all_running_table_sessions_with_id = get_all_unbilled_running_table_sessions_with_id()
    all_vacant_tables_with_id = get_all_vacant_tables_with_id()
    form.running_table_session_id.choices = all_running_table_sessions_with_id
    form.vacant_table_id.choices = all_vacant_tables_with_id
    if request.method == 'POST' and form.validate_on_submit():
        shift_table_in_db(form)
        flash("Table Shifted", category='success')
        return redirect(url_for('shift_table'))
    return render_template("pos/shift_table.html", form = form,
                           all_running_table_sessions_with_id=all_running_table_sessions_with_id,
                           all_vacant_tables_with_id=all_vacant_tables_with_id)
