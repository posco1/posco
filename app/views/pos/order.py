import time

from app import app
from app.forms.pos.order import OrderForm
from app.models.db_inserts import add_new_order_token, add_new_order, \
    add_new_table_session
from app.models.db_queries import get_all_active_servers_with_id, \
    get_all_active_table_names_with_id, \
    get_all_active_subcategories_with_id, \
    get_all_menu_items_with_subcategory_id_and_price, \
    get_all_printer_names_with_id, get_subcategory_id_for_menu_item_id, \
    get_printer_id_for_subcategory_id, there_exists_a_running_session_on_table, get_customer_id_for_contact_number, \
    there_exists_a_billed_but_unsettled_session_on_table, get_unbilled_running_tables_with_session_id, \
    get_table_details_for_sqaure_buttons
from app.models.db_upserts import add_order_id_to_existing_table_session, \
    update_order_id_and_table_session_id_in_order_token, \
    insert_or_update_customer_info
from app.printer.print_order_token import send_order_token_to_printers
from bson import ObjectId
from config import SERVER_ID_TAKE_AWAY, SERVER_ID_HOME_DELIVERY, NO_INFO_CUSTOMER_ID, \
    TABLE_ID_TAKE_AWAY, TABLE_ID_HOME_DELIVERY
from flask import render_template, jsonify, request, send_from_directory, g
from flask_login import login_required
from os import walk


@app.route('/order', methods=['GET', 'POST'])
@login_required
def order():
    table_details = get_table_details_for_sqaure_buttons()
    all_active_servers_with_id = get_all_active_servers_with_id()
    all_active_servers_with_id.insert(0, ('0', '- select server -'))
    all_active_table_names_with_id = get_all_active_table_names_with_id()
    all_running_tables_with_session_id = get_unbilled_running_tables_with_session_id()
    all_active_table_names_with_id.insert(-1, ('home_delivery', 'Home Delivery'))
    all_active_table_names_with_id.insert(-1, ('take_away', 'Take Away'))
    all_active_subcategories_with_id = get_all_active_subcategories_with_id()
    all_active_subcategories_with_id.insert(0, ('0', 'all subcategory'))
    all_active_printer_names_with_id = [(str(f['_id']), f['data'][0]['printer_name'])
                                        for f in get_all_printer_names_with_id() if
                                        f['data'][0]['is_active']]
    form = OrderForm()
    print(table_details)
    form.server_name.choices = all_active_servers_with_id
    form.table_name.choices = all_active_table_names_with_id
    return render_template("pos/order.html",
                           all_active_servers_with_id=all_active_servers_with_id,
                           all_running_tables_with_session_id=all_running_tables_with_session_id,
                           all_active_table_names_with_id=all_active_table_names_with_id,
                           all_active_subcategories_with_id=all_active_subcategories_with_id,
                           all_active_printer_names_with_id=all_active_printer_names_with_id,
                           table_details = table_details,
                           form=form)


@app.route('/all-menu-items-with-subcategory', methods=['POST'])
@login_required
def all_menu_items_with_subcategory():
    all_menu_items_and_subcategory_id_and_price = \
        get_all_menu_items_with_subcategory_id_and_price()
    return jsonify(result=all_menu_items_and_subcategory_id_and_price)


@app.route('/add-order', methods=['POST'])
@login_required
def add_order():
    order_details = request.json['order_details']
    order_tokens_list = split_order_into_order_tokens(order_details)
    order_token_ids_list = []
    for order_token in order_tokens_list:
        order_token_id = add_new_order_token(order_token)
        order_token_ids_list.append(str(order_token_id))
    order_type = str(order_details['order_type'])
    table_id = str(order_details['table_id'])

    split_table_name = str(order_details['split_table_name'])
    server_id = str(order_details['server_id'])
    number_of_pax = order_details['number_of_pax']
    customer_id = NO_INFO_CUSTOMER_ID
    if order_type == 'take_away':
        server_id = SERVER_ID_TAKE_AWAY
        table_id = TABLE_ID_TAKE_AWAY
    if order_type == 'home_delivery':
        server_id = SERVER_ID_HOME_DELIVERY
        table_id = TABLE_ID_HOME_DELIVERY
        contact_number = str(order_details['contact_number'])
        email_address = str(order_details['email_address'])
        customer_name = str(order_details['customer_name'])
        customer_address = str(order_details['customer_address'])
        insert_or_update_customer_info(contact_number, email_address, customer_name,
                                       customer_address)
        customer_id = get_customer_id_for_contact_number(contact_number)
    if there_exists_a_billed_but_unsettled_session_on_table(table_id, split_table_name):
        return 'cannot take order on billed but unsetlled table'
    order_id = str(add_new_order(table_id, split_table_name, server_id, number_of_pax,
                                 order_token_ids_list, order_type))

    if there_exists_a_running_session_on_table(table_id, split_table_name):
        table_session_info = add_order_id_to_existing_table_session(table_id,
                                                                    split_table_name,
                                                                    order_id)
        table_session_id = str(table_session_info.get('_id'))
    else:
        table_session_id = str(
            add_new_table_session(table_id, split_table_name, order_id, number_of_pax,
                                  server_id, order_type, customer_id))

    for order_token_id in order_token_ids_list:
        update_order_id_and_table_session_id_in_order_token(order_token_id, order_id,
                                                            table_session_id)
    send_order_token_to_printers(order_id, table_id, split_table_name)
    return 'order added'


@app.route('/ot/<filename>')
def ot_file(filename):
    return send_from_directory(app.config['TEMP_FILES_FOLDER'], filename)


@app.route('/temp-prints', methods=['GET', 'POST'])
@login_required
def temp_prints():
    f = []
    for (dirpath, dirnames, filenames) in walk(app.config['TEMP_FILES_FOLDER']):
        f.extend(filenames)
    return render_template("pos/temp_print_kots.html", all_ots_fired=f)


def split_order_into_order_tokens(order_details):
    '''
        since only quantity is allowed to be modified that too only once here is the structure used
        [item_name, latest_quantity, original_quantity, item_message ]
    '''
    all_items_ordered = order_details['items_ordered']
    order_tokens = dict()
    for item in all_items_ordered:
        item_name = item[0]
        latest_quantity = original_quantity = item[1]
        item_message = item[2]
        item_id = item[3]
        subcategory_id = get_subcategory_id_for_menu_item_id(item_id)
        printer_id = get_printer_id_for_subcategory_id(ObjectId(subcategory_id))
        if printer_id in order_tokens:
            order_tokens[printer_id]['items_in_this_token'].append(
                [item_id, item_name, latest_quantity, original_quantity,
                 item_message])
        else:
            order_tokens[printer_id] = {
                'items_in_this_token': [
                    [item_id, item_name, latest_quantity, original_quantity,
                     item_message]]}
        for printer_id in order_tokens.keys():
            order_tokens[printer_id]['printer_message'] = order_details[
                'all_printer_messages'].get(printer_id, '')
            order_tokens[printer_id]['printer_id'] = printer_id
            order_tokens[printer_id]['is_deleted'] = False
            order_tokens[printer_id]['is_modified'] = False
            order_tokens[printer_id]['modified_by'] = ''
            order_tokens[printer_id]['modification_time'] = ''
            order_tokens[printer_id]['time'] = time.time()
            order_tokens[printer_id]['user_id'] = g.user_id
            order_tokens[printer_id]['modification_justification'] = 'na'
            order_tokens[printer_id]['modification_message_to_printer'] = 'na'
    return list(order_tokens.values())
