import json
from pprint import pprint
from app.models.db_inserts import add_bill_printer_user_id_mapping, \
    add_bill_to_database
import copy
from os import walk

from app.models.db_upserts import update_bill_printer_user_id_mapping, \
    update_billing_details_in_table_session_db
from app.printer.print_bill import send_bill_to_bill_printer, \
    delete_all_files_from_temp_folder
from app import app
from app.forms.pos.billing import BillingForm
from app.models.db_queries import get_all_un_billed_running_tables_with_id, \
    get_bill_details_for_table_session, get_bill_printer_name_for_user_id
from flask import render_template, request, jsonify, send_from_directory
from flask_login import login_required


@app.route("/billing")
@login_required
def billing():
    all_un_billed_running_table_with_id = get_all_un_billed_running_tables_with_id()
    form = BillingForm()
    return render_template("pos/billing.html", form=form,
                           all_un_billed_running_table_with_id=all_un_billed_running_table_with_id)


@app.route('/generate-bill', methods=['POST'])
@login_required
def generate_bill():
    table_session_id = request.json['running_table_session_id']
    bill = get_bill_details_for_table_session(table_session_id)
    return jsonify(bill=bill)


@app.route('/print-normal-bill', methods=['POST'])
@login_required
def print_and_add_normal_bill():
    running_table_session_id = request.json['running_table_session_id']
    printer_name = request.json['printer_name']
    bill = get_bill_details_for_table_session(running_table_session_id)
    bill_id = add_bill_to_database(bill, running_table_session_id)
    # todo get printer response with actual printer and add to database only on sucessful print
    delete_all_files_from_temp_folder()
    send_bill_to_bill_printer(bill_id, bill, running_table_session_id, printer_name)
    update_billing_details_in_table_session_db(running_table_session_id, bill_id)
    # todo after adding, ask if this bill should be added to customer database
    response = {'message': 'bill added and printed', 'all_bill_ids': json.dumps([str(bill_id)]),
                'table_session_id': str(running_table_session_id)}
    return jsonify(response=response)


@app.route("/get-printer-name-for-current-user", methods=['POST'])
@login_required
def printer_current_user():
    user_id = request.json['user_id']
    printer_name = get_bill_printer_name_for_user_id(user_id)
    if printer_name is None:
        return 'no user printer mapping'
    else:
        return printer_name['printer_name']


@app.route("/save-user-printer-map", methods=['POST'])
@login_required
def save_user_printer_map():
    user_id = request.json['user_id']
    printer_name = request.json['printer_name']
    if get_bill_printer_name_for_user_id(user_id) is None:
        add_bill_printer_user_id_mapping(user_id, printer_name)
    else:
        update_bill_printer_user_id_mapping(user_id, printer_name)
    return 'user printer map saved'


@app.route('/bill/<filename>')
def bill_file(filename):
    return send_from_directory(app.config['TEMP_FILES_FOLDER'], filename)


@app.route('/temp-bill-print', methods=['GET', 'POST'])
@login_required
def temp_bill_print():
    all_bills_fired_on_printer = []
    for (dirpath, dirnames, filenames) in walk(app.config['TEMP_FILES_FOLDER']):
        all_bills_fired_on_printer.extend(filenames)
    return render_template("pos/temp_print_bill.html",
                           all_bills_fired_on_printer=all_bills_fired_on_printer)


@app.route('/print-category-split-bills', methods=['POST'])
@login_required
def print_category_split_bills():
    table_session_id = request.json['table_session_id']
    category_split_data = request.json['category_split_data']
    user_id = request.json['user_id']
    printer_name = get_bill_printer_name_for_user_id(user_id)['printer_name']
    number_of_splits = len(category_split_data)
    full_bill = get_bill_details_for_table_session(table_session_id)
    split_bills_temp = []
    for i in range(1, number_of_splits + 1):
        split_bills_temp.insert(i - 1, copy.deepcopy(full_bill))
    split_bills_final = copy.deepcopy(split_bills_temp)
    for i, item in enumerate(split_bills_temp):
        categories_for_this_bill = category_split_data['bill' + str(i + 1)]
        for key in split_bills_temp[i]['category_wise']:
            if key not in categories_for_this_bill:
                split_bills_final[i]['category_wise'].pop(key, None)
    # Now fire print for all split bills
    delete_all_files_from_temp_folder()
    all_bill_ids = []
    for i, bill in enumerate(split_bills_final):
        bill_id = add_bill_to_database(bill, table_session_id)
        all_bill_ids.insert(i, str(bill_id))
        # todo get printer response with actual printer and add to database only on sucessful print
        # todo here the printer names have been put as different to not override file. In real all bills get printed  on the same printer
        send_bill_to_bill_printer(bill_id, bill, table_session_id,
                                  printer_name + str(i))
        update_billing_details_in_table_session_db(table_session_id, bill_id)
    response = {'message': 'category split bills added and printed',
                'all_bill_ids': json.dumps(all_bill_ids),
                'table_session_id': str(table_session_id)}
    return jsonify(response=response)


@app.route('/print-item-wise-split-bills', methods=['POST'])
@login_required
def print_item_wise_split_bills():
    table_session_id = request.json['table_session_id']
    item_wise_split_bills_data = request.json['item_wise_split_bills_data']
    print(item_wise_split_bills_data)
    user_id = request.json['user_id']
    printer_name = get_bill_printer_name_for_user_id(user_id)['printer_name']
    number_of_splits = len(item_wise_split_bills_data)
    full_bill = get_bill_details_for_table_session(table_session_id)
    split_bills_temp = []
    for i in range(number_of_splits):
        split_bills_temp.insert(i, copy.deepcopy(full_bill))
    split_bills_final = copy.deepcopy(split_bills_temp)
    for i, bill in enumerate(split_bills_temp):
        for category_name in split_bills_temp[i]['category_wise']:
            for index, items in enumerate(split_bills_temp[i]['category_wise'][category_name]['all_items']):
                name = items[0]
                split_quantity = int(item_wise_split_bills_data[i][name])
                price = split_bills_final[i]['category_wise'][category_name]['all_items'][index][2]
                split_bills_final[i]['category_wise'][category_name]['all_items'][index][1] = split_quantity
                split_bills_final[i]['category_wise'][category_name]['all_items'][index][3] = split_quantity * price
    # since our bill printing ignores all items with 0 qty or categories with 0 items we need not remove them here
    # Now fire print for all split bills
    delete_all_files_from_temp_folder()
    all_bill_ids = []
    for i, bill in enumerate(split_bills_final):
        bill_id = add_bill_to_database(bill, table_session_id)
        all_bill_ids.insert(i, str(bill_id))
        # todo get printer response with actual printer and add to database only on sucessful print
        # todo here the printer names have been put as different to not override file. In real all bills get printed  on the same printer
        send_bill_to_bill_printer(bill_id, bill, table_session_id,
                                  printer_name + str(i))
        update_billing_details_in_table_session_db(table_session_id, bill_id)
    response = {'message': 'item wise split bills added and printed',
                'all_bill_ids': json.dumps(all_bill_ids),
                'table_session_id': str(table_session_id)}
    return jsonify(response=response)
