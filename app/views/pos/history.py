from app import app
from flask import render_template
from flask_login import login_required


@app.route("/history")
@login_required
def history():
    return render_template("pos/history.html")
