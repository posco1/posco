from app import app
from app.forms.pos.modify_order_token import ModifyOrderTokenForm
from app.models.db_deletes import mark_order_token_as_deleted
from app.models.db_queries import get_all_un_billed_running_tables_with_id, \
    get_all_printer_wise_order_token_ids_for_table_session, get_order_token_details, \
    get_printer_name_from_printer_id, get_order_token_details_from_order_token_id
from app.models.db_upserts import modify_order_token_in_db
from app.printer.print_adhoc_message import print_adhoc_message
from flask import render_template, request, jsonify
from flask_login import login_required


@app.route("/modify-order-token")
@login_required
def modify_order_token():
    form = ModifyOrderTokenForm()
    all_un_billed_running_tables_with_id = get_all_un_billed_running_tables_with_id()
    return render_template("pos/modify_order_token.html", form=form,
                           all_un_billed_running_tables_with_id= \
                               all_un_billed_running_tables_with_id)


@app.route('/get-all-printer-wise-order-token-ids-for-the-table-session-id',
           methods=['POST'])
@login_required
def all_printer_wise_order_token_ids_for_table_session_id():
    table_session_id = request.json['table_session_id']
    all_printer_wise_order_token_ids_for_table_session = \
        get_all_printer_wise_order_token_ids_for_table_session(table_session_id)
    return jsonify(all_printer_wise_order_token_ids_for_table_session= \
                       all_printer_wise_order_token_ids_for_table_session)


@app.route('/get-order-token-details', methods=['POST'])
@login_required
def get_order_token_details_from_given_order_token_id():
    order_token_id = request.json['order_token_id']
    order_token_details = get_order_token_details(order_token_id)
    return jsonify(order_token_details=order_token_details)


@app.route('/modify-order-token-in-db', methods=['POST'])
@login_required
def modify_order_token_db():
    order_token_id = request.json['order_token_id']
    modified_items_in_this_order_token = request.json[
        'modified_items_in_this_order_token']
    ot_modification_reason = request.json['ot_modification_reason']
    ot_modification_printer_message = request.json['ot_modification_reason']
    modify_order_token_in_db(order_token_id, modified_items_in_this_order_token,
                             ot_modification_reason, ot_modification_printer_message)
    print_ot_modification_message(order_token_id, ot_modification_printer_message)
    return 'order token modified'


@app.route('/delete-order-token-in-db', methods=['POST'])
@login_required
def delete_order_token_db():
    order_token_id = request.json['order_token_id']
    table_session_id = request.json['table_session_id']
    ot_modification_reason = request.json['ot_modification_reason']
    ot_modification_printer_message = request.json['ot_modification_reason']
    mark_order_token_as_deleted(order_token_id, ot_modification_reason,
                                ot_modification_printer_message, table_session_id)
    print_ot_deletion_message(order_token_id, ot_modification_printer_message)
    return "order token deleted"


def print_ot_deletion_message(order_token_id, ot_modification_printer_message):
    order_token_details = get_order_token_details_from_order_token_id(order_token_id)
    items_in_token = order_token_details['items_in_this_token']
    printer_name = get_printer_name_from_printer_id(order_token_details['printer_id'])
    message = 'TOKEN DELETE\n'
    message += 'OT Id: ' + order_token_id[-5:] + '\n'
    for item in items_in_token:
        item_name = item[1]
        latest_quantity = item[2] or 0
        message += item_name + ':' + str(latest_quantity) + '\n'
    message += 'reason: ' + ot_modification_printer_message + '\n'
    print_adhoc_message(message, printer_name)


def print_ot_modification_message(order_token_id, ot_modification_printer_message):
    order_token_details = get_order_token_details_from_order_token_id(order_token_id)
    items_in_token = order_token_details['items_in_this_token']
    printer_name = get_printer_name_from_printer_id(order_token_details['printer_id'])
    message = 'CANCEL\n'
    message += 'in OT Id: ' + order_token_id[-5:] + '\n'
    for item in items_in_token:
        item_name = item[1]
        latest_quantity = item[2] or 0
        original_quantity = item[3]
        if latest_quantity < original_quantity:
            message += item_name + ':' + str(
                (int(original_quantity) - int(latest_quantity))) + '\n'
    message += 'reason: ' + ot_modification_printer_message + '\n'
    print_adhoc_message(message, printer_name)
