import json
from app import app
from app.forms.pos.modify_bill import ModifyBillForm
from app.models.db_deletes import delete_if_bill_already_exists_for_table_session_id
from app.models.db_inserts import add_bill_to_database
from app.models.db_queries import get_all_un_settled_running_tables_with_id
from app.models.db_upserts import update_billing_details_in_table_session_db
from app.printer.print_bill import delete_all_files_from_temp_folder, \
    send_bill_to_bill_printer
from flask import render_template, request, jsonify
from flask_login import login_required


@app.route("/modify-bill")
@login_required
def modify_bill():
    form = ModifyBillForm()
    all_un_settled_running_tables_with_id = get_all_un_settled_running_tables_with_id()
    return render_template("pos/modify_bill.html", form=form,
                           all_un_settled_running_tables_with_id=all_un_settled_running_tables_with_id)


@app.route('/print-modified-bill', methods=['POST'])
@login_required
def print_and_add_modified_bill():
    running_table_session_id = request.json['running_table_session_id']
    printer_name = request.json['printer_name']
    bill = request.json['modified_bill']
    delete_if_bill_already_exists_for_table_session_id(running_table_session_id)
    bill_id = add_bill_to_database(bill, running_table_session_id)
    # todo get printer response with actual printer and add to database only on sucessful print
    delete_all_files_from_temp_folder()
    send_bill_to_bill_printer(bill_id, bill, running_table_session_id, printer_name)
    update_billing_details_in_table_session_db(running_table_session_id, bill_id)
    # todo after adding, ask if this bill should be added to customer database
    response = {'message': 'modified bill added and printed', 'all_bill_ids': json.dumps([str(bill_id)]),
                'table_session_id': str(running_table_session_id)}
    return jsonify(response=response)
