from app import app
from app.models.db_queries import get_all_billed_but_unsettled_table_with_id, \
    get_bill_amount_for_bill_id, \
    get_all_active_settlement_modes_with_id, get_unsettled_bill_ids_for_table_session
from app.models.db_upserts import mark_bill_as_settled_with_settlement_details, \
    mark_table_session_as_settled
from flask import render_template, request, jsonify
from flask_login import login_required


@app.route("/settlement")
@login_required
def settlement():
    all_billed_but_unsettled_table_with_id = get_all_billed_but_unsettled_table_with_id()
    all_settlement_modes_with_id =([(str(i['_id']), i['data'][0]['settlement_mode_name']) for i in
                    list(get_all_active_settlement_modes_with_id()) if i['data'][0]['is_active']])
    return render_template("pos/settlement.html",
                           all_billed_but_unsettled_table_with_id=all_billed_but_unsettled_table_with_id,
                           all_settlement_modes_with_id=all_settlement_modes_with_id)


@app.route('/get-all-unsettled-bill-ids-for-table-session', methods=['POST'])
@login_required
def all_unsettled_bill_ids_for_table_session():
    unsettled_table_session_id = request.json['unsettled_table_session_id']
    all_bill_ids = get_unsettled_bill_ids_for_table_session(unsettled_table_session_id)
    return jsonify(all_bill_ids=all_bill_ids)


@app.route('/get-bill-amount-for-bill-id', methods=['POST'])
@login_required
def bill_amount_for_bill_id():
    bill_id = request.json['bill_id']
    bill_amount = get_bill_amount_for_bill_id(bill_id)
    return jsonify(bill_amount=bill_amount)


@app.route("/settle-bill", methods=['POST'])
@login_required
def settle_bill():
    bill_id = request.json['bill_id']
    table_session_id = request.json['table_session_id']
    settlement_details = request.json['settlement_details']
    mark_bill_as_settled_with_settlement_details(bill_id, settlement_details)
    all_unsettled_bills_for_table_session = get_unsettled_bill_ids_for_table_session(table_session_id)
    if not all_unsettled_bills_for_table_session:
        mark_table_session_as_settled(table_session_id)
    return 'bill settled'
