from app import app
from app.forms.pos.merge_tables import MergeTablesForm
from app.forms.pos.shift_table import ShiftTableForm
from app.models.db_queries import get_all_unbilled_running_table_sessions_with_id, \
    get_all_vacant_tables_with_id
from app.models.db_upserts import shift_table_in_db, merge_table_in_db
from flask import render_template, request, flash, url_for
from flask_login import login_required
from werkzeug.utils import redirect


@app.route("/merge-tables", methods=['GET', 'POST'])
@login_required
def merge_tables():
    form = MergeTablesForm()
    all_un_billed_running_table_sessions_with_id = get_all_unbilled_running_table_sessions_with_id()
    all_un_billed_running_table_sessions_with_id.insert(0, ('0', '--all un-billed running tables--'))
    form.absorbed_table_session_id.choices = all_un_billed_running_table_sessions_with_id
    form.absorber_table_session_id.choices = all_un_billed_running_table_sessions_with_id
    if request.method == 'POST' and form.validate_on_submit():
        merge_table_in_db(form)
        flash("Tables Merged", category='success')
        return redirect(url_for('merge_tables'))
    return render_template("pos/merge-tables.html", form=form,
                           all_un_billed_running_table_sessions_with_id=all_un_billed_running_table_sessions_with_id,
                           )
