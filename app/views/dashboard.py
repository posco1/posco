from os import walk
from app import app
from app.models.db_queries import get_table_details_for_sqaure_buttons, \
    get_all_printer_names_with_id, get_unbilled_running_table_details_for_dashboard_table, \
    get_all_billed_but_unsettled_table_with_id, \
    get_unsettled_bill_details_for_dashboard
from app.printer.print_adhoc_message import print_adhoc_message
from flask import render_template, request, send_from_directory
from flask_login import login_required


@app.route("/dashboard")
@login_required
def dashboard():
    table_details_for_dashboard_squares = get_table_details_for_sqaure_buttons()
    all_active_printer_names_with_id = [(str(f['_id']), f['data'][0]['printer_name'])
                                        for f in get_all_printer_names_with_id() if
                                        f['data'][0]['is_active']]
    all_running_table_details = get_unbilled_running_table_details_for_dashboard_table()
    unsettled_bill_details = get_unsettled_bill_details_for_dashboard()
    return render_template("dashboard.html",
                           table_details_for_dashboard_squares=table_details_for_dashboard_squares,
                           unsettled_bill_details = unsettled_bill_details,
                           all_printers_with_id = all_active_printer_names_with_id,
                           all_running_table_details=all_running_table_details)


@app.route('/print-adhoc-message', methods=['POST'])
@login_required
def print_adhoc_message_on_printer():
    message = request.json['message']
    printer_name = request.json['printer_name']
    print_adhoc_message(message, printer_name)
    return 'adhoc message printed'


@app.route('/adhoc/<filename>')
def adhoc_file(filename):
    return send_from_directory(app.config['TEMP_FILES_FOLDER'], filename)


@app.route('/adhoc-temp-prints', methods=['GET', 'POST'])
@login_required
def adhoc_temp_prints():
    f = []
    for (dirpath, dirnames, filenames) in walk(app.config['TEMP_FILES_FOLDER']):
        f.extend(filenames)
    return render_template("pos/temp_print_adhoc.html", all_adhocs_fired=f)
