from app import app
from app.forms.masters.incentive import IncentiveForm
from app.models.db_deletes import delete_incentive_from_db
from app.models.db_inserts import add_new_incentive
from app.models.db_queries import get_all_active_incentives
from app.models.db_upserts import edit_incentive_from_db
from flask import render_template, request, flash, url_for
from flask_login import login_required
from werkzeug.utils import redirect


@app.route('/incentive', methods=['GET', 'POST'])
@login_required
def incentive():
    form = IncentiveForm()
    all_active_incentives = get_all_active_incentives()
    if request.method == 'POST' and form.validate_on_submit():
        add_new_incentive(form)
        flash("Incentive added", category='success')
        return redirect(url_for('incentive'))
    return render_template("masters/incentive.html", form=form,
                           all_active_incentives=all_active_incentives)


@app.route('/delete-incentive', methods=['POST'])
@login_required
def delete_incentive():
    incentive_to_be_deleted = request.json['incentive_to_be_deleted']
    delete_incentive_from_db(incentive_to_be_deleted)
    return 'incentive deleted successfully'
    # todo check if incentive is used in menu items before delete
    '''if there_are_menu_items_in_incentive(incentive_to_be_deleted):
        return 'cannot delete as incentive has associated menu items'
    else:
        delete_incentive_from_db(incentive_to_be_deleted)
        return 'incentive deleted successfully'
    '''


@app.route('/edit-incentive', methods=['POST'])
@login_required
def edit_incentive_percent():
    incentive_0 = request.json['incentive_0']
    incentive_1 = request.json['incentive_1']
    edit_incentive_from_db(incentive_0, incentive_1)
    return 'incentive edited successfully'
