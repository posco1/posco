from app import app
from app.forms.masters.table_name import TableNameForm
from app.models.db_deletes import delete_table_name_from_db
from app.models.db_inserts import add_new_table_name
from app.models.db_queries import get_all_active_table_names
from app.models.db_upserts import edit_table_name_from_db
from flask import render_template, request, flash, url_for
from flask_login import login_required
from werkzeug.utils import redirect


@app.route('/table-name', methods=['GET', 'POST'])
@login_required
def table_name():
    form = TableNameForm()
    all_active_table_names = get_all_active_table_names()
    if request.method == 'POST' and form.validate_on_submit():
        add_new_table_name(form)
        flash("Table added", category='success')
        return redirect(url_for('table_name'))
    return render_template("masters/table_name.html", form=form,
                           all_active_table_names=all_active_table_names)


@app.route('/delete-table-name', methods=['POST'])
@login_required
def delete_table_name():
    table_name_to_be_deleted = request.json['table_name_to_be_deleted']
    # todo delete not allowed if table is taking active order
    delete_table_name_from_db(table_name_to_be_deleted)
    return 'table name deleted successfully'


@app.route('/edit-table-name', methods=['POST'])
@login_required
def edit_table_name():
    table_name_0 = request.json['table_name_0']
    table_name_1 = request.json['table_name_1']
    edit_table_name_from_db(table_name_0, table_name_1)
    return 'table name edited successfully'
