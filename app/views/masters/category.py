from app import app
from app.forms.masters.category import CategoryForm
from app.models.db_deletes import delete_category_from_db
from app.models.db_inserts import add_new_category
from app.models.db_queries import category_has_subcategories, \
    get_all_categories_with_applicable_vat
from app.models.db_upserts import edit_category_from_db
from flask import render_template, request, flash, url_for
from flask_login import login_required
from werkzeug.utils import redirect


@app.route('/category', methods=['GET', 'POST'])
@login_required
def category():
    form = CategoryForm()
    if request.method == 'POST' and form.validate_on_submit():
        add_new_category(form)
        flash("Category Added successfully", category='success')
        return redirect(url_for('category'))
    all_active_categories_with_vat = [
        (c['data'][0]['category_name'], c['data'][0]['applicable_vat'])
        for c in get_all_categories_with_applicable_vat() if c['data'][0]['is_active']]
    return render_template("masters/category.html", form=form,
                           all_active_categories_with_vat=all_active_categories_with_vat)


@app.route('/delete-category', methods=['POST'])
@login_required
def delete_category():
    category_to_be_deleted = request.json['category_to_be_deleted']
    vat = request.json['vat_for_category_to_be_deleted']
    if category_has_subcategories(category_to_be_deleted):
        return 'cannot delete as category has subcategories'
    else:
        delete_category_from_db(category_to_be_deleted, vat)
        return 'category deleted successfully'


@app.route('/edit-category', methods=['POST'])
@login_required
def edit_category():
    category_name_0 = request.json['category_0']
    category_name_1 = request.json['category_1']
    vat_1 = request.json['vat_1']
    edit_category_from_db(category_name_0, category_name_1, vat_1)
    return 'category edited successfully'
