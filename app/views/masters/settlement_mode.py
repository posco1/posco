from app import app
from app.forms.masters.settlement_mode import SettlementModeForm
from app.models.db_deletes import delete_settlement_mode_from_db
from app.models.db_inserts import add_new_settlement_mode
from app.models.db_queries import get_all_active_settlement_mode, \
    get_active_settlement_mode_id_from_settlement_mode_name
from app.models.db_upserts import edit_settlement_mode_from_db
from flask import render_template, request, flash, url_for
from flask_login import login_required
from werkzeug.utils import redirect


@app.route('/settlement-mode', methods=['GET', 'POST'])
@login_required
def settlement_mode():
    form = SettlementModeForm()
    if request.method == 'POST' and form.validate_on_submit():
        add_new_settlement_mode(form)
        flash("settlement mode added", category='success')
        return redirect(url_for('settlement_mode'))
    all_active_settlement_mode = [c['data'][0]['settlement_mode_name'] for c in
                                  get_all_active_settlement_mode() if
                                  c['data'][0]['is_active']]
    return render_template("masters/settlement_mode.html", form=form,
                           all_active_settlement_mode=all_active_settlement_mode)


@app.route('/delete-settlement-mode', methods=['POST'])
@login_required
def delete_settlement_mode():
    settlement_mode_to_be_deleted = request.json['settlement_mode_to_be_deleted']
    delete_settlement_mode_from_db(settlement_mode_to_be_deleted)
    return 'settlement mode deleted successfully'


@app.route('/edit-settlement-mode', methods=['POST'])
@login_required
def edit_settlement_mode():
    settlement_mode_name_0 = request.json['settlement_mode_0']
    settlement_mode_name_1 = request.json['settlement_mode_1']
    edit_settlement_mode_from_db(settlement_mode_name_0, settlement_mode_name_1)
    return 'settlement mode edited successfully'
