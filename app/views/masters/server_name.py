from app import app
from app.forms.masters.server_name import ServerNameForm
from app.models.db_deletes import delete_server_name_from_db
from app.models.db_inserts import add_new_server_name
from app.models.db_queries import get_all_active_server_names
from app.models.db_upserts import edit_server_name_from_db
from flask import render_template, request, flash, url_for
from flask_login import login_required
from werkzeug.utils import redirect


@app.route('/server-name', methods=['GET', 'POST'])
@login_required
def server_name():
    form = ServerNameForm()
    all_server_names = get_all_active_server_names()
    if request.method == 'POST' and form.validate_on_submit():
        add_new_server_name(form)
        flash("Server added", category='success')
        return redirect(url_for('server_name'))
    return render_template("masters/server_name.html", form=form,
                           all_server_names=all_server_names)


@app.route('/delete-server-name', methods=['POST'])
@login_required
def delete_server_name():
    server_name_to_be_deleted = request.json['server_name_to_be_deleted']
    delete_server_name_from_db(server_name_to_be_deleted)
    return 'server name deleted successfully'


@app.route('/edit-server-name', methods=['POST'])
@login_required
def edit_server_name():
    server_name_0 = request.json['server_name_0']
    server_name_1 = request.json['server_name_1']
    edit_server_name_from_db(server_name_0, server_name_1)
    return 'server name edited successfully'
