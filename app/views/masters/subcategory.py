from app import app
from app.forms.masters.subcategory import SubCategoryForm
from app.models.db_deletes import delete_subcategory_from_db
from app.models.db_inserts import add_new_subcategory
from app.models.db_queries import get_all_categories_with_id, \
    get_all_printer_names_with_id, get_all_active_subcategory_category_printer_maps, \
    get_all_printers_with_id
from app.models.db_upserts import edit_subcategory_from_db
from flask import render_template, request, flash, url_for, jsonify
from flask_login import login_required
from werkzeug.utils import redirect


@app.route('/subcategory', methods=['GET', 'POST'])
@login_required
def subcategory():
    all_active_categories_with_id = [(str(f['_id']), f['data'][0]['category_name'])
                                     for f in
                                     get_all_categories_with_id() if
                                     f['data'][0]['is_active']]
    all_active_categories_with_id.insert(0, ('0', 'select category'))
    all_active_printer_names_with_id = [(str(f['_id']), f['data'][0]['printer_name'])
                                        for f in get_all_printer_names_with_id() if
                                        f['data'][0]['is_active']]
    all_active_printer_names_with_id.insert(0, ('0', 'select printer'))
    form = SubCategoryForm()
    form.category_id.choices = all_active_categories_with_id
    form.printer_id.choices = all_active_printer_names_with_id
    if request.method == 'POST' and form.validate_on_submit():
        add_new_subcategory(form)
        flash("Sub Category Added successfully", category='success')
        return redirect(url_for('subcategory'))
    all_active_subcategory_category_printer_maps = get_all_active_subcategory_category_printer_maps()
    return render_template("masters/subcategory.html", form=form,
                           all_active_subcategory_category_printer_maps=all_active_subcategory_category_printer_maps,
                           all_active_categories_with_id=all_active_categories_with_id,
                           all_active_printer_names_with_id=all_active_printer_names_with_id)


@app.route('/delete-subcategory', methods=['POST'])
@login_required
def delete_subcategory():
    subcategory_name_to_be_deleted = request.json['subcategory_name_to_be_deleted']
    # todo prevent delete if it has active menu items after menu items has been defined
    '''if there_are_menu_items_in_subcategory(subcategory_name_to_be_deleted):
        return 'cannot delete as subcategory has associated menu items'
    else:'''
    delete_subcategory_from_db(subcategory_name_to_be_deleted)
    return 'subcategory deleted successfully'


@app.route('/edit-subcategory', methods=['POST'])
@login_required
def edit_subcategory():
    subcategory0 = request.json['subcategory0']
    subcategory1 = request.json['subcategory1']
    category1 = request.json['category1']
    printer1 = request.json['printer1']
    edit_subcategory_from_db(subcategory0, subcategory1, category1, printer1)
    return 'subcategory edited successfully'


@app.route('/get-all-active-categories-with-id', methods=['GET'])
@login_required
def get_all_active_categories_with_their_ids():
    return jsonify([(str(i['_id']), i['data'][0]['category_name']) for i in
                    list(get_all_categories_with_id()) if i['data'][0]['is_active']])


@app.route('/get-all-active-printers-with-id', methods=['GET'])
@login_required
def get_all_active_printers_with_their_ids():
    return jsonify([(str(i['_id']), i['data'][0]['printer_name']) for i in
                    list(get_all_printers_with_id()) if i['data'][0]['is_active']])
