from app import app
from app.forms.masters.configurations import ConfigurationsForm
from app.models.db_queries import get_configurations_from_db
from app.models.db_upserts import edit_category_from_db, update_configurations
from app.views.utils import countries_list
from flask import render_template, request, flash, url_for
from flask_login import login_required
from werkzeug.utils import redirect


@app.route('/configurations', methods=['GET', 'POST'])
@login_required
def configurations():
    form = ConfigurationsForm()
    form.country.choices = countries_list
    if request.method == 'POST' and form.validate_on_submit():
        update_configurations(form)
        flash("Configurations Updated", category='success')
        return redirect(url_for('configurations'))
    configurations_from_db = get_configurations_from_db()
    return render_template("masters/configuration.html", form=form,
                           configurations=configurations_from_db,
                           countries_list=countries_list)


