from app.models.db_upserts import edit_menu_item_from_db
from app.views.utils import random_filename
import os
from app import app
from app.forms.masters.menu import MenuForm
from app.models.db_deletes import delete_menu_item_from_db
from app.models.db_inserts import add_new_item_to_menu
from app.models.db_queries import get_all_active_subcategories_with_id, \
    get_all_active_incentives_with_id, get_all_menu_items
from flask import render_template, request, flash, url_for, jsonify, \
    send_from_directory
from flask_login import login_required
from werkzeug.utils import redirect, secure_filename


@app.route('/menu', methods=['GET', 'POST'])
@login_required
def menu():
    all_menu_items = get_all_menu_items()
    file_size_ok = True
    all_active_subcategories_with_id = get_all_active_subcategories_with_id()
    all_active_subcategories_with_id.insert(0, ('0', '-subcategory-'))
    all_active_incentives_with_id = get_all_active_incentives_with_id()
    all_active_incentives_with_id.insert(0, ('0', '-incentive -'))
    form = MenuForm()
    form.subcategory_id.choices = all_active_subcategories_with_id
    form.incentive_id.choices = all_active_incentives_with_id
    if request.content_length is not None and request.content_length > 535769:
        form.item_image.errors = []
        form.item_image.errors.append('file size too large.max 500kb allowed')
        file_size_ok = False
    if request.method == 'POST' and form.validate_on_submit() and file_size_ok:
        file = request.files['item_image']
        if file.filename is not '':
            filename = secure_filename(file.filename)
            random_file_name = random_filename(filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], random_file_name))
        else:
            random_file_name = 'default.png'
        add_new_item_to_menu(form, random_file_name)
        flash("Item added to menu", category='success')
        return redirect(url_for('menu'))
    return render_template("masters/menu.html", form=form,
                           all_active_subcategories_with_id=all_active_subcategories_with_id,
                           all_active_incentives_with_id=all_active_incentives_with_id,
                           all_menu_items=all_menu_items)


@app.route('/delete-menu-item', methods=['POST'])
@login_required
def delete_menu_item():
    menu_item_to_be_deleted = request.json['menu_item_to_be_deleted']
    delete_menu_item_from_db(menu_item_to_be_deleted)
    # todo delete menu image from hard disk
    return 'menu item deleted successfully'


@app.route('/delete-multiple-menu-item', methods=['POST'])
@login_required
def delete_multiple_menu_item():
    for menu_item_to_be_deleted in request.form.getlist('data[]'):
        delete_menu_item_from_db(menu_item_to_be_deleted)
    # todo delete menu image from hard disk
    return 'selected menu items removed'


@app.route('/edit-menu-item', methods=['POST'])
@login_required
def edit_menu_item():
    menu_item_0 = request.form['menuItem0']
    menu_item_1 = request.form['menuItem1']
    price_1 = request.form['price1']
    subcategory_1 = request.form['subcategory1']
    incentives_1 = request.form['incentives1']
    image0 = request.form['image0']
    try:
        new_image_file = request.files['new_image_file']
    except:
        new_image_file = None
    edit_menu_item_from_db(menu_item_0, menu_item_1, price_1, subcategory_1,
                           incentives_1, new_image_file, image0)
    return 'menu item edited successfully'


@app.route('/get-all-subcategories-with-id', methods=['GET'])
@login_required
def get_all_subcategories_with_their_ids():
    return jsonify(list(get_all_active_subcategories_with_id()))


@app.route('/get-all-incentives-with-id', methods=['GET'])
@login_required
def get_all_incentives_with_their_ids():
    return jsonify(list(get_all_active_incentives_with_id()))


@app.route('/menu_item_images/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)
