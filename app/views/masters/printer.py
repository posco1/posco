from app import app
from app.forms.masters.printer import PrinterForm
from app.models.db_deletes import delete_printer_from_db
from app.models.db_inserts import add_new_printer
from app.models.db_queries import get_all_active_printers, \
    get_active_printer_id_from_printer_name, is_printer_id_linked_to_active_subcategory
from app.models.db_upserts import edit_printer_from_db
from flask import render_template, request, flash, url_for
from flask_login import login_required
from werkzeug.utils import redirect


@app.route('/printer', methods=['GET', 'POST'])
@login_required
def printer():
    form = PrinterForm()
    if request.method == 'POST' and form.validate_on_submit():
        add_new_printer(form)
        flash("printer added", category='success')
        return redirect(url_for('printer'))
    all_active_printers = [c['data'][0]['printer_name'] for c in
                           get_all_active_printers() if c['data'][0]['is_active']]
    return render_template("masters/printer.html", form=form,
                           all_active_printers=all_active_printers)


@app.route('/delete-printer', methods=['POST'])
@login_required
def delete_printer():
    printer_to_be_deleted = request.json['printer_to_be_deleted']
    printer_id_to_be_deleted = get_active_printer_id_from_printer_name(printer_to_be_deleted)
    if is_printer_id_linked_to_active_subcategory(printer_id_to_be_deleted):
        return 'cannot delete printer as it is associated with an active subcategory'
    else:
        delete_printer_from_db(printer_to_be_deleted)
        return 'printer deleted successfully'


@app.route('/edit-printer', methods=['POST'])
@login_required
def edit_printer():
    printer_name_0 = request.json['printer_0']
    printer_name_1 = request.json['printer_1']
    edit_printer_from_db(printer_name_0, printer_name_1)
    return 'printer edited successfully'
