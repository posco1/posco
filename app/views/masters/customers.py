import json
from app import app
from app.forms.masters.customers import CustomersForm
from app.models.db_inserts import add_new_customer
from app.models.db_queries import get_customer_info_from_contact_number, \
    get_all_customers
from app.models.db_upserts import edit_customer_from_db
from flask import jsonify, request, url_for, flash, render_template
from flask_login import login_required
from werkzeug.utils import redirect


@app.route('/customers', methods=['GET', 'POST'])
@login_required
def customers():
    form = CustomersForm()
    if request.method == 'POST' and form.validate_on_submit():
        add_new_customer(form)
        flash("Customer Added successfully", category='success')
        return redirect(url_for('customers'))
    all_customers = get_all_customers()
    return render_template("masters/customers.html", form=form,
                           all_customers=all_customers)


@app.route('/get-customer-info-from-contact-number', methods=['POST'])
@login_required
def get_customer_info_frm_contact_number():
    contact_number = request.json['contact_number']
    customer_info = get_customer_info_from_contact_number(contact_number)
    if customer_info is None:
        return jsonify(response='None')
    else:
        response = {"customer_address": customer_info['customer_address'],
                    "email_address": customer_info['email_address'],
                    "customer_name": customer_info['customer_name']
                    }
    return jsonify(response=response)


@app.route('/edit-customer', methods=['POST'])
@login_required
def edit_customer():
    contact_number_0 = request.json['contact_number_0']
    customer_name_1 = request.json['customer_name_1']
    email_address_1 = request.json['email_address_1']
    contact_number_1 = request.json['contact_number_1']
    customer_address_1 = request.json['customer_address_1']
    edit_customer_from_db(contact_number_0, customer_name_1, email_address_1,
                          contact_number_1, customer_address_1)
    return 'customer edited successfully'
