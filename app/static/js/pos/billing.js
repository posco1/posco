var bill;
$('#all_running_table_drop_down').val('0');
$("#all_running_table_drop_down").focus();

$(document).on( 'shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
   var selected_tab = $(e.target).attr("href") 
    var running_table_session_id = $('#all_running_table_drop_down').val();
    getBillData(running_table_session_id, selected_tab);

});

$("#all_running_table_drop_down").change(function() {
    bill = undefined;
    var running_table_session_id = this.value;
    var selected_tab = $('.dropdown-menu > li.active a').attr("href");
    getBillData(running_table_session_id, selected_tab);
});

var selected_tab_title = {"#no_split_tab":"", "#category_wise_split_tab": "split bill category wise",  "#percentage_wise_split_tab": "split bill percent wise",
 "#item_wise_split_tab": "split bill item wise"}

function getBillData(running_table_session_id, selected_tab) {
    $('#subtitle').html(selected_tab_title[selected_tab]);
    if(running_table_session_id=='0'){$(".tab-pane").addClass('hidden'); return; } else{$(".tab-pane").removeClass('hidden');}
    var data = {};
    data['running_table_session_id'] = running_table_session_id;
    var url = '/generate-bill';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            bill = data.bill;
            callDelegator(selected_tab);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });
}

function callDelegator(selected_tab){
    $('#subtitle').html(selected_tab_title[selected_tab]);
    switch (selected_tab) {
    case undefined:
        showNoSplitBillTabContent();
        break;
    case "#no_split_tab":
        showNoSplitBillTabContent();
        break;
    case "#category_wise_split_tab":
        showCategoryWiseSplitBillTabContent();
        break;
    case "#percentage_wise_split_tab":
        showPercentageWiseSplitBillTabContent();
        break;
    case "#item_wise_split_tab":
        showItemWiseSplitBillTabContent();
        break;
}
    
}
/*

No Split - Normal Bill Section

*/

function showNoSplitBillTabContent() {
    $('#bill_preview_table #mtbody tr').slice(1).remove(); 
    $(".show_bill").removeClass('hidden');
    var service_tax_rate = bill.service_tax_rate;
    var service_charge_rate = bill.service_charge_rate;
    var order_type = bill.order_type;
    if(order_type == 'take_away' || order_type =='home_delivery'){service_charge_rate = 0;}
    var home_delivery_charge;
    if(order_type == 'home_delivery') {home_delivery_charge = parseFloat(bill.home_delivery_charge); } else {home_delivery_charge = 0;}
    var total_bill = 0
    $.each(bill.category_wise, function(category_name, category) {
        var category_total = 0;
        $.each(category.all_items, function(i, item) {
            if(item[1]==0){return false;}
            $('#bill_preview_table #mtbody tr:last').after('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td><td>' + roundTo2(item[2]) + '</td><td class="ta-right">' + roundTo2(item[3]) + '</td></tr>');
            category_total = category_total + item[3];
        });
        $('#bill_preview_table > #mtbody tr:last').after('<tr><td class="ta-right" colspan="3">Total for ' + category_name + '</td><td class="ta-right">' + roundTo2(category_total) + '</td></tr>');
        var service_charge_amount = calculateServiceCharge(category_total, service_charge_rate);
        var vat_amount = calculateVat(category_total, category.vat_rate);
        var service_tax_amount = calculateServiceTax(category_total, service_tax_rate);
        if(order_type == 'dine-in') {
        $('#bill_preview_table #mtbody tr:last').after('<tr><td class="ta-right" colspan="3">Service charge @' + service_charge_rate + '%</td><td class="ta-right">' + roundTo2(service_charge_amount) + '</td></tr>');}
        $('#bill_preview_table #mtbody tr:last').after('<tr><td class="ta-right" colspan="3">Vat @' + category.vat_rate + '%</td><td class="ta-right">' + roundTo2(vat_amount) + '</td></tr>');
        $('#bill_preview_table #mtbody tr:last').after('<tr><td class="ta-right" colspan="3">Govt. Service Tax @' + service_tax_rate + '%</td><td class="ta-right">' + roundTo2(service_tax_amount) + '</td></tr>');
        total_bill = total_bill + category_total + service_tax_amount + vat_amount + service_charge_amount;
    });
    if(order_type == 'home_delivery') {
        $('#bill_preview_table #mtbody tr:last').after('<tr><td class="ta-right" colspan="3">Home Delivery Charge</td><td class="ta-right">' + roundTo2(home_delivery_charge) + '</td></tr>');
        total_bill = total_bill + home_delivery_charge;
        
    }
    $('#bill_preview_table > #mtbody tr:last').after('<tr><td class="ta-right" colspan="3"><strong>Bill Total:</strong></td><td class="ta-right">' + roundTo2(Math.round(total_bill)) + '</td></tr>');
    $('#placeholder_for_normal_bill_print_button').html('<button id="print_normal_bill_button" class="btn btn-primary">Print Bill</button>');
    }


$('#placeholder_for_normal_bill_print_button').click(sendNoSplitBillPrintRequest);

function sendNoSplitBillPrintRequest() {
  var running_table_session_id = $("#all_running_table_drop_down").val();
  var printer_name = printerNameForCurrentUser;
  if(running_table_session_id=='0'){return;}
    var data = {};
    data['running_table_session_id'] = running_table_session_id;
    data['printer_name'] = printer_name;
    var url = '/print-normal-bill';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            if(data['response']['message']=='bill added and printed'){
                    var all_bill_ids = data['response']['all_bill_ids'];
                    var table_session_id = data['response']['table_session_id']
                    sessionStorage.noSplitBillPrinted = 'true';
                    window.location.reload();    

            }
            
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });
}


$( function () {
    if ( sessionStorage.noSplitBillPrinted == 'true') {
        bootbox.alert('Bill Printed. Temporarily Visit <a target="_blank" href="/temp-bill-print">here</a> to see the bill.<br> Next-- Would you like to capture Customer Details Yes or No Button Comes Here'); 
        sessionStorage.noSplitBillPrinted = 'false';
    }
} );


function calculateServiceCharge(amount, service_charge_rate) {
    return amount * service_charge_rate / 100;
}

function calculateVat(amount, vat_rate) {
    return amount * vat_rate / 100;
}
function calculateServiceTax(amount, service_tax_rate) {
    return amount * service_tax_rate / 100;
}


function roundTo2(num) {
    return parseFloat(Math.round(num * 100) / 100).toFixed(2);
}


/*

Bill Printer Settings Section

*/

var printerInputHtml;
$('#bill-printer-settings').click(onPrinterSettingsClicked);

function onPrinterSettingsClicked() {
    printerInputHtml = generateHTMLforPrinterDropDown();
    showPrinterChooserDialog();
    $('#printer_name').val(printerNameForCurrentUser);
}


function showPrinterChooserDialog() {
    var msg = printerInputHtml || generateHTMLforPrinterDropDown();
    bootbox.dialog({
        title: "<small>Print Bills on:</small>",
        message: msg,
        buttons: {
            success: {
                label: "Save",
                className: "btn-success",
                callback: function() {
                    var printer_name = $('#printer_name').val();
                    onPrinterChooserSaveButtonClicked(printer_name);

                }
            }
        }
    });

}

function onPrinterChooserSaveButtonClicked(printer_name) {
    if ((printer_name == undefined) || (printer_name == '')) {
        showPrinterChooserDialog();
        bootbox.alert('you must select a printer to proceed');
    } else {
        sendPrinterUserMapSaveRequest(printer_name);
        window.location.reload(true);
    }
}


function sendPrinterUserMapSaveRequest(printer_name) {
    var data = {};
    data['user_id'] = $('#user-id').html();
    data['printer_name'] = printer_name;
    var url = '/save-user-printer-map';
    $.ajax({
        url: url,
        type: "POST",
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(data, null, '\t'),
        success: function(data, textStatus, jqXHR) {
            if (data == "user printer map saved") {
                bootbox.alert('printer settings saved');
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });



}

var printerNameForCurrentUser;

$(document).ready(function($) {
    var user_id = $('#user-id').html();
    printerNameForCurrentUser = getprinterNameForCurrentUser(user_id);
});


function generateHTMLforPrinterDropDown() {
    temp = '<div class="row"><div class="col-md-3"></div><div class="col-md-6"><form class="form-horizontal"> ' +
        '<div class="form-group"><input type="text" class="form-control input-md" value="'+printerNameForCurrentUser +'" id="printer_name"></input>';
    temp = temp + '</div></form> </div> </div>';
    return temp;
}

function getprinterNameForCurrentUser(user_id) {
    var data = {};
    data['user_id'] = user_id;
    var url = '/get-printer-name-for-current-user';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            if (data == 'no user printer mapping') {
                showPrinterChooserDialog();
            } else {
                printerNameForCurrentUser = data;
                return printerNameForCurrentUser;
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });
}


/* 

Split Bills By Category

*/


function showCategoryWiseSplitBillTabContent(){
    $("#category_wise_split_table").html("");
    $('#print_category_split_bills_button').addClass('hidden'); 
    $('#number_of_category_wise_splits').find("option:gt(0)").remove();
    var number_of_categories = countNumberOfCategories(bill['category_wise']);
    if(number_of_categories<2){
        $('#number_of_category_wise_splits option:last').after($('<option value="-1">Bill Has Single Category. Nothing to Split</option>'));
        
    }
    else {
        
        for(i=2; i<=number_of_categories; i++){
        $('#number_of_category_wise_splits option:last').after($('<option value="'+i+'">'+i+'</option>'));}}
}

$("#number_of_category_wise_splits").change(function() {
    var number_of_categories = countNumberOfCategories(bill['category_wise']);
    var number_of_category_wise_splits = this.value;
    createCategoryWiseSplitTable(number_of_categories, number_of_category_wise_splits);
});



function createCategoryWiseSplitTable(rows, cols){
    $("#category_wise_split_table").html("");
    if(cols <= 1){ $('#print_category_split_bills_button').addClass('hidden'); return;  }
    categories_array = getCategoriesArray();
    var table = $('<table></table>').attr({ class: "table table-bordered table-hover table-condensed m-t-20" });
    $('<tbody></tbody>').appendTo(table);
    var row = $('<tr></tr>').appendTo(table);
    for (var j = 0; j <= cols; j++) {
        if(j==0){$('<th></th>').appendTo(row);} else {$('<th></th>').text("Bill " + j ).appendTo(row);}
    } 
    var tr = [];
    for (var i = 0; i < rows; i++) {
        var row = $('<tr></tr>').appendTo(table);
        for (var j = 0; j <= cols; j++) {
            if(j==0){$('<td></td>').text(categories_array[i]).appendTo(row); } 
            else {
                var radioBtn = '<input type="radio" checked="checked" name="'+categories_array[i]+'" value="rbtn'+(i+1)+''+j+'" />';
                $('<td></td>').html(radioBtn).appendTo(row); 
            }
        }
     }
    table.appendTo("#category_wise_split_table");
    $('#print_category_split_bills_button').removeClass('hidden'); 

}

var categorySplitErrorMessage;

$('#print_category_split_bills_button').on('click', function(event) {
    categorySplitErrorMessage = null;
    var number_of_categories = countNumberOfCategories(bill['category_wise']);
    var number_of_category_wise_splits = $("#number_of_category_wise_splits").val();
    categories_array = getCategoriesArray();
    category_split_data = {};
    for(i=1; i<=number_of_category_wise_splits; i++){
        category_split_data["bill"+i] = [];

    }
    for(col=1; col<=number_of_category_wise_splits; col++){
         $.each(categories_array, function(row, category_name) {
            if( $("input:radio[value=rbtn"+ (row+1) + ''+ col+"]").is(':checked')) {
                            category_split_data['bill'+(col)].push(category_name);
            }
        });

    }
    if(validateSplitData(category_split_data)) {sendCategorySplitBillPrintRequest(category_split_data);}
        else{bootbox.alert(categorySplitErrorMessage);}
});


function sendCategorySplitBillPrintRequest(category_split_data){
    var data = {};
    var table_session_id = $('#all_running_table_drop_down').val();
      data['table_session_id'] = table_session_id;
      data['category_split_data'] = category_split_data;
      data['user_id'] = $('#user-id').html();
      var url = '/print-category-split-bills';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
            if(data['response']['message']=="category split bills added and printed"){
                    var all_bill_ids = data['response']['all_bill_ids'];
                    var table_session_id = data['response']['table_session_id']
                    sessionStorage.categorySplitBillsPrinted = 'true';
                    window.location.reload();    
            }
        },
          error: function(jqXHR, textStatus, errorThrown) {
        bootbox.alert(errorThrown);
        }
    });
}



$( function () {

    if ( sessionStorage.categorySplitBillsPrinted == 'true') {
        bootbox.alert('Bill Printed. Temporarily Visit <a target="_blank" href="/temp-bill-print">here</a> to see the bill.<br> Next-- Would you like to capture Customer Details Yes or No Button Comes Here'); 
        sessionStorage.categorySplitBillsPrinted = 'false';
    }
} );

function validateSplitData(category_split_data){
    for(bill_name in category_split_data){
        if(category_split_data[bill_name].length == 0){categorySplitErrorMessage = "No categories added to "+ bill_name; return false; }
    }
    return true;
}

function countNumberOfCategories(obj) {
    var count = 0;
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            ++count;
    }
    return count;
}

function getCategoriesArray() {
    var categories_array = [];
    for(var prop in bill['category_wise']) {
        if(bill['category_wise'].hasOwnProperty(prop))
            categories_array.push(prop);
    }
    return categories_array;
}


/* 

Split Bills By Items

*/


function showItemWiseSplitBillTabContent(){
    $("#item_wise_split_table").html("");
    $('#print_item_split_bills_button').addClass('hidden');
    $('#number_of_item_wise_splits').find("option:gt(0)").remove();
    var number_of_allowed_splits = get_number_of_allowed_splits();
    for(i=2; i<=number_of_allowed_splits; i++){
        $('#number_of_item_wise_splits option:last').after($('<option value="'+i+'">'+i+'</option>'));
    }

}

function get_number_of_allowed_splits(){
    var all_items_in_the_bill = get_all_items_in_the_bill();
    var number_of_items = 0 
    $.each(all_items_in_the_bill, function(index, value){
        number_of_items = number_of_items + parseInt(value[1]);
    });
    return number_of_items;
}


$("#number_of_item_wise_splits").change(function() {
    var all_items_in_the_bill = get_all_items_in_the_bill();
    var number_of_items = all_items_in_the_bill.length;
    var number_of_bills = parseInt(this.value);
    createItemWiseSplitTable(all_items_in_the_bill, number_of_items, number_of_bills);
});


function get_all_items_in_the_bill(){
    var items_array = [];
    for(var category_name in bill['category_wise']) {
        if(bill['category_wise'].hasOwnProperty(category_name))
            for( index in bill['category_wise'][category_name]['all_items']){
                items_array.push(bill['category_wise'][category_name]['all_items'][index]);
            } 
        }
    return items_array;
}

function createItemWiseSplitTable(all_items_in_the_bill, rows, cols){
    $("#item_wise_split_table").html("");
    var table = $('<table></table>').attr({ class: "table table-bordered table-hover table-condensed m-t-20" });
    $('<tbody></tbody>').appendTo(table);
    var row = $('<tr></tr>').appendTo(table);
    for (var j = 0; j < cols+3; j++) {
        if(j==0){$('<th>Items</th>').appendTo(row);}
        if (j==1) {$('<th>Qty</th>').appendTo(row);}
        if (j==2) {$('<th>Balance</th>').appendTo(row);}
        if(j>2) {$('<th></th>').text("Bill " + (j-2) ).appendTo(row);}
    } 
    var tr = [];
    for (var i = 0; i < rows; i++) {
        var row = $('<tr></tr>').appendTo(table);
        for (var j = 0; j < cols+3; j++) {
            var item_name = all_items_in_the_bill[i][0];
            var qty = all_items_in_the_bill[i][1];
            if(j==0){$('<td></td>').text(item_name).appendTo(row); } 
            if(j==1){$('<td></td>').attr('id', "qty" + i).text(qty).appendTo(row); }
            if(j==2){$('<td></td>').attr('id', "balance" + i).text(qty).appendTo(row); }
            if (j>2) {
                var textBox = '<input class="form-control input-md item_split_input" type="text"  min="0" value="0" id="bill'+(j-2) + 'row'+ i +'"/>';
                $('<td></td>').html(textBox).appendTo(row); 
            }
        }
     }
    table.appendTo("#item_wise_split_table");
    bindKeyPressToInputBoxes(all_items_in_the_bill, rows, cols);
    $('#print_item_split_bills_button').removeClass('hidden'); 
    $('#print_item_split_bills_button').prop('disabled', true);
    // todo makeTableYScrollable;
}


function bindKeyPressToInputBoxes(all_items_in_the_bill, rows, cols){
    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < cols+3; j++){
            if (j>2) {
                var input_box_id = "#bill" +(j-2) + "row" + i
                acceptOnlyNumbers(input_box_id, rows);
            }
        }
    }
}

function checkItemSplitBalance(input_box_id){
    res = input_box_id.split("row");   
    id_of_bill = res[0]; //eg "#bill1"
    row =  parseInt(res[res.length-1]);
    var qty = parseInt($("#qty"+row).html());
    var number_of_bills = parseInt($("#number_of_item_wise_splits").val());
    var sum_of_qtys = 0;
    for(var i=1; i<=number_of_bills; i++){
        sum_of_qtys =  sum_of_qtys + parseInt($('#bill'+i+'row'+row).val());
    }
    $("#balance"+row).html(qty-sum_of_qtys);
    if(sum_of_qtys== qty) {
        $("#balance"+row).addClass('success').removeClass('warning');
        
        } 
    else {
        $("#balance"+row).addClass('warning').removeClass('success');
        
    }
    
}

function checkTotalValidityOfItemSplitTable(total_number_of_rows){
    for(var i=0; i<total_number_of_rows; i++){
        if($("#balance"+i).html() !="0"){$('#print_item_split_bills_button').prop('disabled', true); return false;}
    }
    $('#print_item_split_bills_button').prop('disabled', false);
}

function acceptOnlyNumbers(input_box_id, total_number_of_rows){
    $('#item_wise_split_table').on('keyup', input_box_id, (function (e) {
        this.value = this.value.replace(/[^0-9\. ]/g,'0');        
    checkItemSplitBalance(input_box_id);
    checkTotalValidityOfItemSplitTable(total_number_of_rows);
   }));
}



$('#print_item_split_bills_button').click(on_print_item_split_bills_button_clicked);

function on_print_item_split_bills_button_clicked(){
    collectItemWiseSplitDataToSend();    
}

function collectItemWiseSplitDataToSend() {
    var number_of_bills = parseInt($("#number_of_item_wise_splits").val());
    var all_items_in_the_bill = get_all_items_in_the_bill();
    var number_of_items = all_items_in_the_bill.length;
    var item_wise_split_bills_data = [];
    for(i=0; i<number_of_bills; i++){
        item_wise_split_bills_data.push({});
        for (index in all_items_in_the_bill){
            var item_name = all_items_in_the_bill[index][0];
            var quantity = parseInt($('#bill'+ (i+1) + 'row'+ index).val());
            item_wise_split_bills_data[i][item_name] = quantity;
        }
    }
    sendItemWiseSplitBillPrintRequest(item_wise_split_bills_data);

}


function sendItemWiseSplitBillPrintRequest(item_wise_split_bills_data){
    var data = {};
    var table_session_id = $('#all_running_table_drop_down').val();
      data['table_session_id'] = table_session_id;
      data['item_wise_split_bills_data'] = item_wise_split_bills_data;
      data['user_id'] = $('#user-id').html();
      var url = '/print-item-wise-split-bills';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
            if(data['response']['message']=="item wise split bills added and printed"){
                    var all_bill_ids = data['response']['all_bill_ids'];
                    var table_session_id = data['response']['table_session_id']
                    sessionStorage.itemWiseSplitBillsPrinted = 'true';
                    window.location.reload();    
            }
        },
          error: function(jqXHR, textStatus, errorThrown) { bootbox.alert(errorThrown);
        }
    });
}


$( function () {
    $('#all_running_table_drop_down').val('0');
    if ( sessionStorage.itemWiseSplitBillsPrinted == 'true') {
        bootbox.alert('Bills Printed. Temporarily Visit <a target="_blank" href="/temp-bill-print">here</a> to see the bill.'); 
        sessionStorage.itemWiseSplitBillsPrinted = 'false';
    }
} );



/* 

Split Bills By Percentage

*/

function showPercentageWiseSplitBillTabContent(){$(".tab-pane").addClass('hidden'); log('handle percent split here');}


