$("#all_un_billed_running_table_drop_down").focus();
$("#all_un_billed_running_table_drop_down").change(function() {
	$('#all_order_token_ids_for_the_table_session_and_given_printer').find('option:gt(0)').remove();
	$('#all_printers_for_the_table_session').find('option:gt(0)').remove();
	var table_session_id = this.value;
	$('#order_token_details').addClass('hidden');
    if(table_session_id=='0') {$('#order_token_details').addClass('hidden'); $('#printer_drop_down_section').addClass('hidden');$('#order_token_drop_down_section').addClass('hidden'); return;} else{$('#printer_drop_down_section').removeClass('hidden');}
    get_all_printer_wise_order_token_ids_for_the_table_session_id(table_session_id);
   
});


function get_all_printer_wise_order_token_ids_for_the_table_session_id(table_session_id){
	var data = {};
    data['table_session_id'] = table_session_id;
    var url = '/get-all-printer-wise-order-token-ids-for-the-table-session-id';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
        	var all_printer_wise_order_token_ids = data.all_printer_wise_order_token_ids_for_table_session;
        	add_all_printers_to_drop_down(all_printer_wise_order_token_ids);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });
}


function add_all_printers_to_drop_down(all_printer_wise_order_token_ids){
	$('#all_printers_for_the_table_session').find('option:gt(0)').remove();
	$('#all_printers_for_the_table_session').focus();
	$.each(all_printer_wise_order_token_ids, function(key) {
		var printer =  key.split('~');
		var printer_name = printer[0];
		var printer_id = printer[1];
		$('#all_printers_for_the_table_session').append($('<option>', { value : printer_id }).text(printer_name));
	});

	bind_printer_change(all_printer_wise_order_token_ids);
}

function bind_printer_change(all_printer_wise_order_token_ids){
$("#all_printers_for_the_table_session").off('change');
$("#all_printers_for_the_table_session").change(function() {
	var printer_id = this.value;
	var printer_name = $(this).find("option:selected").text();
	$('#order_token_details').addClass('hidden');
	var key = printer_name + '~'+ printer_id;
	if(printer_id=='0') {$('#order_token_details').addClass('hidden'); $('#order_token_drop_down_section').addClass('hidden'); return;} else{$('#order_token_drop_down_section').removeClass('hidden');}
	var all_order_token_ids_for_the_printer = all_printer_wise_order_token_ids[key];
	addOrderTokenIdsToDropDown(all_order_token_ids_for_the_printer); 

   
});
}

function addOrderTokenIdsToDropDown(all_order_token_ids_for_the_printer){
    $('#all_order_token_ids_for_the_table_session_and_given_printer').find('option:gt(0)').remove();
    $('#all_order_token_ids_for_the_table_session_and_given_printer').focus();
    $.each(all_order_token_ids_for_the_printer, function(index, value) {
         $('#all_order_token_ids_for_the_table_session_and_given_printer')
              .append($('<option>', { value : value })
              .text('ID -'+ value.slice(-5)));
    });
    $("#all_order_token_ids_for_the_table_session_and_given_printer").focus();
    bind_order_token_change();
    
}	


function bind_order_token_change(){
	$("#all_order_token_ids_for_the_table_session_and_given_printer").off('change');
	$("#all_order_token_ids_for_the_table_session_and_given_printer").change(function() {
		var order_token_id = this.value;
		if(order_token_id=='0') {$('#order_token_details').addClass('hidden');}
		else {
			get_order_token_details(order_token_id);
			$('#order_token_details').removeClass('hidden');
		}		
	    
		
	});
}


function get_order_token_details(order_token_id){
   $('#order_token_details_table').find('tr:gt(0)').remove();
	var data = {};
    data['order_token_id'] = order_token_id;
    var url = '/get-order-token-details';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
        	var items_in_this_order_token = data['order_token_details'];
        	updateTableWithItems(items_in_this_order_token);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });		
}

function updateTableWithItems(items_in_this_order_token){
    $.each(items_in_this_order_token, function(index, value){
		var item_name = value[1];
        var item_id = value[0];
		var item_quantity = value[2];
        if(item_quantity == 0) {return;} //continue
        var select_drop_down_content;
        for (var i=1; i<=item_quantity; i++) {
            if (i==item_quantity) {select_drop_down_content +=   "<option value="+i+" selected>"+ i+ "</option>";}
            else {select_drop_down_content +=   "<option value="+i+">"+ i+ "</option>";}
           
        }
        $("#order_token_details_table > tbody").append("<tr><td>"+item_name+"</td><td>"+ item_quantity+ "</td><td>" + 
            "<select id='drop_down_"+item_id + "' class='form-control input-md'><option value='0'>delete item</option>" + select_drop_down_content +"</td></tr>");	
	});
    $('#delete_order_token_btn').off('click');
    $('#save_modified_order_token_btn').off('click');
    $('#delete_order_token_btn').click({'items_in_this_order_token': items_in_this_order_token}, onDeleteOrderTokenButtonClicked);
    $('#save_modified_order_token_btn').click({'items_in_this_order_token': items_in_this_order_token}, onSaveModifiedOrderTokenButtonClicked);
}



var errorMessage;
function formValidates(){
    var ot_modification_reason = $('#ot_modification_reason').val();
    var ot_modification_printer_message = $('#ot_modification_printer_message').val();
    if(ot_modification_reason == '') {errorMessage = 'modification reason must be specified'; return false;}
    if(ot_modification_printer_message == '') {errorMessage = 'printer message must be specified'; return false;}
    return true;
}

/*

Delete Order Token

*/
function onDeleteOrderTokenButtonClicked(event){
    if ( ! formValidates()) {bootbox.alert(errorMessage); return;}
    senddeleteOrderTokenRequestTOServer();    

}

function senddeleteOrderTokenRequestTOServer(){
    var data = {};
    data['table_session_id'] = $('#all_un_billed_running_table_drop_down').val();
    data['order_token_id'] = $('#all_order_token_ids_for_the_table_session_and_given_printer').val();
    data['ot_modification_reason'] = $('#ot_modification_reason').val();
    data['ot_modification_printer_message'] = $('#ot_modification_printer_message').val();
    var url = '/delete-order-token-in-db';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            if(data="order token deleted") {
                    sessionStorage.orderTokenDeleted = 'true';
                    window.location.reload();
            }
        
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });   
}



$( function () {
    if ( sessionStorage.orderTokenDeleted == 'true' ) {
        $('#all_un_billed_running_table_drop_down').val('0');
        bootbox.alert( "order token deleted visit <a href='/temp-prints' target='_blank'>here</a> to see printer message" );
        sessionStorage.orderTokenDeleted = 'false';
    }
} );



/*

Modify Order Token

*/

function onSaveModifiedOrderTokenButtonClicked(event){
    if ( ! formValidates()) {bootbox.alert(errorMessage); return;}
    var items_in_this_order_token = event.data.items_in_this_order_token;
    var modified_items_in_this_order_token = $.extend(true, [], items_in_this_order_token);;
    for (index in modified_items_in_this_order_token){
        var item_id = modified_items_in_this_order_token[index][0];
        modified_items_in_this_order_token[index][2] = $('#drop_down_'+item_id).val() || 0;
        modified_items_in_this_order_token[index][3] = items_in_this_order_token[index][2];
        
    }
    if(ifAllItemsAreBeingIndividuallyDeleted(modified_items_in_this_order_token)) {
        bootbox.alert('to empty the token use the "Delete this order token" button');
        return;
    }
    if( ! changesMade(items_in_this_order_token, modified_items_in_this_order_token)){
        bootbox.alert('Nothing changed in the order token');
        return;   
    }
    sendModifiedOrderTokenToServer(modified_items_in_this_order_token);

}

function changesMade(items_in_this_order_token, modified_items_in_this_order_token){
    for (index in modified_items_in_this_order_token){
        var quantity_0 = items_in_this_order_token[index][2];
        var quantity_1 = modified_items_in_this_order_token[index][2];
        if (quantity_0 != quantity_1) {return true;}
    }
    return false;
}


function ifAllItemsAreBeingIndividuallyDeleted(modified_items_in_this_order_token) {
    for (index in modified_items_in_this_order_token){
        var quantity = modified_items_in_this_order_token[index][2];
        if (quantity != 0) {return false;}
    }
    return true;

}



function sendModifiedOrderTokenToServer(modified_items_in_this_order_token){
    var data = {};
    data['order_token_id'] = $('#all_order_token_ids_for_the_table_session_and_given_printer').val();
    data['modified_items_in_this_order_token'] = modified_items_in_this_order_token;
    data['ot_modification_reason'] = $('#ot_modification_reason').val();
    data['ot_modification_printer_message'] = $('#ot_modification_printer_message').val();
    var url = '/modify-order-token-in-db';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            if(data="order token modified") {
                    sessionStorage.orderTokenModified = 'true';
                    window.location.reload();
            }
        
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });   
}


$( function () {
    if ( sessionStorage.orderTokenModified == 'true' ) {
        $('#all_un_billed_running_table_drop_down').val('0');
        bootbox.alert( "order token modifed visit <a href='/temp-prints' target='_blank'>here</a> to see printer message" );
        sessionStorage.orderTokenModified = 'false';
    }
} );