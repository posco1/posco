$(document).ready(function(){

	function displayCalculatorResult(v){
			$("#not_so_simple_calculator_result_display").val(v);
		}

	function clearCalculatorResultDisplay(){
			$("#not_so_simple_calculator_result_display").val(" ");
		}

	function calculate(val){
		if(val=='~'){val = "=";}
		var result="";
		var dtemp=$("#not_so_simple_calculator_result_display").val();
		if(dtemp=="0"){
			clearCalculatorResultDisplay();
			result=val;
		}else if(val=="c"){
			clearCalculatorResultDisplay();
		}else if(val=="="){
			result=eval(dtemp);
		}else{
			result=dtemp+val;
		}
			displayCalculatorResult(result);
		}

	$("#not_so_simple_calculator").keypress(function(e){
			var arg;
			e.preventDefault();
			if (e.which == 13){
			    arg = '~';
			    calculate(arg);
			    return;
			    }
			if(e.which == 8){
			    var dtemp=$("#not_so_simple_calculator_result_display").val();
			    displayCalculatorResult(dtemp.slice(0, -1)); return;
			}
			if (e.which != 0 && (e.which < 41 || e.which > 57)) {
			    e.preventDefault();
			    return;
			    }
			arg =String.fromCharCode(e.which); 
			calculate(arg);
		});


	$('.calc_button').click(function(e){
			e.preventDefault();
			calculate(this.value);
		});



});
