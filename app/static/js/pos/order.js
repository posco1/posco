$("#table_drop_down").attr('disabled',false);
$('#order-form').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});

$("#order-form").submit(function(){
  return false;
});


var available_items_table;
var all_menu_items_with_subcategory_id_and_price;
var order_list = []; // an array of arrays

var ordered_items_table;
draw_ordered_items_table([]);

function draw_ordered_items_table(data) {
    ordered_items_table = $('#ordered_items_table').DataTable({
        "data": data,
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false

    });

}
$("#order_type_drop_down").val('dine-in');
$("#order_type_drop_down").focus();
$('#customer_info').hide(); 
$("#table_drop_down option[value=" + 'take_away' + "]").hide();
$("#table_drop_down option[value=" + 'home_delivery' + "]").hide();

$('#order_type_drop_down').on('change', function() {
  var order_type = this.value; 
  if(order_type=="dine-in") {
    $('#split_table_name').attr("placeholder", "split table name*");
    $("#table_drop_down").attr('disabled',false);
    $("#table_drop_down option[value=" + 'take_away' + "]").hide();
    $("#table_drop_down option[value=" + 'home_delivery' + "]").hide();
    $("#table_drop_down").val('0'); 
    $('#customer_info').hide(); 
    $('#server_and_pax').show();
}

if(order_type=="take_away") { 
    $("#table_drop_down option[value=" + 'take_away' + "]").show();
    $("#table_drop_down").val('take_away'); 
    $("#table_drop_down").attr('disabled',true);
    $('#customer_info').hide(); 
    $('#split_table_name').attr("placeholder", "order number*");
    $('#split_table_name').focus();
    $('#server_and_pax').hide();
}

if(order_type=="home_delivery") {
    $("#table_drop_down option[value=" + 'home_delivery' + "]").show();
    $("#table_drop_down").val('home_delivery'); 
    $("#table_drop_down").attr('disabled',true);
    $('#split_table_name').attr("placeholder", "order number*");
    $('#split_table_name').focus();
    $('#server_and_pax').hide(); 
    $('#customer_info').show(); 
    }
    
});

function draw_menu_table(data) {
    available_items_table = $("#available_items_table").DataTable({
        "data": data,
        "dom": '<"toolbar">frtip',
        "paging": false,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "columns": [null],
        "info": false,
        "autoWidth": true,
        "stateSave": true,
        "columnDefs": [{
            targets: 0,
            className: 'ripplelink'
        }]
    });
    var html = $(".all_active_subcategories_drop_down").outerHTML();
    $("div.toolbar").html(html);
    $(".to_be_hidden").hide();
}

$(document).ready(function() {
    var url = '/all-menu-items-with-subcategory';
    $.ajax({
        url: url,
        type: "POST",
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            all_menu_items_with_subcategory_id_and_price = data['result'];
            populate_table_with_all_menu_items();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });
});


function populate_table_with_all_menu_items() {
    var all_items = [];
    $.each(all_menu_items_with_subcategory_id_and_price, function(i, obj) {
        all_items.push([all_menu_items_with_subcategory_id_and_price[i].item_name])
    });
    draw_menu_table(all_items);
}


function populate_table_with_filtered_menu_items_based_on_subcategory_id(subcategory_id) {
    var items_for_subcategory = [];
    $.each(all_menu_items_with_subcategory_id_and_price, function(i, obj) {
        if (all_menu_items_with_subcategory_id_and_price[i].subcategory_id == subcategory_id)
            items_for_subcategory.push([all_menu_items_with_subcategory_id_and_price[i].item_name])
    });
    draw_menu_table(items_for_subcategory);
}


function get_price_for_item(item_to_add) {
    var price = 0;
    $.each(all_menu_items_with_subcategory_id_and_price, function(i, obj) {
        if (all_menu_items_with_subcategory_id_and_price[i].item_name == item_to_add) {
            price = all_menu_items_with_subcategory_id_and_price[i].item_price;
            return false;
        }

    });
    return price;
}


$("#wrapper_to_attach_dom").on('change', '#subcategory_drop_down', (function() {
    var selected_subcategory_id = this.value;
    available_items_table.destroy();
    if (selected_subcategory_id == 0) {
        populate_table_with_all_menu_items();
    } else {
        populate_table_with_filtered_menu_items_based_on_subcategory_id(selected_subcategory_id);
    }
    $("#wrapper_to_attach_dom #subcategory_drop_down").val(selected_subcategory_id);
}));



jQuery.fn.outerHTML = function() {
    return jQuery('<div />').append(this.eq(0).clone()).html();
};



$('#available_items_table').on('click', 'tr', function() {
    $(this).addClass('active');
    var item_to_add = (available_items_table.cell('.active', 0).data());
    var quantity = 1;
    var specialInstruction = '';
    add_to_order_list(item_to_add, quantity, specialInstruction);

    $(this).removeClass('active');
});


function add_to_order_list(item_to_add, quantity, specialInstruction) {
    if (item_already_exists_on_order_list(item_to_add)) {
        bootbox.alert('item <strong>' + item_to_add + '</strong> already on the order list. You can change its quantity')
    } else {
        order_list.push([item_to_add, quantity, specialInstruction]);
        number_of_existing_rows = ordered_items_table.rows().data().length;
        id_name_for_quantity = 'dyna_quantity' + number_of_existing_rows;
        id_name_for_special_instruction = 'dyna_special_instruction' + number_of_existing_rows;
        var price = get_price_for_item(item_to_add);
        quantityInputBox = "<span class='col-lg-2'><input onchange='updateTotalKOTPrice();' onkeyup='value=isNaN(parseFloat(value))?1:value;updateTotalKOTPrice();'  class='form-control input-sm' id='" + id_name_for_quantity + "' type='number' min='1'  value='" + quantity + "' /><span class='price_per_unit'>@" + price + "/unit</span></span>";
        specialInstructionInputBox = "<span class='col-lg-4'><input class='form-control input-sm' id='" + id_name_for_special_instruction + "' type='text' value='" + specialInstruction + "' /></span>";
        removeIcon = '<i class="remove_item_icon fa fa-close"></i>';
        ordered_items_table.row.add([item_to_add, quantityInputBox, specialInstructionInputBox, removeIcon]).draw(false);
        updateTotalKOTPrice();
        updateOrderCount();

    }

}




function updateOrderCount() {
    number_of_rows = ordered_items_table.rows().data().length;
    $('#order_count').html(number_of_rows);
}

function restructureIdsAfterDelete(row_to_delete) {
    new_order_list = ordered_items_table.rows().data();
    for (var i = row_to_delete; i < new_order_list.length; i++) {
        existing_id_name_for_quantity = '#dyna_quantity' + (i + 1);
        new_id_name_for_quantity = 'dyna_quantity' + i;
        existing_id_name_for_special_instruction = '#dyna_special_instruction' + (i + 1);
        new_id_name_for_special_instruction = 'dyna_special_instruction' + i;
        $(existing_id_name_for_quantity).attr('id', new_id_name_for_quantity);
        $(existing_id_name_for_special_instruction).attr('id', new_id_name_for_special_instruction);
    }

}


$('#ordered_items_table').on('click', '.remove_item_icon', function() {
    row_to_delete = ordered_items_table.row($(this).parents('tr')).index();
    ordered_items_table.row($(this).parents('tr')).remove().draw();
    restructureIdsAfterDelete(row_to_delete);
    update_order_list();
    updateTotalKOTPrice();
    updateOrderCount();

});


function updateTotalKOTPrice() {
    update_order_list();
    var total_price_of_this_kot = 0;
    $.each(order_list, function(i, obj) {
        per_unit_price = get_price_for_item(order_list[i][0]);
        id_name_for_quantity = '#dyna_quantity' + i;
        quantity = $(id_name_for_quantity).val();
        price_of_this_item = per_unit_price * quantity;
        total_price_of_this_kot = total_price_of_this_kot + price_of_this_item;
    });
    $('#order_price_kot').html(total_price_of_this_kot);

}


function update_order_list() {
    order_list = ordered_items_table.rows().data();
    $.each(order_list, function(i, obj) {
        id_name_for_quantity = '#dyna_quantity' + i;
        id_name_for_special_instruction = '#dyna_special_instruction' + i;
        order_list[i][1] = $(id_name_for_quantity).val();
        order_list[i][2] = $(id_name_for_special_instruction).val();
        order_list[i].splice(3, 1); // remove the icon information from the array
    });


}


function item_already_exists_on_order_list(item_to_add) {
    already_exists = false;
    $.each(order_list, function(i, obj) {
        if (order_list[i][0] == item_to_add) {
            already_exists = true;
        }

    });
    return already_exists;
}

/* General instruction Accordion */
$("#general_instruction_content").slideUp(0);
$(".add_general_instruction_button").click(function() {
    $("#general_instruction_content").slideUp("fast");
    $('.add_general_instruction_button').html('<i class="fa fa-plus-circle"></i> add general instruction');
    $(this).find('.expand').removeClass('collapse');
    if ($(this).next().is(":hidden") == true) {
        $('.expand').removeClass('collapse');
        $(this).next().slideDown("normal");
        $('.add_general_remark_button').html('<i class="fa fa-minus-circle"></i> add general instruction');
        $(this).find('.expand').addClass('collapse');
    }
});


$("#take_order").click(onTakeOrderButtonClicked);

function onTakeOrderButtonClicked(event) {
    update_order_list();
    var order_type = $('#order_type_drop_down').val(); 
    var table_id = $('#table_drop_down').val();
    var running_or_new = $('#table_drop_down :selected').parent();
    console.log(running_or_new);
    var table_name = $('#table_drop_down :selected').text(); //notice table name - not the id
    var split_table_name = $('#split_table_name').val();
    var server_id = 'na';
    var number_of_pax = '';
    if(order_type=="dine-in") {
        server_id = $('#server_drop_down').val();
        number_of_pax = $('#number_of_pax').val();
    }
    if(order_type=="home_delivery") {
        var contact_number = $('#contact_number').val(); 
        var email_address = $('#email_address').val();
        var customer_name = $('#customer_name').val();
        var customer_address = $('#customer_address').val();
    }
    else{
        var contact_number = 'na'; 
        var email_address = 'na';
        var customer_name = 'na';
        var customer_address = 'na';
    }
    var number_of_items_ordered = order_list['length'];
    var items_ordered = [];
    for(var i=0; i<number_of_items_ordered; i++){
        order_list[i].push(get_item_id_from_name(order_list[i][0]));
        items_ordered.push(order_list[i]);
    }
    var all_printer_ids = [];
    $('#general_instruction_content :input').each(function(){ all_printer_ids.push($(this).attr('id'));});
    var all_printer_messages = {}
    $.each(all_printer_ids, function( index, value ){
         all_printer_messages[value] =  $('#'+value).val();
    });
    var order_details = {'order_type': order_type, 'server_id': server_id, 'table_id': table_id,'split_table_name': split_table_name,
      'number_of_pax':number_of_pax,  'items_ordered': items_ordered, 'all_printer_messages': all_printer_messages,
      'contact_number': contact_number, 'email_address': email_address, 'customer_name': customer_name, 'customer_address': customer_address
    };
    if(order_form_validates(order_type, server_id, table_id, split_table_name, items_ordered, number_of_pax, email_address, customer_name, customer_address)){    
        sendAddOrderRequest(order_details);
    }
    else{
        bootbox.alert(errorMessage);
    }
    event.preventDefault();
}

var errorMessage;
function order_form_validates(order_type, server_id, table_id,split_table_name, items_ordered, number_of_pax, email_address, customer_name, customer_address){
    if (items_ordered[0] == null){errorMessage = "Please select items to order"; return false;}
    if (table_id =='0'){errorMessage = "Please select table"; return false;}
    if (order_type=="dine-in") {
        if (server_id =='0'){errorMessage = "Please select server"; return false;}
        if (number_of_pax == ''){errorMessage = "Please enter number of pax"; return false;}
    }
    if (order_type=="home_delivery") { 
        if(split_table_name==''){errorMessage = "order number is compulsory for home delivery orders";     $('#split_table_name').focus(); return false;} 
        if($('#contact_number').val()==''){errorMessage = "Please provide contact number"; return false;} 
        if($('#customer_address').val()==''){errorMessage = "Please provide delivery address"; return false;} 
        if( isNotValidEmail(email_address) && email_address != "") {errorMessage = "Please provide valid email address or leave it blank"; return false;} 
    }
    if (order_type=="take_away") { 
        if(split_table_name==''){errorMessage = "order number is compulsory for take away orders";     $('#split_table_name').focus(); return false;} 
    }
    
    return true;
}


function isNotValidEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return !(regex.test(email));
}

function get_item_id_from_name(item_name){
    var item_id;
    $.each(all_menu_items_with_subcategory_id_and_price, function(index, value){
        if(value.item_name == item_name){
                item_id = value.item_id;
        }
    });
    return item_id;
}


function sendAddOrderRequest(order_details){
    var data = {};
      data['order_details'] = order_details;
      var url = '/add-order';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
            if(data=='cannot take order on billed but unsetlled table') {
                bootbox.alert('cannot take fresh order on a billed but unsettled table') ;
                return;   
            }

            if (data == "order added") {
                    sessionStorage.reloadAfterPageLoad = 'true';
                    reset_form_values_to_new();
                    window.location.reload();
            }

        },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });
}

function reset_form_values_to_new(){
    $('#server_drop_down').val('0');
    $('#table_drop_down').val('0');
    $('input[type=text]').val(''); 
    $('input[type=number]').val(''); 
}

$( function () {
    if ( sessionStorage.reloadAfterPageLoad == 'true') {
        bootbox.alert( "Order added. Temporarily visit <a href='/temp-prints' target='_blank'>here</a> to see kot, bot etc" );
        sessionStorage.reloadAfterPageLoad = 'false';
    }
} );


/*
    
Fetch customer record on home delivery phone number focus out

*/

$("#contact_number").focusout(function () {
    var url = '/get-customer-info-from-contact-number';
    data = {};
    data['contact_number'] = $('#contact_number').val();
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            customer_info = data['response'];
            if(customer_info=='None'){return;}
            else{
                var email_address = customer_info['email_address'];
                var customer_name = customer_info['customer_name'];
                var customer_address = customer_info['customer_address'];
                $('#email_address').val(email_address);
                $('#customer_name').val(customer_name);
                $('#customer_address').val(customer_address);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });

});



/* Sort Order Attempt

    $('#ordered_items_table tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('active') ) {
            $(this).removeClass('active');
        }
        else {
            ordered_items_table.$('tr.active').removeClass('active');
            $(this).addClass('active');
        }
    } );


$('#move_item_up').on('click', moveSelectedUp);

function moveSelectedUp(){
log('hi');
}*/

$(".demo-03").toggleButton({radio: true})
			.on("toggle.change", function(e, active){
				log(JSON.stringify($(this).serializeGroup()));
	});
