$("#all_unsettled-running_table_drop_down").focus();
$("#all_unsettled-running_table_drop_down").change(function() {

    onDropDownsSelected();
});

function onDropDownsSelected(){
    
    bill = undefined;
    var running_table_session_id = $('#all_unsettled-running_table_drop_down').val();
    var bill_modification_type = $('#mark_bill_with').val();
    if(running_table_session_id !='0'){getBillData(running_table_session_id);}
    $('.show_bill').addClass('hidden'); 

    
}

$("#mark_bill_with").change(function() {
    var running_table_session_id = $('#all_unsettled-running_table_drop_down').val();
    var bill_modification_type = $('#mark_bill_with').val();
    $('.show_bill').addClass('hidden');
    onDropDownsSelected();
});

function getBillData(running_table_session_id) {
    var data = {};
    data['running_table_session_id'] = running_table_session_id;
    var url = '/generate-bill';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            var bill = data.bill;
            var bill_modification_type = $('#mark_bill_with').val();
            if((bill_modification_type=='complementary') || (bill_modification_type=='on_the_house') ){ showComplementaryAndOnTheHouseTable(bill); }
            if(bill_modification_type=='discount_on_item'){showItemWiseDiscountModificationTable(bill);}    
            if(bill_modification_type=='discount_on_total'){ showDiscountOnTotalModificationTable(bill); }
            if(bill_modification_type=='item_as_complementary'){ showItemAsComplementaryModificationTable(bill); }
                $('.print_modified_bill_button').off('click');
                $('.print_modified_bill_button').click({bill: bill}, on_print_modified_bill_button_clicked);

        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });
}

function showItemWiseDiscountModificationTable(bill) {
    $('#item_wise_discount_bill_preview_table #item_wise_discount_tbody tr').slice(1).remove(); 
    $(".item_wise_discount_section").removeClass('hidden');
    var service_tax_rate = bill.service_tax_rate;
    var service_charge_rate = bill.service_charge_rate;
    var order_type = bill.order_type;
    if(order_type == 'take_away' || order_type =='home_delivery'){service_charge_rate = 0;}
    var home_delivery_charge;
    if(order_type == 'home_delivery') {home_delivery_charge = parseFloat(bill.home_delivery_charge); } else {home_delivery_charge = 0;}
    var total_bill = 0
    $.each(bill.category_wise, function(category_name, category) {
        var category_total = 0;
        $.each(category.all_items, function(i, item) {
            $('#item_wise_discount_bill_preview_table #item_wise_discount_tbody tr:last').after('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td><td>' + roundTo2(item[2]) + '</td><td id="actual_amount_for_'+item[4]+'" class="ta-right">' + roundTo2(item[3]) + '</td><td><input class="discount form-control input-sm" type="text" value="0" id="discount_percent_on_'+item[4]+'"/></td><td  id="discount_amount_on_'+item[4]+'">0.00</td><td  id="after_discount_amount_for_'+item[4]+'">'+roundTo2(item[3])+'</td></tr>');
            bindKeyPressInDiscountInputBox(item[4], bill);
            category_total = category_total + item[3];
        });
        $('#item_wise_discount_bill_preview_table > #item_wise_discount_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Total for ' + category_name + '</td><td class="ta-right">' + category_total.toFixed(2) + '</td><td></td><td id="total_for_category_'+category_name+'"></td><td id="total_after_discount_for_category_'+removeSpecialCharacterAndSpacesFromCategoryName(category_name)+'" >'+ category_total.toFixed(2)+'</td></tr>');
        var service_charge_amount = calculateServiceCharge(category_total, service_charge_rate);
        var vat_amount = calculateVat(category_total, category.vat_rate);
        var service_tax_amount = calculateServiceTax(category_total, service_tax_rate);
        if(order_type == 'dine-in') {
        $('#item_wise_discount_bill_preview_table #item_wise_discount_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Service charge @' + service_charge_rate + '%</td><td class="ta-right">' + service_charge_amount.toFixed(2) + '</td><td></td><td></td><td id="service_charge_after_discount_for_category_'+removeSpecialCharacterAndSpacesFromCategoryName(category_name)+'" >'+service_charge_amount.toFixed(2)+'</td></tr>');}
        $('#item_wise_discount_bill_preview_table #item_wise_discount_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Vat @' + category.vat_rate + '%</td><td class="ta-right">' + vat_amount.toFixed(2) + '</td><td></td><td></td><td id="vat_after_discount_for_category_'+removeSpecialCharacterAndSpacesFromCategoryName(category_name)+'" >'+vat_amount.toFixed(2)+'</td></tr>');
        $('#item_wise_discount_bill_preview_table #item_wise_discount_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Govt. Service Tax @' + service_tax_rate + '%</td><td class="ta-right">' + service_tax_amount.toFixed(2) + '</td><td></td><td></td><td id="service_tax_after_discount_for_category_'+removeSpecialCharacterAndSpacesFromCategoryName(category_name)+'" >'+ service_tax_amount.toFixed(2)+'</td></tr>');
        total_bill = total_bill + category_total + service_tax_amount + vat_amount + service_charge_amount;
    });
    if(order_type == 'home_delivery') {
        $('#item_wise_discount_bill_preview_table #item_wise_discount_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Home Delivery Charge</td><td class="ta-right">' + home_delivery_charge.toFixed(2) + '</td><td></td><td></td><td id="home_delivery_charge_after_discount">'+ home_delivery_charge.toFixed(2) +'</td></tr>');
        total_bill = total_bill + home_delivery_charge;
        
    }
    $('#item_wise_discount_bill_preview_table > #item_wise_discount_tbody tr:last').after('<tr><td class="ta-right" colspan="3"><strong>Bill Total:</strong></td><td id="total_before_discount" class="ta-right">' + roundTo2(Math.round(total_bill)) + '</td><td></td><td></td><td id="bill_total_after_item_wise_discount">'+ roundTo2(Math.round(total_bill)) +'</td></tr>');
    $("input:text").first().focus();
    
    }

function bindKeyPressInDiscountInputBox(item_ID, bill){
    var input_box_id = '#discount_percent_on_' +item_ID ;
    $('#item_wise_discount_bill_preview_table').on('keyup', input_box_id, (function (e) {
        this.value = this.value.replace(/[^0-9\.]/g,'');
        if($(input_box_id).val()==''){return;}
        var discount = parseFloat($(input_box_id).val());
        if(discount >100) {$(input_box_id).val('0');}
        updateTable(item_ID, bill);
   }));
}

function updateTable(item_ID, bill){
    updateItemAfterDiscount(item_ID);
    updateCategoryTotalAfterDiscount(bill);

}


function updateCategoryTotalAfterDiscount(bill){
    var service_tax_rate = bill.service_tax_rate;
    var service_charge_rate = bill.service_charge_rate;
    var order_type = bill.order_type;
    if(order_type == 'take_away' || order_type =='home_delivery'){service_charge_rate = 0;}
    var home_delivery_charge;
    var bill_total_after_discount = 0;
    if(order_type == 'home_delivery') {home_delivery_charge = parseFloat(bill.home_delivery_charge); } else {home_delivery_charge = 0;}
    $.each(bill.category_wise, function(category_name, category) {
        var cat_name = removeSpecialCharacterAndSpacesFromCategoryName(category_name);
        var id_for_category_total_after_discount  = '#total_after_discount_for_category_'+cat_name;
        var id_for_vat_amount_after_discount  = '#vat_after_discount_for_category_'+cat_name;
        var id_for_service_charge_after_discount  = '#service_charge_after_discount_for_category_'+cat_name;
        var id_for_service_tax_after_discount  = '#service_tax_after_discount_for_category_'+cat_name;
        var category_total_after_discount = 0;
        $.each(category.all_items, function(i, item) {
            var after_discount_amount_for_item = parseFloat($('#after_discount_amount_for_'+item[4]).html());
            category_total_after_discount = category_total_after_discount + after_discount_amount_for_item;
        });
        category_total_after_discount = category_total_after_discount;
        var vat_amount_after_discount = calculateVat(category_total_after_discount, category.vat_rate);
        var service_tax_amount_after_discount = calculateServiceTax(category_total_after_discount, service_tax_rate);
        var service_charge_amount_after_discount = calculateServiceCharge(category_total_after_discount, service_charge_rate);
        bill_total_after_discount = bill_total_after_discount + parseFloat(category_total_after_discount) + parseFloat(vat_amount_after_discount) + parseFloat(service_tax_amount_after_discount) + parseFloat(service_charge_amount_after_discount);
        $(id_for_category_total_after_discount).html(category_total_after_discount.toFixed(2));
        $(id_for_vat_amount_after_discount).html(vat_amount_after_discount.toFixed(2));
        $(id_for_service_charge_after_discount).html(service_charge_amount_after_discount.toFixed(2));
        $(id_for_service_tax_after_discount).html(service_tax_amount_after_discount.toFixed(2));
    });
        if(bill.order_type == 'home_delivery') {
                bill_total_after_discount = bill_total_after_discount + parseFloat(bill.home_delivery_charge);
        
        }
        $('#home_delivery_charge_after_discount').html(parseFloat(bill.home_delivery_charge).toFixed(2));
        $('#bill_total_after_item_wise_discount').html(roundTo2(bill_total_after_discount));
}

function updateItemAfterDiscount(item_ID){
    var input_box_id = '#discount_percent_on_' +item_ID ;
    var actual_total_id = '#actual_amount_for_' +item_ID ;
    var discount = parseFloat($(input_box_id).val());
    var actual_amount = parseFloat($(actual_total_id).html());
    var discount_amount = actual_amount * discount / 100;
    var item_total_after_discount =  actual_amount - discount_amount;
    $('#discount_amount_on_'+item_ID).html(discount_amount.toFixed(2));
    $('#after_discount_amount_for_'+item_ID).html(item_total_after_discount.toFixed(2));
}


function removeSpecialCharacterAndSpacesFromCategoryName(str){
    var new_string = str.replace(/[^A-Z0-9]+/ig, "_");
    return new_string;
}


function calculateServiceCharge(amount, service_charge_rate) {
    return parseFloat(amount) * parseFloat(service_charge_rate) / 100;
}

function calculateVat(amount, vat_rate) {
    return parseFloat(amount) * parseFloat(vat_rate) / 100;
}
function calculateServiceTax(amount, service_tax_rate) {
    return parseFloat(amount) * (service_tax_rate) / 100;
}


function roundTo2(num) {
    return (Math.round(num * 100 / 100)).toFixed(2);
}

/* 





Item As Complementary 




*/

function showItemAsComplementaryModificationTable(bill){
    $('#item_as_complementary_bill_preview_table #item_as_complementary_tbody tr').slice(1).remove(); 
    $(".item_as_complementary_section").removeClass('hidden');
    var service_tax_rate = bill.service_tax_rate;
    var service_charge_rate = bill.service_charge_rate;
    var order_type = bill.order_type;
    if(order_type == 'take_away' || order_type =='home_delivery'){service_charge_rate = 0;}
    var home_delivery_charge;
    if(order_type == 'home_delivery') {home_delivery_charge = parseFloat(bill.home_delivery_charge); } else {home_delivery_charge = 0;}
    var total_bill = 0
    $.each(bill.category_wise, function(category_name, category) {
        var category_total = 0;
        $.each(category.all_items, function(i, item) {
            $('#item_as_complementary_bill_preview_table #item_as_complementary_tbody tr:last').after('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td><td>' + roundTo2(item[2]) + '</td><td id="actual_amount_for_'+item[4]+'" class="ta-right">' + roundTo2(item[3]) + '</td><td><input class="discount form-control input-sm" type="text" value="0" id="item_as_complementary_on_'+item[4]+'"/></td><td  id="after_complementary_amount_for_'+item[4]+'">'+roundTo2(item[3])+'</td></tr>');
            bindKeyPressInComplementaryInputBox(item[4], item[1], item[2], bill);
            category_total = category_total + item[3];
        });
        $('#item_as_complementary_bill_preview_table > #item_as_complementary_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Total for ' + category_name + '</td><td class="ta-right">' + category_total.toFixed(2) + '</td><td id="total_for_category_'+category_name+'"></td><td id="total_after_item_complementary_for_category_'+removeSpecialCharacterAndSpacesFromCategoryName(category_name)+'" >'+ category_total.toFixed(2)+'</td></tr>');
        var service_charge_amount = calculateServiceCharge(category_total, service_charge_rate);
        var vat_amount = calculateVat(category_total, category.vat_rate);
        var service_tax_amount = calculateServiceTax(category_total, service_tax_rate);
        if(order_type == 'dine-in') {
        $('#item_as_complementary_bill_preview_table #item_as_complementary_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Service charge @' + service_charge_rate + '%</td><td class="ta-right">' + service_charge_amount.toFixed(2) + '</td><td></td><td id="service_charge_after_item_complementary_for_category_'+removeSpecialCharacterAndSpacesFromCategoryName(category_name)+'" >'+service_charge_amount.toFixed(2)+'</td></tr>');}
        $('#item_as_complementary_bill_preview_table #item_as_complementary_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Vat @' + category.vat_rate + '%</td><td class="ta-right">' + vat_amount.toFixed(2) + '</td><td></td><td id="vat_after_item_complementary_for_category_'+removeSpecialCharacterAndSpacesFromCategoryName(category_name)+'" >'+vat_amount.toFixed(2)+'</td></tr>');
        $('#item_as_complementary_bill_preview_table #item_as_complementary_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Govt. Service Tax @' + service_tax_rate + '%</td><td class="ta-right">' + service_tax_amount.toFixed(2) + '</td><td></td><td id="service_tax_after_item_complementary_for_category_'+removeSpecialCharacterAndSpacesFromCategoryName(category_name)+'" >'+ service_tax_amount.toFixed(2)+'</td></tr>');
        total_bill = total_bill + category_total + service_tax_amount + vat_amount + service_charge_amount;
    });
    if(order_type == 'home_delivery') {
        $('#item_as_complementary_bill_preview_table #item_as_complementary_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Home Delivery Charge</td><td class="ta-right">' + home_delivery_charge.toFixed(2) + '</td><td></td><td id="home_delivery_charge_after_item_complementary">'+ home_delivery_charge.toFixed(2) +'</td></tr>');
        total_bill = total_bill + home_delivery_charge;
        
    }
    $('#item_as_complementary_bill_preview_table > #item_as_complementary_tbody tr:last').after('<tr><td class="ta-right" colspan="3"><strong>Bill Total:</strong></td><td id="total_before_discount" class="ta-right">' + roundTo2(Math.round(total_bill)) + '</td><td></td><td id="bill_total_after_item_complementary">'+ roundTo2(Math.round(total_bill)) +'</td></tr>');
    $("input:text").first().focus();

}



function bindKeyPressInComplementaryInputBox(item_ID, quantity_ordered, rate, bill){
     var input_box_id = '#item_as_complementary_on_' +item_ID ;
    $('#item_as_complementary_bill_preview_table').on('keyup', input_box_id, (function (e) {
        this.value = this.value.replace(/[^0-9]/g,'');
        var quantity_given_as_complementary = $(input_box_id).val();
        if(quantity_given_as_complementary==''){quantity_given_as_complementary = 0;}
        quantity_given_as_complementary = parseFloat(quantity_given_as_complementary);
        if(quantity_given_as_complementary > parseFloat(quantity_ordered)) {$(input_box_id).val('0'); bootbox.alert('You cannot mark item as complementary exceeding the original ordered quantity !'); return;}
        var item_amount_after_complementary = (parseFloat(quantity_ordered) - quantity_given_as_complementary)* parseFloat(rate);
        $('#after_complementary_amount_for_'+item_ID).html(item_amount_after_complementary.toFixed(2));
        updateCategoryTotalAfterItemComplementary(bill);
   }));
}


function updateCategoryTotalAfterItemComplementary(bill){
    var service_tax_rate = bill.service_tax_rate;
    var service_charge_rate = bill.service_charge_rate;
    var order_type = bill.order_type;
    if(order_type == 'take_away' || order_type =='home_delivery'){service_charge_rate = 0;}
    var home_delivery_charge;
    var bill_total_after_item_complementary = 0;
    if(order_type == 'home_delivery') {home_delivery_charge = parseFloat(bill.home_delivery_charge); } else {home_delivery_charge = 0;}
    $.each(bill.category_wise, function(category_name, category) {
        var cat_name = removeSpecialCharacterAndSpacesFromCategoryName(category_name);
        var id_for_category_total_after_item_complementary  = '#total_after_item_complementary_for_category_'+cat_name;
        var id_for_vat_amount_after_item_complementary  = '#vat_after_item_complementary_for_category_'+cat_name;
        var id_for_service_charge_after_item_complementary  = '#service_charge_after_item_complementary_for_category_'+cat_name;
        var id_for_service_tax_after_item_complementary  = '#service_tax_after_item_complementary_for_category_'+cat_name;
        var category_total_after_item_complementary = 0;
        $.each(category.all_items, function(i, item) {
            var after_item_complementary_amount_for_item = parseFloat($('#after_complementary_amount_for_'+item[4]).html());
            category_total_after_item_complementary = category_total_after_item_complementary + after_item_complementary_amount_for_item;
        });
        category_total_after_item_complementary = category_total_after_item_complementary;
        var vat_amount_after_item_complementary = calculateVat(category_total_after_item_complementary, category.vat_rate);
        var service_tax_amount_after_item_complementary = calculateServiceTax(category_total_after_item_complementary, service_tax_rate);
        var service_charge_amount_after_item_complementary = calculateServiceCharge(category_total_after_item_complementary, service_charge_rate);
        bill_total_after_item_complementary = bill_total_after_item_complementary + parseFloat(category_total_after_item_complementary) + parseFloat(vat_amount_after_item_complementary) + parseFloat(service_tax_amount_after_item_complementary) + parseFloat(service_charge_amount_after_item_complementary);
        $(id_for_category_total_after_item_complementary).html(category_total_after_item_complementary.toFixed(2));
        $(id_for_vat_amount_after_item_complementary).html(vat_amount_after_item_complementary.toFixed(2));
        $(id_for_service_charge_after_item_complementary).html(service_charge_amount_after_item_complementary.toFixed(2));
        $(id_for_service_tax_after_item_complementary).html(service_tax_amount_after_item_complementary.toFixed(2));
    });
        if(bill.order_type == 'home_delivery') {
                bill_total_after_item_complementary = bill_total_after_item_complementary + parseFloat(bill.home_delivery_charge);
        
        }
        $('#home_delivery_charge_after_item_complementary').html(parseFloat(bill.home_delivery_charge).toFixed(2));
        $('#bill_total_after_item_complementary').html(roundTo2(bill_total_after_item_complementary));
}

/*



Discount On Total



*/


function showDiscountOnTotalModificationTable(bill){
    $('#discount_on_total_bill_preview_table #discount_on_total_tbody tr').slice(1).remove(); 
    $(".discount_on_total_section").removeClass('hidden');
    var service_tax_rate = bill.service_tax_rate;
    var service_charge_rate = bill.service_charge_rate;
    var order_type = bill.order_type;
    if(order_type == 'take_away' || order_type =='home_delivery'){service_charge_rate = 0;}
    var home_delivery_charge;
    if(order_type == 'home_delivery') {home_delivery_charge = parseFloat(bill.home_delivery_charge); } else {home_delivery_charge = 0;}
    var total_bill = 0
    $.each(bill.category_wise, function(category_name, category) {
        var category_total = 0;
        $.each(category.all_items, function(i, item) {
            $('#discount_on_total_bill_preview_table #discount_on_total_tbody tr:last').after('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td><td>' + roundTo2(item[2]) + '</td><td id="actual_amount_for_'+item[4]+'" class="ta-right">' + roundTo2(item[3]) + '</td></tr>');
            bindKeyPressInComplementaryInputBox(item[4], item[1], item[2], bill);
            category_total = category_total + item[3];
        });
        $('#discount_on_total_bill_preview_table > #discount_on_total_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Total for ' + category_name + '</td><td class="ta-right">' + category_total.toFixed(2) + '</td></tr>');
        var service_charge_amount = calculateServiceCharge(category_total, service_charge_rate);
        var vat_amount = calculateVat(category_total, category.vat_rate);
        var service_tax_amount = calculateServiceTax(category_total, service_tax_rate);
        if(order_type == 'dine-in') {
        $('#discount_on_total_bill_preview_table #discount_on_total_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Service charge @' + service_charge_rate + '%</td><td class="ta-right">' + service_charge_amount.toFixed(2) + '</td></tr>');}
        $('#discount_on_total_bill_preview_table #discount_on_total_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Vat @' + category.vat_rate + '%</td><td class="ta-right">' + vat_amount.toFixed(2) + '</td></tr>');
        $('#discount_on_total_bill_preview_table #discount_on_total_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Govt. Service Tax @' + service_tax_rate + '%</td><td class="ta-right">' + service_tax_amount.toFixed(2) + '</td></tr>');
        total_bill = total_bill + category_total + service_tax_amount + vat_amount + service_charge_amount;
    });
    if(order_type == 'home_delivery') {
        $('#discount_on_total_bill_preview_table #discount_on_total_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Home Delivery Charge</td><td class="ta-right">' + home_delivery_charge.toFixed(2) + '</td></tr>');
        total_bill = total_bill + home_delivery_charge;
        
    }
    $('#discount_on_total_bill_preview_table > #discount_on_total_tbody tr:last').after('<tr><td class="ta-right" colspan="3"><strong>Bill Total:</strong></td><td id="total_before_discount" class="ta-right">' + roundTo2(Math.round(total_bill)) + '</td></tr>');
    $('#discount_on_total_bill_total').html(roundTo2(Math.round(total_bill)));
    $('#discount_on_total_bill_total_after_discount').html(roundTo2(Math.round(total_bill)));
    $('#discount_on_total_input').focus();

}


 $('#discount_on_total_input').on('keyup', (function (e) {
        this.value = this.value.replace(/[^0-9\.]/g,'');
        var original_bill_total = parseFloat($('#total_before_discount').html());
        var discount_percent = parseFloat($('#discount_on_total_input').val());
        if($('#discount_on_total_input').val()==''){
            discount_percent = 0; 
            reset_discount_on_total_bill_values(); 
             }
        if(discount_percent > 100 || discount_percent < 0) {
            $('#discount_on_total_input').val('0'); 
            discount_percent = 0; 
            reset_discount_on_total_bill_values();
            bootbox.alert('invalid entry!');
            
        }
        var discount_amount = original_bill_total * discount_percent /100;
        var bill_total_after_discount = original_bill_total - discount_amount;

        $('#discount_on_total_amount_of_discount').html(discount_amount.toFixed(2));
        $('#discount_on_total_bill_total_after_discount').html(roundTo2(bill_total_after_discount.toFixed(2)));

   }));


function reset_discount_on_total_bill_values(){
    var original_bill_total = parseFloat($('#total_before_discount').val());
    $('#discount_on_total_bill_total_after_discount').html(original_bill_total);
    $('#discount_on_total_amount_of_discount').html(0.00);

}

/*





Complementary And On The House






*/

function showComplementaryAndOnTheHouseTable(bill){
    var selected_text = $( "#mark_bill_with option:selected" ).text();
    $('#label_for_complementary_and_on_the_house').html(selected_text);
   $('#complementary_and_on_the_house_preview_table #complementary_and_on_the_house_tbody tr').slice(1).remove(); 
    $(".complementary_and_on_the_house_section").removeClass('hidden');
    var service_tax_rate = bill.service_tax_rate;
    var service_charge_rate = bill.service_charge_rate;
    var order_type = bill.order_type;
    if(order_type == 'take_away' || order_type =='home_delivery'){service_charge_rate = 0;}
    var home_delivery_charge;
    if(order_type == 'home_delivery') {home_delivery_charge = parseFloat(bill.home_delivery_charge); } else {home_delivery_charge = 0;}
    var total_bill = 0
    $.each(bill.category_wise, function(category_name, category) {
        var category_total = 0;
        $.each(category.all_items, function(i, item) {
            $('#complementary_and_on_the_house_preview_table #complementary_and_on_the_house_tbody tr:last').after('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td><td>' + roundTo2(item[2]) + '</td><td id="actual_amount_for_'+item[4]+'" class="ta-right">' + roundTo2(item[3]) + '</td></tr>');
            
            category_total = category_total + item[3];
        });
        $('#complementary_and_on_the_house_preview_table > #complementary_and_on_the_house_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Total for ' + category_name + '</td><td class="ta-right">' + category_total.toFixed(2) + '</td></tr>');
        var service_charge_amount = calculateServiceCharge(category_total, service_charge_rate);
        var vat_amount = calculateVat(category_total, category.vat_rate);
        var service_tax_amount = calculateServiceTax(category_total, service_tax_rate);
        if(order_type == 'dine-in') {
        $('#complementary_and_on_the_house_preview_table #complementary_and_on_the_house_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Service charge @' + service_charge_rate + '%</td><td class="ta-right">' + service_charge_amount.toFixed(2) + '</td></tr>');}
        $('#complementary_and_on_the_house_preview_table #complementary_and_on_the_house_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Vat @' + category.vat_rate + '%</td><td class="ta-right">' + vat_amount.toFixed(2) + '</td></tr>');
        $('#complementary_and_on_the_house_preview_table #complementary_and_on_the_house_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Govt. Service Tax @' + service_tax_rate + '%</td><td class="ta-right">' + service_tax_amount.toFixed(2) + '</td></tr>');
        total_bill = total_bill + category_total + service_tax_amount + vat_amount + service_charge_amount;
    });
    if(order_type == 'home_delivery') {
        $('#complementary_and_on_the_house_preview_table #complementary_and_on_the_house_tbody tr:last').after('<tr><td class="ta-right" colspan="3">Home Delivery Charge</td><td class="ta-right">' + home_delivery_charge.toFixed(2) + '</td></tr>');
        total_bill = total_bill + home_delivery_charge;
        
    }
    $('#complementary_and_on_the_house_preview_table > #complementary_and_on_the_house_tbody tr:last').after('<tr><td class="ta-right" colspan="3"><strong>Bill Total:</strong></td><td id="total_before_discount" class="ta-right">' + roundTo2(Math.round(total_bill)) + '</td></tr>');
    $('#complementary_and_on_the_house_total').html(roundTo2(Math.round(total_bill)));
    $('#complementary_and_on_the_house_total_after_discount').html(roundTo2(Math.round(total_bill)));
    $('#message_for_marked_as_complementary_and_on_the_house').html('<br>New bill total: 0.00 <br> You must print this bill to '+selected_text);

}


/*

Bill Printer Settings Section

*/

var printerInputHtml;
$('#bill-printer-settings').click(onPrinterSettingsClicked);

function onPrinterSettingsClicked() {
    printerInputHtml = generateHTMLforPrinterInputBox();
    showPrinterChooserDialog();
    $('#printer_name').val(printerNameForCurrentUser);
}


function showPrinterChooserDialog() {
    var msg = printerInputHtml || generateHTMLforPrinterInputBox();
    bootbox.dialog({
        title: "<small>Print Bills on:</small>",
        message: msg,
        buttons: {
            success: {
                label: "Save",
                className: "btn-success",
                callback: function() {
                    var printer_name = $('#printer_name').val();
                    onPrinterChooserSaveButtonClicked(printer_name);

                }
            }
        }
    });

}

function onPrinterChooserSaveButtonClicked(printer_name) {
    if ((printer_name == undefined) || (printer_name == '')) {
        showPrinterChooserDialog();
        bootbox.alert('you must select a printer to proceed');
    } else {
        sendPrinterUserMapSaveRequest(printer_name);
        window.location.reload(true);
    }
}


function sendPrinterUserMapSaveRequest(printer_name) {
    var data = {};
    data['user_id'] = $('#user-id').html();
    data['printer_name'] = printer_name;
    var url = '/save-user-printer-map';
    $.ajax({
        url: url,
        type: "POST",
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(data, null, '\t'),
        success: function(data, textStatus, jqXHR) {
            if (data == "user printer map saved") {
                bootbox.alert('printer settings saved');
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });
}

var printerNameForCurrentUser;

$(document).ready(function($) {
    var user_id = $('#user-id').html();
    printerNameForCurrentUser = getprinterNameForCurrentUser(user_id);
});


function generateHTMLforPrinterInputBox() {
    temp = '<div class="row"><div class="col-md-3"></div><div class="col-md-6"><form class="form-horizontal"> ' +
        '<div class="form-group"><input type="text" class="form-control input-md" value="'+printerNameForCurrentUser +'" id="printer_name"></input>';
    temp = temp + '</div></form> </div> </div>';
    return temp;
}

function getprinterNameForCurrentUser(user_id) {
    var data = {};
    data['user_id'] = user_id;
    var url = '/get-printer-name-for-current-user';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            if (data == 'no user printer mapping') {
                showPrinterChooserDialog();
            } else {
                printerNameForCurrentUser = data;
                return printerNameForCurrentUser;
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });
}


/*

Send Data to Server

*/




function on_print_modified_bill_button_clicked(event){
    var original_bill = event.data.bill;
    var running_table_session_id = $('#all_unsettled-running_table_drop_down').val();
    var bill_modification_type = $('#mark_bill_with').val();
    var modified_bill;
    if((bill_modification_type=='complementary') || (bill_modification_type=='on_the_house') ){
        modified_bill = billForComplementaryAndOnTheHouse(original_bill); }
    if(bill_modification_type=='discount_on_item'){modified_bill= billForItemWiseDiscount(original_bill);}    
    if(bill_modification_type=='discount_on_total'){ modified_bill= billForDiscountOnTotal(original_bill); }
    if(bill_modification_type=='item_as_complementary'){ modified_bill = billForItemAsComplementary(original_bill); }
    log(modified_bill);
    if(modified_bill==undefined){return;}
    else{send_modified_bill_for_print_request(modified_bill);}
    
}

function send_modified_bill_for_print_request(modified_bill){
  var running_table_session_id = $('#all_unsettled-running_table_drop_down').val();
  var printer_name = printerNameForCurrentUser;
  if(running_table_session_id=='0'){return;}
    var data = {};
    data['running_table_session_id'] = running_table_session_id;
    data['printer_name'] = printer_name;
    data['modified_bill'] = modified_bill;
    var url = '/print-modified-bill';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            if(data['response']['message']=='modified bill added and printed'){
                    var all_bill_ids = data['response']['all_bill_ids'];
                    var table_session_id = data['response']['table_session_id']

                   bootbox.alert('Bill Printed. Temporarily Visit <a target="_blank" href="/temp-bill-print">here</a> to see the bill.<br> Next-- Would you like to capture Customer Details Yes or No Button Comes Here'); 
            }
            
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });



}

function billForComplementaryAndOnTheHouse(original_bill){
    var bill_modification_reason = $('#bill_modification_reason_complementary_and_on_the_house').val();
    if(bill_modification_reason == ''){bootbox.alert('you must specify a bill modification reason'); return;}
    var bill_modification_type = $( "#mark_bill_with" ).val();
    var over_all_bill_discount_percentage = 100;
    var modified_bill = original_bill;
    modified_bill['bill_modification_reason'] = bill_modification_reason;
    modified_bill['bill_modification_type'] = bill_modification_type;
    modified_bill['over_all_bill_discount_percentage'] = over_all_bill_discount_percentage;
    modified_bill['is_bill_modified'] = true;
    return modified_bill;
}


function billForItemWiseDiscount(original_bill){
    var original_amount = $('#total_before_discount').html();
    var amount_after_discount = $('#bill_total_after_item_wise_discount').html();
    if(original_amount == amount_after_discount) {bootbox.alert('nothing modified in the bill !'); return;}
    var bill_modification_reason = $('#bill_modification_reason_item_wise_discount').val();
    if(bill_modification_reason == ''){bootbox.alert('you must specify a bill modification reason'); return;}
    var modified_bill = original_bill;
    $.each(modified_bill.category_wise, function(category_name, category) {
        $.each(category.all_items, function(i, item) {
            var discount_on_item = $('#discount_percent_on_'+item[4]).val();
            if(discount_on_item==""){}
            else{item[5] = parseFloat(discount_on_item);}
        });
    });
    var bill_modification_type = $( "#mark_bill_with" ).val();
    modified_bill['is_bill_modified'] = true;
    modified_bill['bill_modification_reason'] = bill_modification_reason;
    modified_bill['bill_modification_type'] = bill_modification_type;
    return modified_bill;
}


function billForDiscountOnTotal(original_bill){
    var bill_modification_reason = $('#bill_modification_reason_discount_on_total').val();
    if(bill_modification_reason==''){bootbox.alert('you must specify a bill modification reason'); return;}
    var modified_bill = original_bill;
    var discount_rate_on_total_bill = $('#discount_on_total_input').val();
    if(discount_rate_on_total_bill=="" || parseFloat(discount_rate_on_total_bill)==0) {bootbox.alert('nothing modified in the bill !'); return;}
    else{
        var bill_modification_type = $( "#mark_bill_with" ).val();
        modified_bill['is_bill_modified'] = true;
        modified_bill['bill_modification_reason'] = bill_modification_reason;
        modified_bill['bill_modification_type'] = bill_modification_type;
        modified_bill['over_all_bill_discount_percentage'] = discount_rate_on_total_bill; 
    }
    return modified_bill;
}


function billForItemAsComplementary(original_bill){
    var original_amount = $('#total_before_discount').html();
    var amount_after_discount = $('#bill_total_after_item_complementary').html();
    if(original_amount == amount_after_discount) {bootbox.alert('nothing modified in the bill !'); return;}
    var bill_modification_reason = $('#bill_modification_reason_item_as_complementary').val();
    if(bill_modification_reason ==''){bootbox.alert('you must specify a bill modification reason'); return;}
    var modified_bill = original_bill;
    $.each(modified_bill.category_wise, function(category_name, category) {
        $.each(category.all_items, function(i, item) {
            var complementary_quantity_for_this_item = $('#item_as_complementary_on_'+item[4]).val();
            if(complementary_quantity_for_this_item==""){}
            else{item[6] = parseFloat(complementary_quantity_for_this_item);}
        });
    });
    var bill_modification_type = $( "#mark_bill_with" ).val();
    modified_bill['is_bill_modified'] = true;
    modified_bill['bill_modification_reason'] = bill_modification_reason;
    modified_bill['bill_modification_type'] = bill_modification_type;
    return modified_bill;
}
