$('#mark_as_settled_btn_multiple').prop('disabled', true);
$("#all_unsettled_table_drop_down").focus();
$('#all_bill_ids_for_table_session').hide();
$('#all_settlement_modes_drop_down').val('0');
$("#all_unsettled_table_drop_down").change(function() {
    var unsettled_table_session_id = this.value;
        $('#bill_amount_box').addClass('hidden');
        $('.all_settlement_modes_content').addClass('hidden');
        $('#cash_calculator').addClass('hidden');
        if(unsettled_table_session_id=='0'){
        $('#all_bill_ids_for_table_session').hide(); $("#all_settlement_modes").addClass('hidden'); return;
         }
         else{
         $('#all_bill_ids_for_table_session').show();
         }
    getBillsForTableSession(unsettled_table_session_id);
});


function getBillsForTableSession(unsettled_table_session_id){
    var data = {};
    data['unsettled_table_session_id'] = unsettled_table_session_id;
    var url = '/get-all-unsettled-bill-ids-for-table-session';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            var all_bill_ids = data.all_bill_ids;

            addBillIdsToDropDown(all_bill_ids);

        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });


}


function addBillIdsToDropDown(all_bill_ids){
    
    $('#all_bill_ids_for_table_session option').remove();
    $('#all_bill_ids_for_table_session').attr('disabled',false);
    if(all_bill_ids.length==1){
    bill_id = all_bill_ids[0];
    $('#all_bill_ids_for_table_session').append('<option value="'+bill_id+'">'+'Bill ID -'+ bill_id.slice(-5)+'</option>');
    $('#all_bill_ids_for_table_session').attr('disabled',true);
    if($('#all_settlement_modes_drop_down').val() == 'cash') {$('#cash_calculator').removeClass('hidden');}
    fetchBillAmountFromBillID(bill_id);
    }
    if(all_bill_ids.length > 1) {
    
    $('#all_bill_ids_for_table_session').append('<option value="0">select bill id</option>');
    $.each(all_bill_ids, function(index, value) {
         $('#all_bill_ids_for_table_session')
              .append($('<option>', { value : value })
              .text('Bill ID -'+ value.slice(-5)));
    });}
    $("#all_bill_ids_for_table_session").focus();
    $("#all_settlement_modes").addClass('hidden');

    bindChangeOfBillID();
}


function bindChangeOfBillID(){
$("#all_bill_ids_for_table_session").on('change', (function() {
    var bill_id = this.value;
    $('#all_settlement_modes_drop_down').val('0');
    $('.all_settlement_modes_content').addClass('hidden');
    if(bill_id=='0'){$('#bill_amount_box').addClass('hidden'); return;}
    fetchBillAmountFromBillID(bill_id);
}));
}



function fetchBillAmountFromBillID(bill_id){
    var data = {};
    data['bill_id'] = bill_id;
    var url = '/get-bill-amount-for-bill-id';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            var bill_amount = data.bill_amount;
             $('#bill_amount_box').removeClass('hidden');
             $('#bill_amount').html(bill_amount.toFixed(2));
             $("#all_settlement_modes").removeClass('hidden');
             $('#all_settlement_modes_drop_down').unbind('change');
             $("#all_settlement_modes_drop_down").focus();
            bindChangeOfSettlementMode();

        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });
}

function bindChangeOfSettlementMode(){
$("#all_settlement_modes_drop_down").on('change', (function() {
    var settlement_mode_id = this.value;
    $('.all_settlement_modes_content').addClass('hidden');
    if(settlement_mode_id=='0'){$('.all_settlement_modes_content').addClass('hidden');$('#cash_calculator').addClass('hidden');return;}
    showSettlementPageForSettlementMode(settlement_mode_id);
    if(settlement_mode_id=='multiple') { bindMultipleSettlementAmountInputBoxes();}
}));
}


function showSettlementPageForSettlementMode(settlement_mode_id) {
    $('.all_settlement_modes_content').addClass('hidden');
    $('#'+settlement_mode_id).removeClass('hidden');
    if(settlement_mode_id == 'cash'){ 
        $('#cash_calculator').removeClass('hidden');
        $('#tender').focus();
        reBindKeyUpAndacceptOnlyNumbers();
    } else {
        $('#cash_calculator').addClass('hidden');
    }
    
}

/* General instruction Accordion */
$("#cash_calculator_content").slideUp(0);
$("#accordion_click_section_calculator_cash_settlement").click(function() {
    $("#cash_calculator_content").slideUp("fast");
    $('#accordion_click_section_calculator_cash_settlement').html('<i class="fa fa-chevron-circle-down"></i> cash calculator <i class="fa fa-calculator"></i>');
    $(this).find('.expand').removeClass('collapse');
    if ($(this).next().is(":hidden") == true) {
        $('.expand').removeClass('collapse');
        $(this).next().slideDown("normal");
        $('#accordion_click_section_calculator_cash_settlement').html('<i class="fa fa-chevron-circle-up"></i> cash calculator <i class="fa fa-calculator"></i>');
        $(this).find('.expand').addClass('collapse');
        bindAccordionKeyUpAndacceptOnlyNumbers();
    }
});


function reBindKeyUpAndacceptOnlyNumbers(){
    $('#tender').off('keyup');
    $('#tender').on('keyup', (function (e) {
        this.value = this.value.replace(/[^0-9]/g,'');        
        var tender_amount = this.value;
        updateCashRefundAmount(tender_amount);
    
   }));
}


function updateCashRefundAmount(tender_amount){
    var total_bill = parseFloat($('#bill_amount').html());
    if(tender_amount < total_bill) {$('#refund_amount').html(''); return;}
    var amount_to_refund = tender_amount - total_bill;
    $('#refund_amount').html('₹ ' + amount_to_refund.toFixed(2));

}


function bindAccordionKeyUpAndacceptOnlyNumbers(){
    var denominations = [1000,500,100,50,20, 10,5,2,1];
    $.each(denominations, function(index, el) {
    $('#input_'+el).off('keyup');
    $('#input_'+el).on('keyup', (function (e) {
        this.value = this.value.replace(/[^0-9]/g,'');        
        updateDenominationTotalAmount();        
    }));
   });
}

function updateDenominationTotalAmount() {
    var denominations = [1000,500,100,50,10,20, 5,2,1];
    var denominations_total = 0;
    $.each(denominations, function(index, el) {
        var number_of_currencies = parseFloat($('#input_'+el).val());
        number_of_currencies = number_of_currencies || 0;
        var amount = el * number_of_currencies;
        denominations_total = denominations_total + amount;
        
    });
    
    $('td#total_of_all_currencies').html(denominations_total);

}      

function getAllSettlementModeIDs(){
    var all_settlement_mode_ids = new Array();
    $('#all_settlement_modes_drop_down option').each(function(){
        all_settlement_mode_ids.push(this.value);
    });
    all_settlement_mode_ids.splice(0, 1); // remove 0
    all_settlement_mode_ids.splice(-1, 1); // remove multiple
    return all_settlement_mode_ids;
}


function bindMultipleSettlementAmountInputBoxes(){
    var all_settlement_mode_ids = getAllSettlementModeIDs();
    for (id in all_settlement_mode_ids){
        var id_to_bind = '#multiple_'+all_settlement_mode_ids[id] + '_amount';
        $(id_to_bind).off('keyup');
        $(id_to_bind).on('keyup', (function (e) {
            this.value = this.value.replace(/[^0-9]/g,'');        
            updateMultipleAccountedAmount(all_settlement_mode_ids);        
        }));
    }
}

function updateMultipleAccountedAmount(all_settlement_mode_ids){
    var total_amount = 0;
    for (index in all_settlement_mode_ids){
        var amount_for_this_settlement_mode = parseFloat($('#multiple_'+all_settlement_mode_ids[index] + '_amount').val()) || 0 ;
        total_amount += amount_for_this_settlement_mode; 
    }
    $('#multiple_accounted_amount').html(total_amount.toFixed(2));
    var bill_amount =  parseFloat($('#bill_amount').html());
    var remaining_amount = bill_amount - total_amount;
    $('#multiple_remaining_amount').html(remaining_amount.toFixed());
    if (bill_amount == total_amount){
        $('.checkmark').remove();
        $("#multiple_accounted_amount").removeClass('red').addClass('green').fadeIn(1200);
        $( '<span class="checkmark"><i class="green fa fa-check"></i></span>' ).insertAfter( "#multiple_accounted_amount", 800 );
        $('#mark_as_settled_btn_multiple').prop('disabled', false);
    }
    else{
        $('.checkmark').remove();
        $("#multiple_accounted_amount").addClass('red');  
        $( '<span class="checkmark"><i class="red fa fa-close"></i></span>' ).insertAfter( "#multiple_accounted_amount", 800 );
        $('#mark_as_settled_btn_multiple').prop('disabled', true);
    }
}


$('#mark_as_settled_btn_cash').click(on_cash_mark_as_settled_btn_clicked);
$('#mark_as_settled_btn_multiple').click(on_multiple_mark_as_settled_btn_clicked);

function bindAllOtherSettlementButtonsClciked(){ 
    var all_settlement_mode_ids = getAllSettlementModeIDs();
    all_settlement_mode_ids.splice(0, 1) // cash removed as handled separately
    for (index in all_settlement_mode_ids){
        $("#mark_as_settled_btn_"+all_settlement_mode_ids[index]).click({
            'settlement_mode_id': all_settlement_mode_ids[index]}, on_others_mark_as_settled_button_clicked);
    }
}
bindAllOtherSettlementButtonsClciked();

function on_cash_mark_as_settled_btn_clicked(){
    var bill_amount =  parseFloat($('#bill_amount').html());
    var tender_amount = $('#tender').val();
    
    if (tender_amount == '' || tender_amount < bill_amount ){bootbox.alert('you must enter a valid tender amount');}
    var settlement_detail = {"cash": [bill_amount, 'tendered:'+ tender_amount]};
    sendSettlementRequestToServer(settlement_detail);
}

function on_others_mark_as_settled_button_clicked(event){
    var settlement_mode_id = event.data.settlement_mode_id;
    var payment_details = $('#input_'+settlement_mode_id+'_payment_detail').val();
    if(payment_details==''){bootbox.alert('Payment Detail Must be provided'); $('#input_'+settlement_mode_id+'_payment-detail').focus(); return;}
    var bill_amount =  parseFloat($('#bill_amount').html());
    log(settlement_mode_id);
    var settlement_detail = {};
    settlement_detail[settlement_mode_id] = [bill_amount, payment_details];
    sendSettlementRequestToServer(settlement_detail);
}

function on_multiple_mark_as_settled_btn_clicked(){
    var bill_amount =  parseFloat($('#bill_amount').html());
    var total = 0;
    var all_settlement_mode_ids = getAllSettlementModeIDs();
    var settlement_detail = {};
    for (index in all_settlement_mode_ids){
        var amount = parseFloat($('#multiple_'+ all_settlement_mode_ids[index] +'_amount').val()) || 0;
        total = total + amount;
        var payment_details =  $('#multiple_'+all_settlement_mode_ids[index] +'_payment_detail').val();
        if(all_settlement_mode_ids[index]=='cash'){ payment_details = "na"; }
        if(amount >0 && payment_details==''){ bootbox.alert('you must specify payment details for all used settled methods'); return;}
        if(amount >0){
            settlement_detail[all_settlement_mode_ids[index]] = [amount, payment_details];
        } 
    }
    if(total != bill_amount) { bootbox.alert('sum of all payment methods must equal the total bill amount'); }
    else { sendSettlementRequestToServer(settlement_detail);}

}



function sendSettlementRequestToServer(settlement_details){
    var data = {};
    var bill_id = $('#all_bill_ids_for_table_session').val();
    var table_session_id = $('#all_unsettled_table_drop_down').val();
    data['bill_id'] = bill_id;
    data['table_session_id'] = table_session_id;
    data['settlement_details'] = settlement_details;
    var url = '/settle-bill';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            if(data){
                    sessionStorage.billSettled = 'true';
                    window.location.reload();
            }

        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });
}

$( function () {
    if ( sessionStorage.billSettled == 'true' ) {
        $('#all_unsettled_table_drop_down').val('0');
        bootbox.alert( 'bill settled' );
        $('#all_unsettled_table_drop_down').focus();
        sessionStorage.billSettled = 'false';
    }
} );