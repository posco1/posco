Mousetrap.prototype.stopCallback = function () {
     return false;
}

Mousetrap.bind( 'ctrl+1', function(){loadUrl('/order')});
Mousetrap.bind( 'alt+shift+b', function(){loadUrl('/billing')});
Mousetrap.bind( 'alt+shift+s', function(){loadUrl('/settlement')});
Mousetrap.bind( 'alt+shift+l', function(){loadUrl('/logout')});
Mousetrap.bind( 'alt+shift+d', function(){loadUrl('/dashboard')});

function loadUrl(newLocation)
{
window.location = newLocation;
return false;
}

