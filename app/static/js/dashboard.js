$('#sendAdhocMessage').click(onAdhocSendMessageButtonClicked);

function onAdhocSendMessageButtonClicked(){
    log('bingo');
    var printer_id = $('#printer_id').val();
    if(printer_id == '0') {bootbox.alert('Must select a printer'); return;}
    var message = $('#adhoc_message_content').val();
    if (message == '')  {bootbox.alert('Message is empty'); return;}
    var printer_name = $('#printer_id option:selected').text();
    sendPrintRequest(printer_name, message)
}


function sendPrintRequest(printer_name, message){
    var data = {};
    data['printer_name'] = printer_name;
    data['message'] = message;
    var url = '/print-adhoc-message';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
               if(data="adhoc message printed") {
                    sessionStorage.adhocMessagePrinted = 'true';
                    window.location.reload();
            }

        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });


}


$( function () {
    if ( sessionStorage.adhocMessagePrinted == 'true' ) {
        $('#printer_id').val('0');
        bootbox.alert( "message printed \n Temporarily visit <a href='/adhoc-temp-prints' target='_blank'>here</a> to see printed msg" );
        $('#printer_id').focus();
        sessionStorage.adhocMessagePrinted = 'false';
        $('#adhoc_message_content').val('');
    }
} )
