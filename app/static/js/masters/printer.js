
$("#printer").focus();

var table;

$(function() {
      table = $("#all_printer_table").DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "columns": [null, {"orderable": false}],
          "info": true,
          "autoWidth": true,
          "stateSave": true,
          "scrollX": true
      });
  });

/*

Delete

*/

$('.button-delete').on("click", deleteRow);

function deleteRow(){
    var rowToBeRemoved =   table.row( $(this).parents('tr') );
    var trClicked = $(this).parent().parent();
    var tdprinter = trClicked.children("td:nth-child(1)");
    var printerToBeDeleted = tdprinter.html()
    deleteprinter(printerToBeDeleted, rowToBeRemoved);
}

  function deleteprinter(printerToBeDeleted, rowToBeRemoved) {
          bootbox.confirm("Remove <strong>" + printerToBeDeleted + "<strong> ?",
          function(confirmedOK) {
              if (confirmedOK) {
                  sendDeleteprinterRequest(printerToBeDeleted, rowToBeRemoved);
              }
          });
   }

  function sendDeleteprinterRequest(printerToBeDeleted, rowToBeRemoved) {
      var data = {};
      data['printer_to_be_deleted'] = printerToBeDeleted;
      var url = '/delete-printer';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "printer deleted successfully") {
                  rowToBeRemoved.remove().draw();
                  bootbox.alert("printer <strong> "+ printerToBeDeleted + "</strong> deleted");
                      $("#printer").focus();
                  }
              else if (data == "cannot delete printer as it is associated with an active subcategory"){
              bootbox.alert('cannot delete printer as it is associated with an active subcategory');
              }
              $("#printer_name").focus();
               },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
      });

  }

/*


Edit printer


*/

$('.button-edit').on("click", editRow);


function editRow() {
    $(".button-edit").off("click",editRow);
    $('.button-delete').off("click", deleteRow);
    var current_row = $(this).parent().parent();
    var printerTd = current_row.children("td:nth-child(1)");
    var buttonTd = current_row.children("td:nth-child(2)")
    printerTd.addClass("cellEditing");
    var printer0 = printerTd.html();
    printerTd.html("<input class='form-control input-md' id='dyna_printer' type='text' value='" + printerTd.html() + "' />");
    buttonTd.html('<span class="button-save" title="Save"><i class="fa fa-save"></i> Save</span>');
    $(".button-save").on('click', {printer0: printer0, current_row: current_row}, onSaveButtonClicked);
    $('#dyna_printer').focus();
}


var editErrorMessage;

function onSaveButtonClicked(e){
  var printer0 = e.data.printer0;
  var printer1 = $('#dyna_printer').val();
  var current_row = e.data.current_row;
  if (validateprinterEdit(printer1)){
            sendEditprinterRequest(printer0, printer1,current_row)}
  else{
    bootbox.alert(editErrorMessage); $('#dyna_printer').focus();
    }

}

 function validateprinterEdit(printer1) {
      if (printer1.length == 0) {
          editErrorMessage = "printer name cannot be empty";
          return false;
      }
      var $tds = $('#all_printer_table tr > td:nth-child(1)').filter(function() {
          return $.trim($(this).text().toUpperCase()) == printer1.toUpperCase();
      });
      if ($tds.length != 0) {
          editErrorMessage = "Could not update, printer by that name already exists";
          return false;
      }
      return true;
  }

  function sendEditprinterRequest(printer_0, printer_1, current_row) {
      var data = {};
      data['printer_0'] = printer_0;
      data['printer_1'] = printer_1;
      var url = '/edit-printer';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "printer edited successfully") {
                  bootbox.alert("printer Updated");
                  var printerTd = current_row.children("td:nth-child(1)");
                  var buttonTd = current_row.children("td:nth-child(2)")
                  printerTd.html(printer_1);
                  buttonTd.html('<span class="button-edit" title="Edit"><i class="fa fa-edit"></i> Edit</span> | <span class="button-delete" title="Delete"><i class="fa fa-trash"></i> Delete</span>');
                  $('#dyna_printer').removeAttr('id');
                  $(".button-edit").on("click",editRow);
                  $('.button-delete').on("click", deleteRow);
                  $("#printer").focus();
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
      });
  }




