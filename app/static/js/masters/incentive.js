$("#incentive").focus();

var table;

$(function() {
      table = $("#all_incentives_table").DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "columns": [null, {"orderable": false}],
          "info": true,
          "autoWidth": true,
          "stateSave": true,
          "scrollX": true
      });
  });

/*

Delete

*/

$('.button-delete').on("click", deleteRow);

function deleteRow(){
    var rowToBeRemoved =   table.row( $(this).parents('tr') );
    var trClicked = $(this).parent().parent();
    var tdIncentive = trClicked.children("td:nth-child(1)");
    var incentiveToBeDeleted = tdIncentive.html()
    deleteIncentive(incentiveToBeDeleted,rowToBeRemoved);
}

  function deleteIncentive(incentiveToBeDeleted, rowToBeRemoved) {
          bootbox.confirm("Delete Incentive <strong>" + incentiveToBeDeleted + "<strong> ?",
          function(confirmedOK) {
              if (confirmedOK) {
                  sendDeleteIncentiveRequest(incentiveToBeDeleted, rowToBeRemoved);
              }
          });
   }

  function sendDeleteIncentiveRequest(incentiveToBeDeleted, rowToBeRemoved) {
      var data = {};
      data['incentive_to_be_deleted'] = incentiveToBeDeleted;
      var url = '/delete-incentive';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "incentive deleted successfully") {
                      rowToBeRemoved.remove().draw();
                      bootbox.alert("Incentive <strong> "+ incentiveToBeDeleted + "</strong> deleted");
                      $("#incentive").focus();
              }
              else if (data == "cannot delete as incentive has associated menu items"){
               bootbox.alert("cannot delete incentive <strong>"+ incentiveToBeDeleted + " </strong> as it has associated menu items");
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
              bootbox.alert(errorThrown);
          }
      });

  }

/*


Edit


*/

var all_categories_object;

$('.button-edit').on("click", editRow);


function editRow() {
    $(".button-edit").off("click",editRow);
    $('.button-delete').off("click", deleteRow);
    var trClicked = $(this).parent().parent();
    var incentiveTd = trClicked.children("td:nth-child(1)");
    var buttonTd = trClicked.children("td:nth-child(2)")
    incentiveTd.addClass("cellEditing");
    var incentive0 = incentiveTd.html();
    incentiveTd.html("<input class='form-control input-md' id='dyna_incentive' type='text' value='" + incentiveTd.html() + "' />");
    bindSaveToEnterKey('#dyna_incentive', incentive0,trClicked );
    buttonTd.html('<span class="button-save" title="Save"><i class="fa fa-save"></i> Save</span>');
    $(".button-save").on('click', {incentive0: incentive0, currentrow: trClicked}, onSaveButtonClicked);
    $('#dyna_incentive').focus();
}


function bindSaveToEnterKey(idName, incentive0,trClicked){
$( idName ).keypress(function( event ) {
  if ( event.which == 13 ) {
      event.data = {};
      event.data.incentive0 = incentive0;
      event.data.currentrow = trClicked;
     onSaveButtonClicked(event);
     event.preventDefault();
  }
});

}


var editErrorMessage;

function onSaveButtonClicked(e){
  var incentive0 = e.data.incentive0;
  var incentive1 = $('#dyna_incentive').val();
  var category1 = $('#categories_selector').val();
  var currentrow = e.data.currentrow;
  if (validateIncentiveEdit(incentive1)){sendEditIncentiveRequest(incentive0, incentive1, currentrow)}
  else{bootbox.alert(editErrorMessage);$('#dyna_incentive').focus();}
}





 function validateIncentiveEdit(incentive1) {
      if (incentive1.length == 0) {
          editErrorMessage = "Incentive cannot be empty";
          return false;
      }
      if (isNaN(incentive1)) {
          editErrorMessage = "Incentive must be a valid number";
          return false;
      }
      
      if ((incentive1 < 0)) {
          editErrorMessage = "Incentive must be a positive number";
          return false;
      }
      var $tds = $('#all_incentives_table tr > td:nth-child(1)').filter(function() {
          return Number($(this).text()).toFixed(1) == Number(incentive1).toFixed(1);
      });
      if ($tds.length != 0) {
          editErrorMessage = "Could not update as Incentive Already Exists";
          return false;
      }

      return true;
  }

  function sendEditIncentiveRequest(incentive_0, incentive_1, currentrow) {
      var data = {};
      data['incentive_0'] = incentive_0;
      data['incentive_1'] = incentive_1;
      var url = '/edit-incentive';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "incentive edited successfully") {
                  bootbox.alert("Incentive Category Updated");
                  var incentiveTd = currentrow.children("td:nth-child(1)");
                  var buttonTd = currentrow.children("td:nth-child(2)")
                  incentiveTd.html(incentive_1);
                  buttonTd.html('<span class="button-edit" title="Edit"><i class="fa fa-edit"></i> Edit</span> | <span class="button-delete" title="Delete"><i class="fa fa-trash"></i> Delete</span>');
                  $('#dyna_incentive').removeAttr('id');
                  $(".button-edit").on("click",editRow);
                  $('.button-delete').on("click", deleteRow);
                  $("#incentive").focus();
              }

          },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
      });
  }




