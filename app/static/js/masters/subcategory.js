/*Hello*/

$("#subcategory_name").focus();

var table;

$(function() {
      table = $("#all_subcategories_table").DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "columns": [null, null, null, {"orderable": false}],
          "info": true,
          "autoWidth": true,
          "stateSave": true,
            "scrollX": true,
          "fnDrawCallback": function() {
                if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                       // $('.dataTables_filter').css("display", "none");
                }
            }
      });
  });

/*

Delete

*/

$('.button-delete').on("click", deleteRow);

function deleteRow(){
    var rowToBeRemoved =   table.row( $(this).parents('tr') );
    var trClicked = $(this).parent().parent();
      var tdSubCategoryName = trClicked.children("td:nth-child(1)");
      var subcategoryNameToBeDeleted = tdSubCategoryName.html()
      deleteSubCategory(subcategoryNameToBeDeleted, rowToBeRemoved);
}

  function deleteSubCategory(subcategoryNameToBeDeleted, rowToBeRemoved) {
          bootbox.confirm("Delete sub category <strong>" + subcategoryNameToBeDeleted + "<strong> ?",
          function(confirmedOK) {
              if (confirmedOK) {
                  sendDeleteSubCategoryRequest(subcategoryNameToBeDeleted, rowToBeRemoved);
              }
          });
   }

  function sendDeleteSubCategoryRequest(subcategoryNameToBeDeleted, rowToBeRemoved) {
      var data = {};
      data['subcategory_name_to_be_deleted'] = subcategoryNameToBeDeleted;
      var url = '/delete-subcategory';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "subcategory deleted successfully") {
                      rowToBeRemoved.remove().draw();
                      bootbox.alert("Sub Category<strong> "+ subcategoryNameToBeDeleted + "</strong> Deleted");
                      $("#subcategory_name").focus();
              }
              else if (data == "cannot delete as subcategory has associated menu items"){
                bootbox.alert("cannot delete <strong>"+ subcategoryNameToBeDeleted + " </strong> as subcategory has associated menu items");
              }
              else {
                bootbox.alert("an unexpected event occurred");
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
      });

  }

/*


Edit Subcategory


*/

var all_categories_object;

$('.button-edit').on("click", editRow);


function editRow() {
    $(".button-edit").off("click",editRow);
    $('.button-delete').off("click", deleteRow);
    var trClicked = $(this).parent().parent();
    var subcategoryTd = trClicked.children("td:nth-child(1)");
    var categoryTd = trClicked.children("td:nth-child(2)");
    var printerTd = trClicked.children("td:nth-child(3)");
    var buttonTd = trClicked.children("td:nth-child(4)")
    subcategoryTd.addClass("cellEditing");
    categoryTd.addClass("cellEditing");
    printerTd.addClass("cellEditing");
    var subcategory0 = subcategoryTd.html();
    var category0 = categoryTd.html();
    var printer0 = printerTd.html();
    subcategoryTd.html("<input class='form-control input-md' id='dyna_subcategory' type='text' value='" + subcategoryTd.html() + "' />");
    categoryTd.html('<select class="form-control input-md" id="categories_selector"><option value="0">--select category--</option></select>');
    createCategoryDropDown(category0);
    printerTd.html('<select class="form-control input-md" id="printers_selector"><option value="0">--select printer--</option></select>');
    createPrinterDropDown(printer0);
    buttonTd.html('<span class="button-save" title="Save"><i class="fa fa-save"></i> Save</span>');
    $(".button-save").on('click', {subcategory0: subcategory0, category0:category0, printer0: printer0, currentrow: trClicked}, onSaveButtonClicked);
    $('#dyna_subcategory').focus();
}


function createCategoryDropDown(category0){
      var url = '/get-all-active-categories-with-id';
      $.ajax({
          url: url,
          type: "GET",
          success: function(all_categories, textStatus, jqXHR) {
            var select = document.getElementById("categories_selector");
            all_categories_object = all_categories;
            for (var key in all_categories) {
                if(all_categories[key]==category0){var itemval= '<option selected="selected" value="'+ key +'">'+ all_categories[key] +'</option>'}
                else{var itemval= '<option value="'+ key +'">'+ all_categories[key] +'</option>'};
                $("#categories_selector").append(itemval);
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
          });
        }


function createPrinterDropDown(printer0){
      var url = '/get-all-active-printers-with-id';
      $.ajax({
          url: url,
          type: "GET",
          success: function(all_printers, textStatus, jqXHR) {
            var select = document.getElementById("printers_selector");
            all_printers_object = all_printers;
            for (var key in all_printers) {
                if(all_printers[key]==printer0){var itemval= '<option selected="selected" value="'+ key +'">'+ all_printers[key] +'</option>'}
                else{var itemval= '<option value="'+ key +'">'+ all_printers[key] +'</option>'};
                $("#printers_selector").append(itemval);
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
          });
        }



var editErrorMessage;

function onSaveButtonClicked(e){
  var subcategory0 = e.data.subcategory0;
  var category0 = e.data.category0;
  var printer0 = e.data.printer0;
  var subcategory1 = $('#dyna_subcategory').val();
  var category1 = $('#categories_selector').val();
  var printer1 = $('#printers_selector').val();
  var currentrow = e.data.currentrow;
  if (validateSubCategoryEdit(subcategory1, category1, printer1)){
            sendEditSubCategoryRequest(subcategory0, subcategory1,
                                        category1, printer1, currentrow)}
  else{bootbox.alert(editErrorMessage);}
}


 function validateSubCategoryEdit(subcategory1, category1, printer1) {
      if((category1==null) || (category1==0)){
        editErrorMessage = "Please select category";
        return false;
      }
      if((printer1==null) || (printer1==0)){
        editErrorMessage = "Please select printer";
        return false;
      }
      if (subcategory1.length == 0) {
          editErrorMessage = "Sub Category Name cannot be empty";
          return false;
      }
      var $tds = $('#all_subcategories_table tr > td:nth-child(1)').filter(function() {
          return $.trim($(this).text().toUpperCase()) == subcategory1.toUpperCase();
      });
      if ($tds.length != 0) {
          editErrorMessage = "Could not update as Sub Category Already Exists";
          return false;
      }

      return true;
  }

  function sendEditSubCategoryRequest(subcategory0, subcategory1, categoryid1,
                 printer1, currentrow) {
      var data = {};
      data['subcategory0'] = subcategory0;
      data['subcategory1'] = subcategory1;
      data['category1'] = categoryid1;
      data['printer1'] = printer1;
      var url = '/edit-subcategory';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "subcategory edited successfully") {
                  bootbox.alert("Sub category detail updated");
                  var subcategoryTd = currentrow.children("td:nth-child(1)");
                  var categoryTd = currentrow.children("td:nth-child(2)");
                  var printerTd = currentrow.children("td:nth-child(3)");
                  var buttonTd = currentrow.children("td:nth-child(4)")
                  subcategoryTd.html(subcategory1);
                  categoryTd.html(all_categories_object[categoryid1]);
                  printerTd.html(all_printers_object[printer1])
                  buttonTd.html('<span class="button-edit" title="Edit"><i class="fa fa-edit"></i> Edit</span> | <span class="button-delete" title="Delete"><i class="fa fa-trash"></i> Delete</span>');
                  $('#dyna_subcategory').removeAttr('id');
                  $('#categories_selector').removeAttr('id');
                  $('#printers_selector').removeAttr('id');
                  $(".button-edit").on("click",editRow);
                  $('.button-delete').on("click", deleteRow);
                  $("#subcategory_name").focus();
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
      });
  }




