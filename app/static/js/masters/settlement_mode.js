
$("#settlementMode").focus();

var table;

$(function() {
      table = $("#all_settlement_mode_table").DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "columns": [null, {"orderable": false}],
          "info": true,
          "autoWidth": true,
          "stateSave": true,
          "scrollX": true
      });
  });

/*

Delete

*/

$('.button-delete').on("click", deleteRow);

function deleteRow(){
    var rowToBeRemoved =   table.row( $(this).parents('tr') );
    var trClicked = $(this).parent().parent();
    var tdSettlementMode = trClicked.children("td:nth-child(1)");
    var settlementModeToBeDeleted = tdSettlementMode.html()
    deleteSettlementMode(settlementModeToBeDeleted, rowToBeRemoved);
}

  function deleteSettlementMode(settlementModeToBeDeleted, rowToBeRemoved) {
          bootbox.confirm("Remove <strong>" + settlementModeToBeDeleted + "<strong> ?",
          function(confirmedOK) {
              if (confirmedOK) {
                  sendDeleteSettlementModeRequest(settlementModeToBeDeleted, rowToBeRemoved);
              }
          });
   }

  function sendDeleteSettlementModeRequest(settlementModeToBeDeleted, rowToBeRemoved) {
      var data = {};
      data['settlement_mode_to_be_deleted'] = settlementModeToBeDeleted;
      var url = '/delete-settlement-mode';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "settlement mode deleted successfully") {
                  rowToBeRemoved.remove().draw();
                  bootbox.alert("settlement mode <strong> "+ settlementModeToBeDeleted + "</strong> deleted");
                      $("#settlementMode").focus();
                  }
              $("#settlementMode_name").focus();
               },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
      });

  }

/*


Edit settlementMode


*/

$('.button-edit').on("click", editRow);


function editRow() {
    $(".button-edit").off("click",editRow);
    $('.button-delete').off("click", deleteRow);
    var current_row = $(this).parent().parent();
    var settlementModeTd = current_row.children("td:nth-child(1)");
    var buttonTd = current_row.children("td:nth-child(2)")
    settlementModeTd.addClass("cellEditing");
    var settlementMode0 = settlementModeTd.html();
    settlementModeTd.html("<input class='form-control input-md' id='dyna_settlement_mode' type='text' value='" + settlementModeTd.html() + "' />");
    buttonTd.html('<span class="button-save" title="Save"><i class="fa fa-save"></i> Save</span>');
    $(".button-save").on('click', {settlementMode0: settlementMode0, current_row: current_row}, onSaveButtonClicked);
    $('#dyna_settlementMode').focus();
}


var editErrorMessage;

function onSaveButtonClicked(e){
  var settlementMode0 = e.data.settlementMode0;
  var settlementMode1 = $('#dyna_settlement_mode').val();
  var current_row = e.data.current_row;
  if (validatesettlementModeEdit(settlementMode1)){
            sendEditsettlementModeRequest(settlementMode0, settlementMode1,current_row)}
  else{
    bootbox.alert(editErrorMessage); $('#dyna_settlement_mode').focus();
    }

}

 function validatesettlementModeEdit(settlementMode1) {
    var re = /^[0-9a-zA-Z ]+$/;
    if ( ! re.test(settlementMode1) ){
          editErrorMessage = "only alpahbets, numbers and space allowed";
          return false;
      }
      if (settlementMode1.length == 0) {
          editErrorMessage = "settlement mode name cannot be empty";
          return false;
      }
      var $tds = $('#all_settlementMode_table tr > td:nth-child(1)').filter(function() {
          return $.trim($(this).text().toUpperCase()) == settlementMode1.toUpperCase();
      });
      if ($tds.length != 0) {
          editErrorMessage = "Could not update, settlement mode by that name already exists";
          return false;
      }
      return true;
  }

  function sendEditsettlementModeRequest(settlement_mode_0, settlement_mode_1, current_row) {
      var data = {};
      data['settlement_mode_0'] = settlement_mode_0;
      data['settlement_mode_1'] = settlement_mode_1;
      var url = '/edit-settlement-mode';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "settlement mode edited successfully") {
                  bootbox.alert("settlement mode updated");
                  var settlementModeTd = current_row.children("td:nth-child(1)");
                  var buttonTd = current_row.children("td:nth-child(2)")
                  settlementModeTd.html(settlement_mode_1);
                  buttonTd.html('<span class="button-edit" title="Edit"><i class="fa fa-edit"></i> Edit</span> | <span class="button-delete" title="Delete"><i class="fa fa-trash"></i> Delete</span>');
                  $('#dyna_settlement_mode').removeAttr('id');
                  $(".button-edit").on("click",editRow);
                  $('.button-delete').on("click", deleteRow);
                  $("#settlementMode").focus();
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
      });
  }




