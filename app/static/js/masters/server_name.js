/*Hello*/
$("#server_name").focus();

var table;

$(function() {
      table = $("#all_server_name_table").DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "columns": [null, {"orderable": false}],
          "info": true,
          "autoWidth": true,
          "stateSave": true,
          "scrollX": true
      });
  });

/*

Delete

*/

$('.button-delete').on("click", deleteRow);

function deleteRow(){
    var rowToBeRemoved =   table.row( $(this).parents('tr') );
    var trClicked = $(this).parent().parent();
    var tdserverName = trClicked.children("td:nth-child(1)");
    var serverNameToBeDeleted = tdserverName.html()
    deleteTableName(serverNameToBeDeleted, rowToBeRemoved);
}

  function deleteTableName(serverNameToBeDeleted, rowToBeRemoved) {
          bootbox.confirm("Remove <strong>" + serverNameToBeDeleted + "<strong> ?",
          function(confirmedOK) {
              if (confirmedOK) {
                  sendDeleteTableNameRequest(serverNameToBeDeleted, rowToBeRemoved);
              }
          });
   }

  function sendDeleteTableNameRequest(serverNameToBeDeleted, rowToBeRemoved) {
      var data = {};
      data['server_name_to_be_deleted'] = serverNameToBeDeleted;
      var url = '/delete-server-name';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "server name deleted successfully") {
                  rowToBeRemoved.remove().draw();
                  bootbox.alert("Server <strong> "+ serverNameToBeDeleted + "</strong> removed");
                      $("#server_name").focus();
                  }
               },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);

          }
      });

  }

/*


Edit Subcategory


*/

$('.button-edit').on("click", editRow);


function editRow() {
    $(".button-edit").off("click",editRow);
    $('.button-delete').off("click", deleteRow);
    var trClicked = $(this).parent().parent();
    var serverNameTd = trClicked.children("td:nth-child(1)");
    var buttonTd = trClicked.children("td:nth-child(2)")
    serverNameTd.addClass("cellEditing");
    var serverName0 = serverNameTd.html();
    serverNameTd.html("<input class='form-control input-md' id='dyna_serverName' type='text' value='" + serverNameTd.html() + "' />");
    bindSaveToEnterKey('#dyna_serverName', serverName0,trClicked );
    buttonTd.html('<span class="button-save" title="Save"><i class="fa fa-save"></i> Save</span>');
    $(".button-save").on('click', {serverName0: serverName0, currentrow: trClicked}, onSaveButtonClicked);
    $('#dyna_serverName').focus();
}


function bindSaveToEnterKey(idName, serverName0,trClicked){
$( idName ).keypress(function( event ) {
  if ( event.which == 13 ) {
      event.data = {};
      event.data.serverName0 = serverName0;
      event.data.currentrow = trClicked;
     onSaveButtonClicked(event);
     event.preventDefault();
  }
});
}


var editErrorMessage;

function onSaveButtonClicked(e){
  var serverName0 = e.data.serverName0;
  var serverName1 = $('#dyna_serverName').val();
  var currentrow = e.data.currentrow;
  if (validateTableNameEdit(serverName1)){sendEditTableNameRequest(serverName0, serverName1, currentrow)}
  else{bootbox.alert(editErrorMessage); $('#dyna_serverName').focus();}
  

}

 function validateTableNameEdit(serverName1) {
      if (serverName1.length == 0) {
          editErrorMessage = "server name cannot be empty";
          return false;
      }

      var $tds = $('#all_server_name_table tr > td:nth-child(1)').filter(function() {
          return $.trim($(this).text().toUpperCase()) == serverName1.toUpperCase();
      });
      if ($tds.length != 0) {
          editErrorMessage = "Could not update, server by that name already exists";
          return false;
      }

      return true;
  }

  function sendEditTableNameRequest(server_name_0, server_name_1, currentrow) {
      var data = {};
      data['server_name_0'] = server_name_0;
      data['server_name_1'] = server_name_1;
      var url = '/edit-server-name';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "server name edited successfully") {
                  bootbox.alert("Server Name Updated");
                  var serverNameTd = currentrow.children("td:nth-child(1)");
                  var buttonTd = currentrow.children("td:nth-child(2)")
                  serverNameTd.html(server_name_1);
                  buttonTd.html('<span class="button-edit" title="Edit"><i class="fa fa-edit"></i> Edit</span> | <span class="button-delete" title="Delete"><i class="fa fa-trash"></i> Delete</span>');
                  $('#dyna_serverName').removeAttr('id');
                  $(".button-edit").on("click",editRow);
                  $('.button-delete').on("click", deleteRow);
                  $("#server_name").focus();
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
      });
  }




