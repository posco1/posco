
$("#category").focus();

var table;

$(function() {
      table = $("#all_customers_table").DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "columns": [null, null, null, null, {"orderable": false}],
          "info": true,
          "autoWidth": true,
          "stateSave": true,
          "scrollX": true
      });
  });


/*


Edit Customer


*/

$('.button-edit').on("click", editRow);


function editRow() {
    $(".button-edit").off("click",editRow);
    var current_row = $(this).parent().parent();
    var customerNameTd = current_row.children("td:nth-child(1)");
    var customerEmailTd = current_row.children("td:nth-child(2)");
    var contactNumberTd = current_row.children("td:nth-child(3)");
    var customerAddressTd = current_row.children("td:nth-child(4)");
    var buttonTd = current_row.children("td:nth-child(5)")
    var customerName0 = customerNameTd.html();
    var customerEmail0 = customerEmailTd.html();
    var contactNumber0 = contactNumberTd.html();
    var customerAddress0 = customerAddressTd.html();
    customerNameTd.html("<input class='form-control input-md' id='dyna_customer_name' type='text' value='" + customerName0 + "' />");
    customerEmailTd.html("<input class='form-control input-md' id='dyna_customer_email' type='text' value='" + customerEmail0 + "' />");
    contactNumberTd.html("<input class='form-control input-md' id='dyna_contact_number' type='text' value='" + contactNumber0 + "' />");
    customerAddressTd.html("<input class='form-control input-md' id='dyna_customer_address' type='text' value='" + customerAddress0 + "' />");
    buttonTd.html('<span class="button-save" title="Save"><i class="fa fa-save"></i> Save</span>');
    $(".button-save").on('click', {customerName0: customerName0, customerEmail0: customerEmail0, contactNumber0: contactNumber0, 
      customerAddress0:customerAddress0, current_row: current_row}, onSaveButtonClicked);
    $('#dyna_category').focus();
}


var editErrorMessage;

function onSaveButtonClicked(e){
  var customerName0 = e.data.customerName0;
  var customerEmail0 = e.data.customerEmail0;
  var contactNumber0 = e.data.contactNumber0;
  var customerAddress0 = e.data.customerAddress0;
 
  var customerName1 = $('#dyna_customer_name').val();
  var customerEmail1 = $('#dyna_customer_email').val();
  var contactNumber1 = $('#dyna_contact_number').val();
  var customerAddress1 = $('#dyna_customer_address').val();
  var current_row = e.data.current_row;
  if (validateCustomerEdit(customerName1, customerEmail1,contactNumber1, customerAddress1)){
            sendEditCustomerRequest(contactNumber0, customerName1, customerEmail1, contactNumber1, customerAddress1, current_row)
                                    }

  else{
    bootbox.alert(editErrorMessage); $('#dyna_customer_name').focus();
    }
  

}

 function validateCustomerEdit(customerName1, customerEmail1, contactNumber1, customerAddress1) {
      if (contactNumber1.length == 0) {
          editErrorMessage = "contact number must be filled";
          return false;
      }
      if(customerEmail1 !=""){
        if(isNotAValidEmail(customerEmail1)){
          editErrorMessage = "Invalid email address";
          return false;
        }
      }
      var $tds = $('#all_category_table tr > td:nth-child(3)').filter(function() {
          return $.trim($(this).text().toUpperCase()) == contactNumber1.toUpperCase();
      });
      if ($tds.length != 0) {
          editErrorMessage = "Could not update, customer record with that contact number already exists";
          return false;
      }
      return true;
  }


  function isNotAValidEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return !(re.test(email));
}

  function sendEditCustomerRequest(contact_number_0, customer_name_1, email_address_1, contact_number_1, customer_address_1, current_row) {
      var data = {};
      data['contact_number_0'] = contact_number_0;
      data['customer_name_1'] = customer_name_1;
      data['email_address_1'] = email_address_1;
      data['contact_number_1'] = contact_number_1;
      data['customer_address_1'] = customer_address_1 ;
      var url = '/edit-customer';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "customer edited successfully") {
                  bootbox.alert("customer record updated");
                  var customerNameTd = current_row.children("td:nth-child(1)");
                  var customerEmailTd = current_row.children("td:nth-child(2)");
                  var contactNumberTd = current_row.children("td:nth-child(3)");
                  var customerAddressTd = current_row.children("td:nth-child(4)");
                  var buttonTd = current_row.children("td:nth-child(5)")
                  customerNameTd.html(customer_name_1);
                  customerEmailTd.html(email_address_1);
                  contactNumberTd.html(contact_number_1);
                  customerAddressTd.html(customer_address_1);
                  buttonTd.html('<span class="button-edit" title="Edit"><i class="fa fa-edit"></i> Edit</span> | <span class="button-delete" title="Delete"><i class="fa fa-trash"></i> Delete</span>');
                  $('#dyna_customer_name').removeAttr('id');
                  $('#dyna_customer_email').removeAttr('id');
                  $('#dyna_contact_number').removeAttr('id');
                  $('#dyna_customer_address').removeAttr('id');
                  $(".button-edit").on("click",editRow);
                  $("#category").focus();
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
      });
  }




