/*Hello*/
//$("#item_name").focus();
$('.preview_icon').hide();
var table;
$(document).ready(function() { 
table = $("#all_menu_items_table").DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "columns": [null, null, null, null, {
            "orderable": false
        }, {
            "orderable": false
        }],
        "info": true,
        "autoWidth": true,
        "stateSave": true,
         "scrollX": true

    });

}); 
/*

Multiple Delete

*/

   $('#all_menu_items_table tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('active');
    } );

    
    $('#delete_all_selected').click( function () {
        var data = table.rows( '.active' ).data();
        if(data.length==0){
            bootbox.alert('no rows selected for delete. \n Click on row to select it.');
            return
        }

        var all_menu_items_to_be_deleted = [];


        for (i = 0; i < data.length; i++) {
            all_menu_items_to_be_deleted.push(data[i][0]);

        }
        var url = '/delete-multiple-menu-item';
        $.ajax({
            type: "POST",
            url: url,
            data: {data:all_menu_items_to_be_deleted},
            success: function(msg){
                if (msg=='selected menu items removed') {
                    var rows = table.rows( '.active' );
                    rows.remove().draw();
                    bootbox.alert('selected menu items removed')
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }

        });
    } );


/*

Single Delete

*/

$('.button-delete').on("click", deleteRow);

function deleteRow() {
    var rowToBeRemoved = table.row($(this).parents('tr'));
    var trClicked = $(this).parent().parent();
    var tdMenuItemName = trClicked.children("td:nth-child(1)");
    var menuItemToBeDeleted = tdMenuItemName.html()
    deleteMenuItem(menuItemToBeDeleted, rowToBeRemoved);
}

function deleteMenuItem(menuItemToBeDeleted, rowToBeRemoved) {
    bootbox.confirm("Delete Menu Item <strong>" + menuItemToBeDeleted + "<strong> ?",
        function(confirmedOK) {
            if (confirmedOK) {
                sendDeleteMenuItemRequest(menuItemToBeDeleted, rowToBeRemoved);
            }
        });
}

function sendDeleteMenuItemRequest(menuItemToBeDeleted, rowToBeRemoved) {
    var data = {};
    data['menu_item_to_be_deleted'] = menuItemToBeDeleted;
    var url = '/delete-menu-item';
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        success: function(data, textStatus, jqXHR) {
            if (data == "menu item deleted successfully") {
                rowToBeRemoved.remove().draw();
                bootbox.alert("Item <strong> " + menuItemToBeDeleted + "</strong> deleted");
                $("#item_name").focus();
            } else {
                bootbox.alert("an unexpected event occurred. Please report");
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
             bootbox.alert(errorThrown);
        }
    });

}

/*


Edit


*/



$('.button-edit').on("click", editRow);
var editErrorMessage;


function editRow() {
    $(".button-edit").off("click", editRow);
    $('.button-delete').off("click", deleteRow);
    var trClicked = $(this).parent().parent();
    var menuItemTd = trClicked.children("td:nth-child(1)");
    var priceTd = trClicked.children("td:nth-child(2)");
    var subcategoryTd = trClicked.children("td:nth-child(3)");
    var incentivesTd = trClicked.children("td:nth-child(4)");
    var imageTd = trClicked.children("td:nth-child(5)");
    var buttonTd = trClicked.children("td:nth-child(6)");
    menuItemTd.addClass("cellEditing");
    priceTd.addClass("cellEditing");
    subcategoryTd.addClass("cellEditing");
    incentivesTd.addClass("cellEditing");
    imageTd.addClass("cellEditing");
    imageTd.attr('id', 'item_image');
    $('#item_image img .item_icon').addClass("cellEditing");
    $('#item_image img .preview_icon').addClass("cellEditing");
    buttonTd.addClass("cellEditing");

    var menuItem0 = menuItemTd.html();
    var price0 = priceTd.html();
    var subcategory0 = subcategoryTd.html();
    var incentives0 = incentivesTd.html();
    var image0html = imageTd.html();
    var image0 = $('#item_image img').attr('src').split('/').pop();
    menuItemTd.html("<input class='form-control input-md' id='dyna_menu_item' type='text' value='" + menuItem0 + "' />");
    priceTd.html("<input class='form-control input-md' id='dyna_price' type='text' value='" + price0 + "' />");
    subcategoryTd.html('<select class="form-control input-md" id="subcategories_selector"></select>');
    createSubCategoryDropDown(subcategory0);
    incentivesTd.html('<select class="form-control input-md" id="incentives_selector"></select>');
    createIncentivesDropDown(incentives0);
    imageTd.html(image0html + '<span class="btn btn-default btn-file"> Image <input type="file" accept="image/*" class="new_image" name="item_image"></span>');
    buttonTd.html('<span class="button-save" title="Save"><i class="fa fa-save"></i> Save</span> </form>');
    bindToListenForFileUpload();
    //bindSaveToEnterKey('#dyna_menu_item', menuItem0, price0, subcategory0, incentives0, trClicked, image0)
    //bindSaveToEnterKey('#dyna_price', menuItem0, price0, subcategory0, incentives0, trClicked, image0)
    $(".button-save").on('click', {
        menuItem0: menuItem0,
        price0: price0,
        subcategory0: subcategory0,
        incentives0: incentives0,
        currentrow: trClicked,
        image0: image0
    }, onSaveButtonClicked);


}

function bindSaveToEnterKey(idName, menuItem0, price0, subcategory0, incentives0, trClicked, image0) {
    $(idName).keypress(function(event) {
        if (event.which == 13) {
            event.data = {};
            event.data.menuItem0 = menuItem0;
            event.data.price0 = price0;
            event.data.subcategory0 = subcategory0;
            event.data.incentives0 = incentives0;
            event.data.currentrow = trClicked;
            event.data.image0 = image0;
            onSaveButtonClicked(event);
            event.preventDefault();
        }
    });
}



function bindToListenForFileUpload(){
    $(".new_image").change(function(event) {
        filename = $(this).val();
        if(filename.endsWith(".png") ||filename.endsWith(".jpeg") ||filename.endsWith(".jpg") || filename.endsWith(".gif")  ) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        $('#item_image .item_icon').hide();
        $("#item_image .preview_icon").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));

        }
        else{
            bootbox.alert('Not a valid or supported image format');
        }
    });

}


function onSaveButtonClicked(e) {
    var menuItem0 = e.data.menuItem0;
    var price0 = e.data.price0;
    var subcategory0 = e.data.subcategory0;
    var incentives0 = e.data.incentives0;
    var image0 = e.data.image0;

    var currentRow = e.data.currentrow;

    var menuItem1 = $('#dyna_menu_item').val();
    var price1 = $('#dyna_price').val();
    var subcategory1 = $('#subcategories_selector').val();
    var incentives1 = $('#incentives_selector').val();
    var new_image_file = $('.new_image')[0].files[0];

    if (validateSubCategoryEdit(menuItem1, price1, subcategory1,
            incentives1, new_image_file)) {
        sendEditMenuItemRequest(
            menuItem0, menuItem1, price1, subcategory1, incentives1, new_image_file, currentRow, image0);
    } else {
        bootbox.alert(editErrorMessage);
    }

}

function sendEditMenuItemRequest(menuItem0, menuItem1, price1, subcategory1, incentives1, new_image_file, currentRow, image0) {
    var data = {};
    var formData = new FormData();
    formData.append("menuItem0", menuItem0);
    formData.append("menuItem1", menuItem1);
    formData.append("price1", price1);
    formData.append("subcategory1", subcategory1);
    formData.append("incentives1", incentives1);
    formData.append("image0", image0);
    formData.append("new_image_file", new_image_file);
    var url = '/edit-menu-item';
    $.ajax({
        url: url,
        data: formData,
        type: 'POST',
        enctype: 'multipart/form-data',
        cache: false,
        contentType: false,
        processData: false,
        success: function(data, textStatus, jqXHR) {
            if (data == "menu item edited successfully") {
                console.log('ok');
                sessionStorage.reloadAfterPageLoad = 'true';
                var buttonTd = currentRow.children("td:nth-child(6)");
                buttonTd.html('please wait, refreshing content');
                window.location.reload();

            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
        }
    });
}


$( function () {
    if ( sessionStorage.reloadAfterPageLoad =='true' ) {
        sessionStorage.reloadAfterPageLoad = 'false';
        bootbox.alert("menu item edited");

    }
} )


function validateSubCategoryEdit(menuItem1, price1, subcategory1, incentives1, new_image_file) {
    if ((subcategory1 == null) || (subcategory1 == 0)) {
        editErrorMessage = "Please select sub category";
        return false;
    }
    if ((incentives1 == null) || (incentives1 == 0)) {
        editErrorMessage = "Please select incentive";
        return false;
    }
    if (menuItem1.length == 0) {
        editErrorMessage = "Item Name cannot be empty";
        return false;
    }
    if (price1.length == 0) {
        editErrorMessage = "Price cannot be empty";
        return false;
    }
    if (isNaN(price1)) {
        editErrorMessage = "Price must be a number";
        return false;
    }
    var $tds = $('#all_menu_items_table tr > td:nth-child(1)').filter(function() {
        return $.trim($(this).text().toUpperCase()) == menuItem1.toUpperCase();
    });
    if ($tds.length != 0) {
        editErrorMessage = "Could not update as menu item <strong>" + menuItem1 + "</strong> already exists";
        return false;
    }

    if (typeof new_image_file !== 'undefined') {
        if (new_image_file.size > 535769) {
            editErrorMessage = "Image file size cannot exceed 50KB";
            console.log(new_image_file);
            return false;
        }
    }

    return true;
}




var all_subcategories_object;

function createSubCategoryDropDown(subcategory0) {
    var url = '/get-all-subcategories-with-id';
    $.ajax({
        url: url,
        type: "GET",
        success: function(all_subcategories, textStatus, jqXHR) {
            var select = document.getElementById("subcategories_selector");
            all_subcategories_object = all_subcategories;
            for (var key in all_subcategories) {
                if (all_subcategories[key] == subcategory0) {
                    var itemval = '<option selected="selected" value="' + key + '">' + all_subcategories[key] + '</option>'
                } else {
                    var itemval = '<option value="' + key + '">' + all_subcategories[key] + '</option>'
                };
                $("#subcategories_selector").append(itemval);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
    });
}



var all_incentives_object;

function createIncentivesDropDown(incentives0) {
    var url = '/get-all-incentives-with-id';
    $.ajax({
        url: url,
        type: "GET",
        success: function(all_incentives, textStatus, jqXHR) {
            var select = document.getElementById("incentives_selector");
            all_incentives_object = all_incentives;
            for (var key in all_incentives) {
                if (all_incentives[key] == incentives0) {
                    var itemval = '<option selected="selected" value="' + key + '">' + all_incentives[key] + '</option>'
                } else {
                    var itemval = '<option value="' + key + '">' + all_incentives[key] + '</option>'
                };
                $("#incentives_selector").append(itemval);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
    });
}
