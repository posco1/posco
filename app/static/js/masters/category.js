
$("#category").focus();

var table;

$(function() {
      table = $("#all_category_table").DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "columns": [null, null, {"orderable": false}],
          "info": true,
          "autoWidth": true,
          "stateSave": true,
          "scrollX": true
      });
  });

/*

Delete

*/

$('.button-delete').on("click", deleteRow);

function deleteRow(){
    var rowToBeRemoved =   table.row( $(this).parents('tr') );
    var trClicked = $(this).parent().parent();
    var tdCategory = trClicked.children("td:nth-child(1)");
    var tdVat = trClicked.children("td:nth-child(2)");
    var categoryToBeDeleted = tdCategory.html();
    var vatForCategoryToBeDeleted = tdVat.html();
    deleteCategory(categoryToBeDeleted, vatForCategoryToBeDeleted, rowToBeRemoved);
}

  function deleteCategory(categoryToBeDeleted, vatForCategoryToBeDeleted, rowToBeRemoved) {
          bootbox.confirm("Remove <strong>" + categoryToBeDeleted + "<strong> ?",
          function(confirmedOK) {
              if (confirmedOK) {
                  sendDeleteCategoryRequest(categoryToBeDeleted, vatForCategoryToBeDeleted, rowToBeRemoved);
              }
          });
   }

  function sendDeleteCategoryRequest(categoryToBeDeleted, vatForCategoryToBeDeleted, rowToBeRemoved) {
      var data = {};
      data['category_to_be_deleted'] = categoryToBeDeleted;
      data['vat_for_category_to_be_deleted'] = vatForCategoryToBeDeleted;
      var url = '/delete-category';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "category deleted successfully") {
                  rowToBeRemoved.remove().draw();
                  bootbox.alert("Category <strong> "+ categoryToBeDeleted + "</strong> deleted");
                      $("#category").focus();
                  }
              if (data == "cannot delete as category has subcategories") {
                bootbox.alert("cannot delete as category has subcategories");
              }
              $("#category_name").focus();
               },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
      });

  }

/*


Edit Category


*/

$('.button-edit').on("click", editRow);


function editRow() {
    $(".button-edit").off("click",editRow);
    $('.button-delete').off("click", deleteRow);
    var current_row = $(this).parent().parent();
    var categoryTd = current_row.children("td:nth-child(1)");
    var vatTd = current_row.children("td:nth-child(2)");
    var buttonTd = current_row.children("td:nth-child(3)")
    categoryTd.addClass("cellEditing");
    var category0 = categoryTd.html();
    var vat0 = vatTd.html();
    categoryTd.html("<input class='form-control input-md' id='dyna_category' type='text' value='" + categoryTd.html() + "' />");
    vatTd.html("<input class='form-control input-md' id='dyna_vat' type='text' value='" + vatTd.html() + "' />");
    buttonTd.html('<span class="button-save" title="Save"><i class="fa fa-save"></i> Save</span>');
    $(".button-save").on('click', {category0: category0, vat0: vat0, current_row: current_row}, onSaveButtonClicked);
    $('#dyna_category').focus();
}


var editErrorMessage;

function onSaveButtonClicked(e){
  var category0 = e.data.category0;
  var vat0 = e.data.vat0;
  var category1 = $('#dyna_category').val();
  var vat1 = $('#dyna_vat').val();
  var current_row = e.data.current_row;
  if (validateCategoryEdit(category1, vat1)){
            sendEditCategoryRequest(category0, category1,
                                    vat0, vat1, current_row)
                                    }

  else{
    bootbox.alert(editErrorMessage); $('#dyna_category').focus();
    }
  

}

 function validateCategoryEdit(category1, vat1) {
      if (category1.length == 0) {
          editErrorMessage = "category name cannot be empty";
          return false;
      }
    if (vat1.length == 0) {
          editErrorMessage = "vat cannot be empty";
          return false;
      }
    if ((vat1<0 || vat1 > 100)) {
          editErrorMessage = "vat mustbe in the range 0-100";
          return false;
      }

      var $tds = $('#all_category_table tr > td:nth-child(1)').filter(function() {
          return $.trim($(this).text().toUpperCase()) == category1.toUpperCase();
      });
      if ($tds.length != 0) {
          editErrorMessage = "Could not update, category by that name already exists";
          return false;
      }
      if (isNaN(vat1)) {
          editErrorMessage = "Vat must be a valid number";
          return false;
      }
      return true;
  }

  function sendEditCategoryRequest(category_0, category_1, vat_0, vat_1, current_row) {
      log(current_row);
      var data = {};
      data['category_0'] = category_0;
      data['category_1'] = category_1;
      data['vat_1'] = vat_1;
      var url = '/edit-category';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "category edited successfully") {
                  bootbox.alert("Category Updated");
                  var categoryTd = current_row.children("td:nth-child(1)");
                  var vatTd = current_row.children("td:nth-child(2)");
                  var buttonTd = current_row.children("td:nth-child(3)")
                  categoryTd.html(category_1);
                  vatTd.html(vat_1);
                  buttonTd.html('<span class="button-edit" title="Edit"><i class="fa fa-edit"></i> Edit</span> | <span class="button-delete" title="Delete"><i class="fa fa-trash"></i> Delete</span>');
                  $('#dyna_category').removeAttr('id');
                  $('#dyna_vat').removeAttr('id');
                  $(".button-edit").on("click",editRow);
                  $('.button-delete').on("click", deleteRow);
                  $("#category").focus();
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
      });
  }




