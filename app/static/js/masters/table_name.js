/*Hello*/
$("#table_name").focus();

var table;

$(function() {
      table = $("#all_table_name_table").DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "columns": [null, {"orderable": false}],
          "info": true,
          "autoWidth": true,
          "stateSave": true,
            "scrollX": true
      });
  });

/*

Delete

*/

$('.button-delete').on("click", deleteRow);

function deleteRow(){
    var rowToBeRemoved =   table.row( $(this).parents('tr') );
    var trClicked = $(this).parent().parent();
    var tdtableName = trClicked.children("td:nth-child(1)");
    var tableNameToBeDeleted = tdtableName.html()
    deleteTableName(tableNameToBeDeleted, rowToBeRemoved);
}

  function deleteTableName(tableNameToBeDeleted, rowToBeRemoved) {
          bootbox.confirm("Delete Table <strong>" + tableNameToBeDeleted + "<strong> ?",
          function(confirmedOK) {
              if (confirmedOK) {
                  sendDeleteTableNameRequest(tableNameToBeDeleted, rowToBeRemoved);
              }
          });
   }

  function sendDeleteTableNameRequest(tableNameToBeDeleted, rowToBeRemoved) {
      var data = {};
      data['table_name_to_be_deleted'] = tableNameToBeDeleted;
      var url = '/delete-table-name';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "table name deleted successfully") {
                  rowToBeRemoved.remove().draw();
                  bootbox.alert("Table <strong> "+ tableNameToBeDeleted + "</strong> Deleted");
                      $("#table_name").focus();
                  }
               },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);

          }
      });

  }

/*


Edit Subcategory


*/

$('.button-edit').on("click", editRow);


function editRow() {
    $(".button-edit").off("click",editRow);
    $('.button-delete').off("click", deleteRow);
    var trClicked = $(this).parent().parent();
    var tableNameTd = trClicked.children("td:nth-child(1)");
    var buttonTd = trClicked.children("td:nth-child(2)")
    tableNameTd.addClass("cellEditing");
    var tableName0 = tableNameTd.html();
    tableNameTd.html("<input class='form-control input-md' id='dyna_tableName' type='text' value='" + tableNameTd.html() + "' />");
    bindSaveToEnterKey('#dyna_tableName', tableName0,trClicked );
    buttonTd.html('<span class="button-save" title="Save"><i class="fa fa-save"></i> Save</span>');
    $(".button-save").on('click', {tableName0: tableName0, currentrow: trClicked}, onSaveButtonClicked);
    $('#dyna_tableName').focus();
}


function bindSaveToEnterKey(idName, tableName0,trClicked){
$( idName ).keypress(function( event ) {
  if ( event.which == 13 ) {
      event.data = {};
      event.data.tableName0 = tableName0;
      event.data.currentrow = trClicked;
     onSaveButtonClicked(event);
     event.preventDefault();
  }
});
}


var editErrorMessage;

function onSaveButtonClicked(e){
  var tableName0 = e.data.tableName0;
  var tableName1 = $('#dyna_tableName').val();
  var currentrow = e.data.currentrow;
  if (validateTableNameEdit(tableName1)){sendEditTableNameRequest(tableName0, tableName1, currentrow)}
  else{bootbox.alert(editErrorMessage); $('#dyna_tableName').focus();}
  

}

 function validateTableNameEdit(tableName1) {
      if (tableName1.length == 0) {
          editErrorMessage = "Table name cannot be empty";
          return false;
      }

      var $tds = $('#all_table_name_table tr > td:nth-child(1)').filter(function() {
          return $.trim($(this).text().toUpperCase()) == tableName1.toUpperCase();
      });
      if ($tds.length != 0) {
          editErrorMessage = "Could not update as Table by that name already exists";
          return false;
      }

      return true;
  }

  function sendEditTableNameRequest(table_name_0, table_name_1, currentrow) {
      var data = {};
      data['table_name_0'] = table_name_0;
      data['table_name_1'] = table_name_1;
      var url = '/edit-table-name';
      $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(data, null, '\t'),
          contentType: 'application/json;charset=UTF-8',
          success: function(data, textStatus, jqXHR) {
              if (data == "table name edited successfully") {
                  bootbox.alert("Table Name Updated");
                  var tableNameTd = currentrow.children("td:nth-child(1)");
                  var buttonTd = currentrow.children("td:nth-child(2)")
                  tableNameTd.html(table_name_1);
                  buttonTd.html('<span class="button-edit" title="Edit"><i class="fa fa-edit"></i> Edit</span> | <span class="button-delete" title="Delete"><i class="fa fa-trash"></i> Delete</span>');
                  $('#dyna_tableName').removeAttr('id');
                  $(".button-edit").on("click",editRow);
                  $('.button-delete').on("click", deleteRow);
                  $("#table_name").focus();
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            bootbox.alert(errorThrown);
          }
      });
  }




