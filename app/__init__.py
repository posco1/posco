import config
from app.models.user import User, Anonymous
from flask import Flask
from flask_login import LoginManager

app = Flask(__name__)

app.config.from_object('config')
app.secret_key = config.SECRET_KEY
app.debug = config.DEBUG

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

from app.views import auth, dashboard
from app.views import pages
from app.views.masters import category, incentive, configurations, \
    server_name, table_name, subcategory, menu, printer,settlement_mode
from app.views.pos import order
from app.views.pos import billing
from app.views.pos import settlement
from app.views.pos import modify_order_token
from app.views.pos import modify_bill
from app.views.pos import history
from app.views.masters import customers
from app.views.pos import shift_table
from app.views.pos import merge_tables

from app.views.mms import mms_dashboard
from app.views.mms import purchase_goods
from app.views.mms import receive_goods
from app.views.mms import transfer_goods
from app.views.mms import recipe_manufacture
from app.views.mms import mms_reports


