from pymongo import MongoClient


APP_NAME = 'chocobar'
SOFTWARE_NAME = 'chocobar'
SITE_ADDRESS = 'chocobar.com'
SITE_DESCRIPTION = 'Restaurant Management Information System'
DEBUG = True
WTF_CSRF_ENABLED = True
SECRET_KEY = 'you-will-never-guess'

# menu item image upload related settings
UPLOAD_FOLDER = '/home/gublu/Documents/restoorant_images'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
#MAX_CONTENT_LENGTH = 0.5 * 1024 * 1024 #0.5 MB
MENU_IMAGE_MAX_SIZE = 0.5 * 1024 * 1024 #0.5 MB

# MongoDB Database related stuff
DB_NAME = 'chocobar' #todo - this is a unique database name for every single outlet
DATABASE = MongoClient()[DB_NAME]
CLIENTS_COLLECTION = DATABASE.clients
# todo  CLIENTS_COLLECTION.ensureIndex("email", unique=True)
CATEGORIES_COLLECTION = DATABASE.categories
SUBCATEGORIES_COLLECTION = DATABASE.subcategories
INCENTIVES_COLLECTION = DATABASE.incentives
TABLE_NAMES_COLLECTION = DATABASE.table_names
SERVER_NAMES_COLLECTION = DATABASE.server_names
MENU_ITEMS_COLLECTION = DATABASE.menu_items
CONFIGURATIONS_COLLECTION = DATABASE.configurations
SETTLEMENT_MODES_COLLECTION = DATABASE.settlement_modes
PRINTERS_COLLECTION = DATABASE.printers
ORDER_TOKENS_COLLECTION = DATABASE.order_tokens
ORDERS_COLLECTION = DATABASE.orders
TABLE_SESSION_COLLECTION = DATABASE.table_sessions
BILLS_COLLECTION = DATABASE.bills
BILL_PRINTER_USER_MAP_COLLECTION = DATABASE.bill_printer_user_map
CUSTOMERS_COLLECTION = DATABASE.customers

CONFIGURATIONS_OBJECT_ID = "542abfe4a14c971c36c96ee3"
SERVER_ID_HOME_DELIVERY = "56acedc2680fdc0f696a4162"
SERVER_ID_TAKE_AWAY = "566bbf0d680fdc1ac922be4c"
TABLE_ID_HOME_DELIVERY = "56acedc2680fdc0f696a4163"
TABLE_ID_TAKE_AWAY = "566bbf0d680fdc1ac922be4d"
NO_INFO_CUSTOMER_ID = "56acedc2680fdc0f696a4162"

TEMP_FILES_FOLDER = '/home/gublu/Documents/restoorant_temp_files'
PRINT_TEMPLATE_FILES_FOLDER = '/home/gublu/Documents/posco/restoorant_print_templates'

